/* Applet_Sub1_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Color;
import java.awt.Event;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.io.DataInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Random;

import cad_lib.GameShell;
import cad_lib.world3d;
import cad_lib.Sprite;
import cad_lib.xmplayer;
import cad_lib.object3d;

public class racer extends GameShell {
	int bestScoresState;
	int easyBots;
	int trackTimesScrollTick;
	int scoreSubmitState;
	int isLoading = 1;
	String playersName;
	int[] anIntArray236 = { 2, 1, 1, 1, 1, 1, 1, 1 };
	int[] decorationTypes = { 0, 1, 2, 3, 0, 1, 2, 3 };
	int[] carColors = { 24576, 24592, 24600, 16408, 24, 408, 664, 652 };
	int decorationType;
	int tracksLen = 4;
	int selectedTrackId;
	int selectedTrackAltered;
	int trackWidth;
	int trackHeight;
	int clearScreenTick;
	int detailSelectScreenOpen = 1;
	int highMem;
	int escPressed;
	int checkpointCount = 1;
	int lapAmount = 3;
	object3d palmObject;
	object3d treeObject;
	object3d houseObject;
	object3d postObject;
	object3d shadowObject;
	object3d carObject;
	object3d arrowObject;
	object3d hutObject;
	object3d cactusObject;
	object3d iglooObject;
	object3d snowtreeObject;
	object3d snowmanObject;
	object3d signObject;
	object3d house2Object;
	Sprite[] trackSprites;
	Sprite[] botTrackSprites;
	int anInt267 = 20;
	int anInt268 = 40;
	int selfCarLogicId = 2;
	int selfCarDrawId = selfCarLogicId;
	Color aColor271 = new Color(0, 100, 100);
	Color aColor272 = new Color(0, 80, 80);
	Color aColor273 = new Color(250, 250, 250);
	Color aColor274 = new Color(200, 200, 200);
	Graphics gameImageGraphics;
	Graphics appletGraphics;
	Graphics screen3dGraphics;
	Image gameImage;
	int carsStartX;
	int carsStartZ;
	int cameraYaw = 224;
	int cameraZoom = 800;
	int cameraPitch = 234;
	int anInt284;
	int anInt285 = 16;
	int anInt286 = 2;
	int anInt287 = 8;
	int anInt288 = 2;
	int anInt289 = 4;
	int anInt290 = 4;
	int anInt291 = 4;
	int anInt292 = 16;
	int anInt293 = 8;
	world3d world;
	object3d trackObject;
	int anInt296 = 12345678;
	boolean leftArrowKeyDown;
	boolean rightArrowKeyDown;
	boolean upArrowKeyDown;
	boolean downArrowKeyDown;
	boolean aBoolean301;
	int[][] heightMap = new int[128][128];
	int[] postPillarCount = new int[6];
	int[][] postPositionX = new int[6][6];
	int[][] postPositionZ = new int[6][6];
	int[] postsHeight = new int[6];
	int wrongCheckpointTick;
	int checkpointTick;
	Font timesRomanSize30;
	Font timesRomanSize20;
	Font timesRomanSize35;
	Font timesRomanSize15;
	xmplayer xmPlayer;
	Image[] lightSprites;
	Image[] arrowSprites;
	Image titleSprite;
	Image titleScreenImage;
	Image[] startSprites;
	int raceStartTick;
	int raceCompleted;
	int anInt323;
	int anInt324;
	int titleScreenOpen;
	int anInt326;
	Sprite selectedBotTrackSprite;
	int anInt328 = 1;
	int selfCarPosition = 1;
	int positionCheckTick;
	int botsSelected;
	int lapTimeElapse;
	int trackTimeElapsed;
	int bestLapTime;
	int carAmount = 8;
	int[] carsPositionX;
	int[] carsPositionZ;
	int[] carsRotation;
	int[] anIntArray339;
	int[] anIntArray340;
	int[] anIntArray341;
	int[] anIntArray342;
	int[] anIntArray343;
	int[] anIntArray344;
	int[] anIntArray345;
	int[] anIntArray346;
	int[] anIntArray347;
	int[] anIntArray348;
	int[] anIntArray349;
	int[] anIntArray350;
	int[] anIntArray351;
	int[] anIntArray352;
	int[] anIntArray353;
	int[] anIntArray354;
	int[] anIntArray355;
	int[] anIntArray356;
	int[] anIntArray357;
	int[] anIntArray358;
	int[] anIntArray359;
	int[] anIntArray360;
	int[] anIntArray361;
	int[] anIntArray362;
	int[] anIntArray363;
	int[] anIntArray364;
	int[] anIntArray365;
	int[] anIntArray366;
	int[] carsBaseAcceleration;
	int[] carsBaseBrake;
	int[] anIntArray369;
	int[] anIntArray370;
	int[] carsRaceCompletion;
	int[] carsCheckpoint;
	int[] carsLap;
	object3d[] carsObject;
	object3d[] carsShadowObject;
	int[][] carsCollisionState = new int[8][8];

	public void read(String fileName, byte[] buffer) {
		try {
			URL url = new URL(this.getCodeBase(), fileName);
			InputStream inputstream = url.openStream();
			DataInputStream datainputstream = new DataInputStream(inputstream);
			try {
				datainputstream.readFully(buffer);
			} finally {
				datainputstream.close();
			}
		} catch (Exception exception) {
			/* empty */
		}
	}

	public boolean isPinkPixel(Sprite sprite, int x, int y) {
		int red = sprite.getPixel(x, y) >> 16 & 0xff;
		int green = sprite.getPixel(x, y) >> 8 & 0xff;
		int blue = sprite.getPixel(x, y) & 0xff;
		if (blue == green && green == red && red > 0 || red == 255 & green == 0 && blue == 255)
			return true;
		return false;
	}

	public void configureTrack(int trackId) {
		Sprite trackSprite = trackSprites[trackId];
		selectedBotTrackSprite = botTrackSprites[trackId];
		trackWidth = trackSprite.getWidth(this);
		trackHeight = trackSprite.getHeight(this);
		decorationType = decorationTypes[selectedTrackId];
		trackObject = new object3d(trackWidth * trackHeight, (trackWidth - 1) * (trackHeight - 1) * 2);
		Random random = new Random(2384791L);
		int tileHeight = 128;
		int[] tileHeights = new int[trackHeight];
		for (int id = 0; id < trackHeight; id++) {
			tileHeight += random.nextDouble() * 64.0 - 32.0;
			if (tileHeight < 0)
				tileHeight = 0;
			else if (tileHeight > 255)
				tileHeight = 255;
			tileHeights[id] = tileHeight;
		}
		tileHeight = 128;
		for (int w = 0; w < trackWidth; w++) {
			for (int h = 0; h < trackHeight; h++) {
				tileHeight = ((tileHeight + tileHeights[h]) / 2 + (int) (random.nextDouble() * 128.0 - 64.0));
				if (tileHeight < 0)
					tileHeight = 0;
				else if (tileHeight > 255)
					tileHeight = 255;
				heightMap[w][h] = tileHeight;
				tileHeights[h] = tileHeight;
			}
		}
		System.out.println("selected track = "+selectedTrackId);
		for (int x = 0; x < trackWidth - 1; x++) {
			for (int y = 0; y < trackHeight - 1; y++) {
				int pixel = trackSprite.getPixel(x, y) & 0xffffff;
				if (pixel == 0xFF0000) {//Red pixel
					tileHeight = heightMap[x][y];
					for (int xOff = -2; xOff <= 2; xOff++) {
						for (int yOff = -2; yOff <= 2; yOff++) {
							if (x + xOff >= 0 && y + yOff >= 0 && x + xOff <= trackWidth - 1 && y + xOff <= trackHeight - 1)
								heightMap[x + xOff][y + yOff] = tileHeight;
						}
					}
				}
			}
		}
		for (int x = 0; x < trackWidth - 1; x++) {
			for (int y = 0; y < trackHeight - 1; y++) {
				int pixel = trackSprite.getPixel(x, y) & 0xffffff;
				int red = pixel >> 16 & 0xff;
				int green = pixel >> 8 & 0xff;
				int blue = pixel & 0xff;
				if (blue == red && blue < 255)
					tileHeight = green;
				else
					tileHeight = 0;
				if (tileHeight > 0) {
					heightMap[x][y] = tileHeight;
					heightMap[x + 1][y] = tileHeight;
					heightMap[x][y + 1] = tileHeight;
					heightMap[x + 1][y + 1] = tileHeight;
				}
				if (pixel == 0xFF00FF) {//Pink pixel
					carsStartX = x;
					carsStartZ = y;
				}
				if (tileHeight == 0 && pixel != 0 && x > 0)
					heightMap[x][y] = heightMap[x - 1][y];
			}
		}
		for (int x = 0; x < trackWidth; x++) {
			for (int y = 0; y < trackHeight; y++) {
				trackObject.addVertex(x * 128, -heightMap[x][y], y * 128);
			}
		}
		int i_22_ = 0;
		int i_23_ = 0;
		if (decorationType == 0) {
			i_22_ = -513;
			i_23_ = -20083;
		} else if (decorationType == 1) {
			i_22_ = -25232;
			i_23_ = -17868;
		} else if (decorationType == 2) {
			i_22_ = -32768;
			i_23_ = -26428;
		} else if (decorationType == 3) {
			i_22_ = -12684;
			i_23_ = -20083;
		}
		for (int x = 0; x < trackWidth - 1; x++) {
			for (int y = 0; y < trackHeight - 1; y++) {
				int i_26_ = i_22_;
				int i_27_ = i_22_;
				boolean bool = false;
				int[] is_28_ = new int[4];
				int[] is_29_ = new int[3];
				if (isPinkPixel(trackSprite, x, y)) {
					i_26_ = i_23_;
					i_27_ = i_23_;
					if (!isPinkPixel(trackSprite, x - 1, y) && !isPinkPixel(trackSprite, x, y - 1)) {
						i_26_ = i_22_;
						bool = false;
					} else if (!isPinkPixel(trackSprite, x + 1, y) && !isPinkPixel(trackSprite, x, y + 1)) {
						i_27_ = i_22_;
						bool = false;
					} else if (!isPinkPixel(trackSprite, x + 1, y) && !isPinkPixel(trackSprite, x, y - 1)) {
						i_27_ = i_22_;
						bool = true;
					} else if (!isPinkPixel(trackSprite, x - 1, y) && !isPinkPixel(trackSprite, x, y + 1)) {
						i_26_ = i_22_;
						bool = true;
					}
				}
				int i_30_ = heightMap[x + 1][y + 1];
				int i_31_ = (heightMap[x + 1][y] + heightMap[x][y + 1] - heightMap[x][y]);
				int i_32_ = i_30_ - i_31_;
				if (i_26_ != i_27_ || i_32_ < -10 || i_32_ > 10) {
					if (!bool) {
						is_28_[0] = y + x * trackHeight;
						is_28_[1] = y + x * trackHeight + 1;
						is_28_[2] = y + x * trackHeight + trackHeight;
						trackObject.addPolygon(3, is_28_, anInt296, i_26_);
						is_29_[0] = y + x * trackHeight + trackHeight + 1;
						is_29_[1] = y + x * trackHeight + trackHeight;
						is_29_[2] = y + x * trackHeight + 1;
						trackObject.addPolygon(3, is_29_, anInt296, i_27_);
					} else {
						is_28_[0] = y + x * trackHeight;
						is_28_[1] = y + x * trackHeight + 1;
						is_28_[2] = y + x * trackHeight + trackHeight + 1;
						trackObject.addPolygon(3, is_28_, anInt296, i_26_);
						is_29_[0] = y + x * trackHeight + trackHeight + 1;
						is_29_[1] = y + x * trackHeight + trackHeight;
						is_29_[2] = y + x * trackHeight;
						trackObject.addPolygon(3, is_29_, anInt296, i_27_);
					}
				} else {
					is_28_[0] = y + x * trackHeight + trackHeight;
					is_28_[1] = y + x * trackHeight;
					is_28_[2] = y + x * trackHeight + 1;
					is_28_[3] = y + x * trackHeight + trackHeight + 1;
					heightMap[x + 1][y + 1] = (heightMap[x + 1][y] + heightMap[x][y + 1] - heightMap[x][y]);
					trackObject.setVertex((x * trackHeight + y + trackHeight + 1), x * 128 + 128, -(heightMap[x + 1][y + 1]), y * 128 + 128);
					trackObject.addPolygon(4, is_28_, anInt296, i_26_);
				}
			}
		}
		world.clearObjects();
		world.anInt193 = 4;
		trackObject.zOffset = -48;
		world.addObject3d(trackObject);
		int[] is_33_ = { 0, 1, 2, 4, 7, 10, 14, 19 };
		int[] is_34_ = { 6, 7, 8, 10, 13, 17, 22, 27 };
		for (int carId = 0; carId < carAmount; carId++) {
			carsPositionX[carId] = (carsStartX * 128 + 80 * carId) * 128;
			carsPositionZ[carId] = (carsStartZ * 128 + 128 * (carId % 2)) * 128;
			carsRotation[carId] = 64;
			anIntArray339[carId] = 0;
			anIntArray340[carId] = 0;
			anIntArray342[carId] = 0;
			anIntArray343[carId] = 0;
			anIntArray344[carId] = 0;
			anIntArray341[carId] = 0;
			anIntArray345[carId] = 0;
			anIntArray346[carId] = 0;
			anIntArray347[carId] = 0;
			anIntArray348[carId] = 0;
			anIntArray349[carId] = 0;
			anIntArray350[carId] = 0;
			anIntArray351[carId] = 0;
			anIntArray352[carId] = 0;
			anIntArray357[carId] = 8;
			anIntArray358[carId] = 8;
			anIntArray359[carId] = 8;
			anIntArray360[carId] = 8;
			anIntArray353[carId] = 0;
			anIntArray354[carId] = 0;
			anIntArray355[carId] = 0;
			anIntArray356[carId] = 0;
			anIntArray361[carId] = carsStartX;
			anIntArray362[carId] = carsStartZ;
			anIntArray363[carId] = 6;
			anIntArray364[carId] = 0;
			anIntArray365[carId] = 0;
			anIntArray366[carId] = 0;
			anIntArray369[carId] = carsPositionX[carId] / 128;
			anIntArray370[carId] = carsPositionZ[carId] / 128;
			carsRaceCompletion[carId] = 0;
			carsCheckpoint[carId] = 0;
			carsLap[carId] = 1;
			if (easyBots == 0 || carId == anIntArray236[trackId]) {
				carsBaseAcceleration[carId] = 50 - is_33_[carId];
				carsBaseBrake[carId] = -25 + is_33_[carId] / 2;
			} else {
				carsBaseAcceleration[carId] = 50 - is_34_[carId];
				carsBaseBrake[carId] = -25 + is_34_[carId] / 2;
			}
			carsShadowObject[carId] = shadowObject.copy();
			world.addObject3d(carsShadowObject[carId]);
		}
		for (int carId = 0; carId < carAmount; carId++) {
			carsObject[carId] = carObject.copy();
			world.addObject3d(carsObject[carId]);
			carsObject[carId].originalColor = new Color(255, 0, 0);
			carsObject[carId].modifiedColor = new Color(carColors[carId] / 1024 % 32 * 8, carColors[carId] / 32 % 32 * 8, carColors[carId] % 32 * 8);
			carsObject[carId].originalColorRgb = 31744;
			carsObject[carId].modifiedColorRgb = carColors[carId];
		}
		for (int postId = 0; postId < 6; postId++) {
			postPillarCount[postId] = 0;
			postsHeight[postId] = -1;
		}
		object3d houseDecorationObject = null;
		object3d treeDecorationObject = null;
		object3d decorationObject = null;
		if (decorationType == 0) {
			houseDecorationObject = houseObject;
			treeDecorationObject = palmObject;
			decorationObject = treeObject;
		} else if (decorationType == 1) {
			houseDecorationObject = hutObject;
			treeDecorationObject = palmObject;
			decorationObject = cactusObject;
		} else if (decorationType == 2) {
			houseDecorationObject = iglooObject;
			treeDecorationObject = snowtreeObject;
			decorationObject = snowmanObject;
		} else if (decorationType == 3) {
			houseDecorationObject = house2Object;
			treeDecorationObject = treeObject;
			decorationObject = signObject;
		}
		for (int x = 0; x < trackWidth; x++) {
			for (int y = 0; y < trackHeight; y++) {
				int pixel = trackSprite.getPixel(x, y) & 0xffffff;
				if (pixel == 0xFF0000) {//red
					object3d copiedObject = houseDecorationObject.copy();
					copiedObject.requestSetTranslate(x * 128, -heightMap[x][y], y * 128);
					world.addObject3d(copiedObject);
				}
				if (pixel == 0xFF8000) {//orange
					object3d copiedObject = treeDecorationObject.copy();
					copiedObject.requestSetTranslate(x * 128, -heightMap[x][y], y * 128);
					world.addObject3d(copiedObject);
				}
				if (pixel == 0xFFFF00) {//yellow
					object3d copiedObject = decorationObject.copy();
					copiedObject.requestSetTranslate(x * 128, -heightMap[x][y], y * 128);
					world.addObject3d(copiedObject);
				}
				if (pixel == 0x80FF || pixel == 0xA0FF || pixel == 0xC0FF || pixel == 0xE0FF || pixel == 65535 || pixel == 0x80FFFF) {
					int postId;
					if (pixel == 0x80FF)
						postId = 0;
					else if (pixel == 0xA0FF)
						postId = 1;
					else if (pixel == 0xC0FF)
						postId = 2;
					else if (pixel == 0xE0FF)
						postId = 3;
					else if (pixel == 0xFFFF)
						postId = 4;
					else
						postId = 5;
					object3d postObj = postObject.copy();
					int postX = x * 128 + 64;
					int postZ = y * 128 + 64;
					int vertexOff = 12;
					world.addObject3d(postObj);
					if (postsHeight[postId] == -1)
						postsHeight[postId] = -getTileHeight(postX, postZ) - 250;
					int vertexX = postX - vertexOff;
					int vertexY = postZ - vertexOff;
					postObj.setVertex(0, vertexX, -getTileHeight(vertexX, vertexY), vertexY);
					postObj.setVertex(7, vertexX, postsHeight[postId], vertexY);
					vertexX = postX + vertexOff;
					vertexY = postZ - vertexOff;
					postObj.setVertex(1, vertexX, -getTileHeight(vertexX, vertexY), vertexY);
					postObj.setVertex(6, vertexX, postsHeight[postId], vertexY);
					vertexX = postX + vertexOff;
					vertexY = postZ + vertexOff;
					postObj.setVertex(2, vertexX, -getTileHeight(vertexX, vertexY), vertexY);
					postObj.setVertex(4, vertexX, postsHeight[postId], vertexY);
					vertexX = postX - vertexOff;
					vertexY = postZ + vertexOff;
					postObj.setVertex(3, vertexX, -getTileHeight(vertexX, vertexY), vertexY);
					postObj.setVertex(5, vertexX, postsHeight[postId], vertexY);
					int pillarId = postPillarCount[postId]++;
					postPositionX[postId][pillarId] = postX;
					postPositionZ[postId][pillarId] = postZ;
				}
			}
		}
		
		for (int checkpointId = 0; checkpointId < 6; checkpointId++) {
			if (postPillarCount[checkpointId] == 2) {
				int pillar1X = postPositionX[checkpointId][0];
				int pillar1Y = postPositionZ[checkpointId][0];
				int pillar2X = postPositionX[checkpointId][1];
				int pillar2Y = postPositionZ[checkpointId][1];
				int vertexOff = 12;
				if (pillar1X > pillar2X) {
					pillar1X -= vertexOff;
					pillar2X += vertexOff;
				} else if (pillar1X < pillar2X) {
					pillar1X += vertexOff;
					pillar2X -= vertexOff;
				}
				if (pillar1Y > pillar2Y) {
					pillar1Y -= vertexOff;
					pillar2Y += vertexOff;
				} else if (pillar1Y < pillar2Y) {
					pillar1Y += vertexOff;
					pillar2Y -= vertexOff;
				}
				object3d flagModel = new object3d(4, 1);
				flagModel.addVertex(pillar1X, postsHeight[checkpointId], pillar1Y);//Upper point
				flagModel.addVertex(pillar2X, postsHeight[checkpointId], pillar2Y);//Upper point
				flagModel.addVertex(pillar2X, postsHeight[checkpointId] + 60, pillar2Y);//Lower point
				flagModel.addVertex(pillar1X, postsHeight[checkpointId] + 60, pillar1Y);//Lower point
				int[] points = { 0, 1, 2, 3 };
				flagModel.addPolygon(4, points, 1, 1);
				world.addObject3d(flagModel);
				checkpointCount = checkpointId + 1;
			}
		}
	}

	public void mainLoad() {
		this.setLoadingPercent(0);
		tracksLen = new Integer(this.getParameter("tracks")).intValue();
		selectedTrackId = tracksLen - 1;
		this.setDelayTime(20);
		gameImage = this.createImage(512, 384);
		appletGraphics = this.getGraphics();
		gameImageGraphics = gameImage.getGraphics();
		timesRomanSize30 = new Font("TimesRoman", 1, 30);
		timesRomanSize20 = new Font("TimesRoman", 1, 20);
		timesRomanSize35 = new Font("TimesRoman", 1, 35);
		timesRomanSize15 = new Font("TimesRoman", 1, 15);
		lightSprites = new Image[3];
		this.loadImagesFromSheet("lights.gif", lightSprites, 3, 40, 80);
		this.setLoadingPercent(10);
		titleSprite = this.loadImage("title.gif");
		startSprites = new Image[2];
		this.loadImagesFromSheet("start.gif", startSprites, 2, 80, 24);
		arrowSprites = new Image[2];
		this.loadImagesFromSheet("arrows.gif", arrowSprites, 2, 200, 18);
		this.setLoadingPercent(20);
		carObject = new object3d(this, "car.obj");
		shadowObject = new object3d(this, "shadow.obj");
		arrowObject = new object3d(this, "arrow.obj");
		carsPositionX = new int[carAmount];
		carsPositionZ = new int[carAmount];
		carsRotation = new int[carAmount];
		anIntArray339 = new int[carAmount];
		anIntArray340 = new int[carAmount];
		anIntArray342 = new int[carAmount];
		anIntArray343 = new int[carAmount];
		anIntArray344 = new int[carAmount];
		anIntArray341 = new int[carAmount];
		anIntArray345 = new int[carAmount];
		anIntArray346 = new int[carAmount];
		anIntArray347 = new int[carAmount];
		anIntArray348 = new int[carAmount];
		anIntArray349 = new int[carAmount];
		anIntArray350 = new int[carAmount];
		anIntArray351 = new int[carAmount];
		anIntArray352 = new int[carAmount];
		anIntArray353 = new int[carAmount];
		anIntArray354 = new int[carAmount];
		anIntArray355 = new int[carAmount];
		anIntArray356 = new int[carAmount];
		anIntArray357 = new int[carAmount];
		anIntArray358 = new int[carAmount];
		anIntArray359 = new int[carAmount];
		anIntArray360 = new int[carAmount];
		anIntArray361 = new int[carAmount];
		anIntArray362 = new int[carAmount];
		anIntArray363 = new int[carAmount];
		anIntArray364 = new int[carAmount];
		anIntArray365 = new int[carAmount];
		anIntArray366 = new int[carAmount];
		anIntArray369 = new int[carAmount];
		anIntArray370 = new int[carAmount];
		carsBaseAcceleration = new int[carAmount];
		carsBaseBrake = new int[carAmount];
		carsRaceCompletion = new int[carAmount];
		carsCheckpoint = new int[carAmount];
		carsLap = new int[carAmount];
		carsObject = new object3d[carAmount];
		carsShadowObject = new object3d[carAmount];
		int scale = 400;
		palmObject = new object3d(this, "palm.obj");
		palmObject.requestScale(scale, scale, scale);
		treeObject = new object3d(this, "tree.obj");
		treeObject.requestScale(scale, scale, scale);
		houseObject = new object3d(this, "house.obj");
		houseObject.requestScale(scale, scale, scale);
		this.setLoadingPercent(25);
		hutObject = new object3d(this, "hut.obj");
		hutObject.requestScale(scale, scale, scale);
		cactusObject = new object3d(this, "cactus.obj");
		cactusObject.requestScale(scale, scale, scale);
		iglooObject = new object3d(this, "igloo.obj");
		iglooObject.requestScale(scale, scale, scale);
		snowmanObject = new object3d(this, "snowman.obj");
		snowmanObject.requestScale(scale, scale, scale);
		this.setLoadingPercent(30);
		snowtreeObject = new object3d(this, "snowtree.obj");
		snowtreeObject.requestScale(scale, scale, scale);
		signObject = new object3d(this, "sign.obj");
		signObject.requestScale(scale, scale, scale);
		signObject.requestAdjustRotate(0, 192, 0);
		house2Object = new object3d(this, "house2.obj");
		house2Object.requestScale(scale, scale, scale);
		postObject = new object3d(this, "post.obj");
		this.setLoadingPercent(35);
		this.fetchBestScores(0, 8);
		trackSprites = new Sprite[tracksLen];
		botTrackSprites = new Sprite[tracksLen];
		for (int trackId = 0; trackId < tracksLen; trackId++) {
			trackSprites[trackId] = new Sprite("track" + (trackId + 1) + ".gif", this);
			botTrackSprites[trackId] = new Sprite("track" + (trackId + 1) + "b.gif", this);
			this.setLoadingPercent(45 + trackId * 10);
		}
		world = new world3d(500, 5000, 60);
		String fileName = "music.xm";
		int bufferLen = 60000;
		byte[] buffer = new byte[bufferLen];
		read(fileName, buffer);
		xmPlayer = new xmplayer(buffer);
		xmPlayer.anInt71 = 48;
		this.setLoadingPercent(100);
		detailSelectScreenOpen = 1;
		isLoading = 0;
	}

	public void mainDestroy() {
		if (xmPlayer != null) {
			xmPlayer.stop();
			xmPlayer = null;
		}
	}

	public int getTileHeight(int x, int y) {
		int i_64_ = x >> 7;
		int i_65_ = y >> 7;
		int i_66_ = x & 0x7f;
		int i_67_ = y & 0x7f;
		int i_68_;
		int i_69_;
		int i_70_;
		if (i_66_ <= 128 - i_67_) {
			i_68_ = heightMap[i_64_][i_65_];
			i_69_ = heightMap[i_64_ + 1][i_65_] - i_68_;
			i_70_ = heightMap[i_64_][i_65_ + 1] - i_68_;
		} else {
			i_68_ = heightMap[i_64_ + 1][i_65_ + 1];
			i_69_ = heightMap[i_64_][i_65_ + 1] - i_68_;
			i_70_ = heightMap[i_64_ + 1][i_65_] - i_68_;
			i_66_ = 128 - i_66_;
			i_67_ = 128 - i_67_;
		}
		int i_71_ = i_68_ + i_69_ * i_66_ / 128 + i_70_ * i_67_ / 128;
		return i_71_;
	}

	public void openTitleScreen() {
		for (int i = 0; i < 8; i++)
			xmPlayer.method33(i);
		xmPlayer.anInt64 = 1;
		xmPlayer.anInt65 = 13;
		titleScreenImage = this.createImage(512, 384);
		Graphics titleScreenGraphics = titleScreenImage.getGraphics();
		anInt323 = 32;
		anInt324 = 32;
		titleScreenOpen = 1;
		carAmount = 1;
		selfCarLogicId = 3;
		selfCarDrawId = 0;
		raceStartTick = -50;
		raceCompleted = 0;
		trackTimesScrollTick = 0;
		cameraYaw = 25 + selectedTrackId * 64;
		cameraZoom = 800;
		cameraPitch = 234;
		world.anInt188 = 5;
		world.maxZ = 1700;
		world.screenWidth = 7;
		world.anInt187 = 350;
		world.minMaxX = 99;
		world.minMaxY = 83;
		screen3dGraphics = gameImageGraphics.create();
		screen3dGraphics.clipRect(52, 154, 199, 166);
		screen3dGraphics.translate(152, 237);
		configureTrack(selectedTrackId);
		for (int loop1 = 0; loop1 < 9; loop1++) {
			for (int loop2 = 0; loop2 < 7; loop2++) {
				if ((loop1 & 0x1) == (loop2 & 0x1))
					titleScreenGraphics.setColor(aColor271);
				else
					titleScreenGraphics.setColor(aColor272);
				titleScreenGraphics.fillRect(loop1 * 64 - anInt323, loop2 * 64 - anInt324, 64, 64);
			}
		}
		titleScreenGraphics.drawImage(arrowSprites[0], 52, 132, this);
		titleScreenGraphics.drawImage(arrowSprites[1], 52, 325, this);
		titleScreenGraphics.setColor(Color.black);
		titleScreenGraphics.fillRect(52, 154, 199, 166);
		titleScreenGraphics.drawRect(273, 132, 196, 209);
		titleScreenGraphics.drawImage(startSprites[0], 156, 355, this);
		titleScreenGraphics.drawImage(startSprites[1], 276, 355, this);
	}

	public void openGameScreen() {
		for (int i = 0; i < 10; i++)
			xmPlayer.method33(i);
		xmPlayer.anInt64 = 1;
		xmPlayer.anInt65 = 0;
		titleScreenOpen = 0;
		carAmount = 8;
		selfCarLogicId = anIntArray236[selectedTrackId];
		selfCarDrawId = selfCarLogicId;
		anInt328 = 1;
		lapTimeElapse = 0;
		trackTimeElapsed = 0;
		bestLapTime = 10000000;
		detailSelectScreenOpen = 0;
		escPressed = 0;
		scoreSubmitState = 0;
		checkpointTick = 0;
		wrongCheckpointTick = 0;
		configureTrack(selectedTrackId);
		raceStartTick = 150;
		raceCompleted = 0;
		cameraYaw = carsRotation[selfCarLogicId] - 175 & 0xff;
		cameraZoom = 1500;
		cameraPitch = 200;
		xmPlayer.method40(6, 7, 9000, 63);
		if (highMem == 0) {
			world.anInt188 = 5;
			world.maxZ = 1550;
			world.screenWidth = 9;
			world.anInt187 = 300;
			world.minMaxX = 256;
			world.minMaxY = 192;
		} else {
			world.anInt188 = 5;
			world.maxZ = 1900;
			world.screenWidth = 9;
			world.anInt187 = 350;
			world.minMaxX = 256;
			world.minMaxY = 192;
		}
		screen3dGraphics = gameImageGraphics.create();
		screen3dGraphics.translate(256, 192);
		screen3dGraphics.clipRect(-world.minMaxX, -world.minMaxY, world.minMaxX + world.minMaxX, world.minMaxY + world.minMaxY);
		gameImageGraphics.setColor(Color.black);
		gameImageGraphics.fillRect(0, 0, 512, 384);
	}

	public void mainRedraw() {
		if (detailSelectScreenOpen == 1) {
			gameImageGraphics.setColor(new Color(0, 32, 64));
			gameImageGraphics.fillRect(0, 0, 512, 384);
			gameImageGraphics.setColor(Color.white);
			this.drawText(gameImageGraphics, "Select Detail Level:", timesRomanSize30, 256, 32);
			this.drawText(gameImageGraphics, "Simple models and textures & near horizon", timesRomanSize20, 256, 128);
			this.drawText(gameImageGraphics, "Recommended for Pentium\u00ae Computers", timesRomanSize20, 256, 152);
			this.drawText(gameImageGraphics, "Detailed models and textures & distant horizon", timesRomanSize20, 256, 256);
			this.drawText(gameImageGraphics, "Recommended for Pentium-II\u00ae Computers", timesRomanSize20, 256, 280);
			gameImageGraphics.setColor(Color.red);
			this.drawText(gameImageGraphics, "Low Detail", timesRomanSize20, 256, 96);
			gameImageGraphics.setColor(Color.green);
			this.drawText(gameImageGraphics, "High Detail", timesRomanSize20, 256, 224);
			appletGraphics.drawImage(gameImage, 0, 0, this);
		} else if (titleScreenOpen == 1) {
			gameImageGraphics.drawImage(titleScreenImage, 0, 0, this);
			Graphics graphics = gameImageGraphics.create();
			graphics.clipRect(70, 18, 372, 100);
			for (int i = 1; i < 8; i++) {
				for (int i_73_ = 0; i_73_ < 3; i_73_++) {
					if ((i & 0x1) == (i_73_ & 0x1))
						graphics.setColor(aColor273);
					else
						graphics.setColor(aColor274);
					graphics.fillRect(i * 64 - anInt323, i_73_ * 64 - anInt324, 64, 64);
				}
			}
			gameImageGraphics.drawImage(titleSprite, 90, 30, this);
			int i = ((anIntArray349[selfCarDrawId] + anIntArray350[selfCarDrawId] + anIntArray351[selfCarDrawId] + anIntArray352[selfCarDrawId]) / 4);
			world.setCamera(carsPositionX[selfCarDrawId] / 128, i, carsPositionZ[selfCarDrawId] / 128, cameraPitch, 256 - cameraYaw & 0xff, 0, cameraZoom);
			world.render(screen3dGraphics);
			gameImageGraphics.setFont(timesRomanSize15);
			gameImageGraphics.setColor(Color.black);
			if (bestScoresState == 0)
				gameImageGraphics.drawString(("Track: " + (selectedTrackId + 1) + " - Best Track Times"), 275, 150);
			if (bestScoresState == 1)
				gameImageGraphics.drawString(("Track: " + (selectedTrackId + 1) + " - Best Lap Times"), 275, 150);
			Graphics scoresGraphics = gameImageGraphics.create();
			scoresGraphics.clipRect(273, 160, 194, 180);
			scoresGraphics.translate(273, 160);
			scoresGraphics.setFont(timesRomanSize15);
			scoresGraphics.setColor(Color.black);
			int scoresOffset = selectedTrackId * 100 + bestScoresState * 50;
			for (int id = 0; id < 50; id++) {
				int scoresY = id * 17 + 190 - trackTimesScrollTick / 3;
				int time = 30000 - bestScoresTime[id + scoresOffset];
				int hour = time / 3000;
				String timeString;
				if (hour < 10)
					timeString = "0" + hour;
				else
					timeString = String.valueOf(hour);
				int minute = time / 50 % 60;
				if (minute < 10)
					timeString += ":0" + minute;
				else
					timeString += ":" + minute;
				int second = time * 2 % 100;
				if (second < 10)
					timeString += ":0" + second;
				else
					timeString += ":" + second;
				if (scoresY > -10 && scoresY < 190) {
					scoresGraphics.drawString((String.valueOf(id + 1) + ": " + bestScoresOwner[id + scoresOffset]), 2, scoresY);
					scoresGraphics.drawString(timeString, 135, scoresY);
				}
			}
			appletGraphics.drawImage(gameImage, 0, 0, this);
			if (selectedTrackAltered == 1) {
				trackTimesScrollTick = 0;
				selectedTrackAltered = 0;
				configureTrack(selectedTrackId);
				cameraYaw = 25 + selectedTrackId * 64;
			}
		} else {
			clearScreenTick += 1 & 0x2;
			if (clearScreenTick == 0) {
				gameImageGraphics.setColor(Color.black);
				gameImageGraphics.fillRect(0, 0, 512, 384);
			}
			int i = ((anIntArray349[selfCarDrawId] + anIntArray350[selfCarDrawId] + anIntArray351[selfCarDrawId] + anIntArray352[selfCarDrawId]) / 4);
			world.setCamera(carsPositionX[selfCarDrawId] / 128, i, carsPositionZ[selfCarDrawId] / 128, cameraPitch, 256 - cameraYaw & 0xff, 0, cameraZoom);
			world.render(screen3dGraphics);
			if (raceCompleted == 0) {
				gameImageGraphics.setColor(Color.white);
				gameImageGraphics.setFont(timesRomanSize20);
				gameImageGraphics.drawString("Checkpoint:", 8, 32);
				gameImageGraphics.drawString("Lap:", 8, 62);
				gameImageGraphics.drawString("Position: ", 8, 92);
				gameImageGraphics.setFont(timesRomanSize30);
				gameImageGraphics.drawString(String.valueOf(carsCheckpoint[selfCarLogicId] + 1) + "/" + checkpointCount, 112, 32);
				gameImageGraphics.drawString((String.valueOf(carsLap[selfCarLogicId]) + "/" + lapAmount), 48, 62);
				gameImageGraphics.drawString(String.valueOf(selfCarPosition) + "/8", 85, 92);
				if (wrongCheckpointTick > 0)
					gameImageGraphics.drawString("Wrong Checkpoint!", 132, 340);
				if (checkpointTick > 0)
					gameImageGraphics.drawString("Checkpoint!", 170, 340);
			} else {
				gameImageGraphics.setFont(timesRomanSize35);
				gameImageGraphics.setColor(Color.white);
				this.drawText(gameImageGraphics, "Race Completed", timesRomanSize35, 256, 30);
				this.drawText(gameImageGraphics, "Final Position: " + selfCarPosition, timesRomanSize35, 256, 75);
				int i_82_ = bestLapTime / 3000;
				String string;
				if (i_82_ < 10)
					string = "0" + i_82_;
				else
					string = String.valueOf(i_82_);
				int i_83_ = bestLapTime / 50 % 60;
				if (i_83_ < 10)
					string += ":0" + i_83_;
				else
					string += ":" + i_83_;
				int i_84_ = bestLapTime * 2 % 100;
				if (i_84_ < 10)
					string += ":0" + i_84_;
				else
					string += ":" + i_84_;
				this.drawText(gameImageGraphics, "Best Lap Time: " + string, timesRomanSize20, 256, 305);
				i_82_ = trackTimeElapsed / 3000;
				if (i_82_ < 10)
					string = "0" + i_82_;
				else
					string = String.valueOf(i_82_);
				i_83_ = trackTimeElapsed / 50 % 60;
				if (i_83_ < 10)
					string += ":0" + i_83_;
				else
					string += ":" + i_83_;
				i_84_ = trackTimeElapsed * 2 % 100;
				if (i_84_ < 10)
					string += ":0" + i_84_;
				else
					string += ":" + i_84_;
				this.drawText(gameImageGraphics, "Track Time: " + string, timesRomanSize20, 256, 330);
				if (scoreSubmitState == 0)
					this.drawText(gameImageGraphics, "Click Mouse to Continue", timesRomanSize20, 256, 370);
				else if (scoreSubmitState == 1)
					this.drawText(gameImageGraphics, ("You have a best time! Enter your name: " + playersName), timesRomanSize20, 256, 370);
				else
					this.drawText(gameImageGraphics, "Please Wait...", timesRomanSize20, 256, 370);
			}
			if (raceStartTick < 100 && raceStartTick > 50)
				gameImageGraphics.drawImage(lightSprites[0], 236, 15, this);
			else if (raceStartTick <= 50 && raceStartTick > 0)
				gameImageGraphics.drawImage(lightSprites[1], 236, 15, this);
			else if (raceStartTick <= 0 && raceStartTick > -49)
				gameImageGraphics.drawImage(lightSprites[2], 236, 15, this);
			appletGraphics.drawImage(gameImage, 0, 0, this);
		}
	}

	public void method25(int carId) {
		int i_85_ = world.sinCosLookupTable[carsRotation[carId]];
		int i_86_ = world.sinCosLookupTable[carsRotation[carId] + 256];
		int i_87_ = carsPositionX[carId] / 128;
		int i_88_ = carsPositionZ[carId] / 128;
		int i_89_ = -24 * i_86_ - 56 * i_85_ >> 15;
		int i_90_ = -24 * i_85_ + 56 * i_86_ >> 15;
		int i_91_ = -getTileHeight(i_87_ + i_89_, i_88_ + i_90_) - 10;
		int i_92_ = 24 * i_86_ - 56 * i_85_ >> 15;
		int i_93_ = 24 * i_85_ + 56 * i_86_ >> 15;
		int i_94_ = -getTileHeight(i_87_ + i_92_, i_88_ + i_93_) - 10;
		int i_95_ = -24 * i_86_ + 56 * i_85_ >> 15;
		int i_96_ = -24 * i_85_ - 56 * i_86_ >> 15;
		int i_97_ = -getTileHeight(i_87_ + i_95_, i_88_ + i_96_) - 10;
		int i_98_ = 24 * i_86_ + 56 * i_85_ >> 15;
		int i_99_ = 24 * i_85_ - 56 * i_86_ >> 15;
		int i_100_ = -getTileHeight(i_87_ + i_98_, i_88_ + i_99_) - 10;
		if (i_91_ < i_94_)
			anIntArray345[carId] = i_91_;
		else
			anIntArray345[carId] = i_94_;
		if (i_91_ < i_97_)
			anIntArray346[carId] = i_91_;
		else
			anIntArray346[carId] = i_97_;
		if (i_97_ < i_100_)
			anIntArray348[carId] = i_97_;
		else
			anIntArray348[carId] = i_100_;
		if (i_100_ < i_94_)
			anIntArray347[carId] = i_100_;
		else
			anIntArray347[carId] = i_94_;
		int i_101_ = ((anIntArray345[carId] + anIntArray346[carId] + anIntArray347[carId] + anIntArray348[carId]) / 4);
		int i_102_ = (int) (Math.atan2((double) (anIntArray348[carId] - anIntArray345[carId]), 112.0) * 40.0);
		int i_103_ = (int) (Math.atan2((double) (anIntArray346[carId] - anIntArray347[carId]), 48.0) * 40.0);
		carsObject[carId].requestSetTranslate(i_87_, i_101_, i_88_);
		carsObject[carId].requestSetRotate(i_102_, -carsRotation[carId], i_103_);
		carsShadowObject[carId].setVertex(0, i_87_ + i_89_, i_91_ - 5, i_88_ + i_90_);
		carsShadowObject[carId].setVertex(1, i_87_ + i_92_, i_94_ - 5, i_88_ + i_93_);
		carsShadowObject[carId].setVertex(2, i_87_ + i_98_, i_100_ - 5, i_88_ + i_99_);
		carsShadowObject[carId].setVertex(3, i_87_ + i_95_, i_97_ - 5, i_88_ + i_96_);
	}

	public void doCollision() {
		boolean bool = true;
		int i = (int) (Math.random() * 2.0);
		for (int i_104_ = 0; i_104_ < 8; i_104_++) {
			for (int i_105_ = 0; i_105_ < 8; i_105_++)
				carsCollisionState[i_104_][i_105_] = 0;
		}
		while (bool) {
			bool = false;
			i = 1 - i;
			for (int carId = 0; carId < carAmount; carId++) {
				int i_107_ = world.sinCosLookupTable[carsRotation[carId]];
				int i_108_ = world.sinCosLookupTable[carsRotation[carId] + 256];
				int i_109_ = carsPositionX[carId] / 128;
				int i_110_ = carsPositionZ[carId] / 128;
				int i_111_ = -24 * i_108_ - 56 * i_107_ >> 15;
				int i_112_ = -24 * i_107_ + 56 * i_108_ >> 15;
				int i_113_ = -getTileHeight(i_109_ + i_111_, i_110_ + i_112_) - 10;
				int i_114_ = 24 * i_108_ - 56 * i_107_ >> 15;
				int i_115_ = 24 * i_107_ + 56 * i_108_ >> 15;
				int i_116_ = -getTileHeight(i_109_ + i_114_, i_110_ + i_115_) - 10;
				int i_117_ = -24 * i_108_ + 56 * i_107_ >> 15;
				int i_118_ = -24 * i_107_ - 56 * i_108_ >> 15;
				int i_119_ = -getTileHeight(i_109_ + i_117_, i_110_ + i_118_) - 10;
				int i_120_ = 24 * i_108_ + 56 * i_107_ >> 15;
				int i_121_ = 24 * i_107_ - 56 * i_108_ >> 15;
				int i_122_ = -getTileHeight(i_109_ + i_120_, i_110_ + i_121_) - 10;
				int i_123_ = 0;
				int i_124_ = 4;
				object3d collidedObject;
				if ((collidedObject = world.method97(carsObject[carId], 1 + carAmount, i_109_ + i_111_, i_113_ - 20, i_110_ + i_112_, i_109_ + i_114_, i_116_ - 20, i_110_ + i_115_, i_124_)) != null)
					i_123_ = 1;
				else if ((collidedObject = world.method97(carsObject[carId], 1 + carAmount, i_109_ + i_111_, i_113_ - 20, i_110_ + i_112_, i_109_ + i_117_, i_119_ - 20, i_110_ + i_118_, i_124_)) != null)
					i_123_ = 2;
				else if ((collidedObject = world.method97(carsObject[carId], 1 + carAmount, i_109_ + i_114_, i_116_ - 20, i_110_ + i_115_, i_109_ + i_120_, i_122_ - 20, i_110_ + i_121_, i_124_)) != null)
					i_123_ = 3;
				else if ((collidedObject = world.method97(carsObject[carId], 1 + carAmount, i_109_ + i_117_, i_119_ - 20, i_110_ + i_118_, i_109_ + i_120_, i_122_ - 20, i_110_ + i_121_, i_124_)) != null)
					i_123_ = 4;
				if (i_123_ > 0) {
					bool = true;
					int collidedCarId = -1;
					for (int id = 0; id < carAmount; id++) {
						if (collidedObject == carsObject[id])
							collidedCarId = id;
					}
					if (collidedCarId == -1) {
						carsRotation[carId] = anIntArray343[carId];
						carsPositionX[carId] = anIntArray342[carId];
						carsPositionZ[carId] = anIntArray344[carId];
						method25(carId);
						if (anIntArray365[carId] < anInt267)
							anIntArray365[carId]++;
						anIntArray339[carId] = -(int) ((double) anIntArray339[carId] * 0.75);
						anIntArray340[carId] = -(int) ((double) anIntArray340[carId] * 0.75);
					} else {
						if (i == 0) {
							carsRotation[carId] = anIntArray343[carId];
							carsPositionX[carId] = anIntArray342[carId];
							carsPositionZ[carId] = anIntArray344[carId];
							method25(carId);
							if (anIntArray365[carId] < anInt267)
								anIntArray365[carId]++;
						} else {
							carsRotation[collidedCarId] = anIntArray343[collidedCarId];
							carsPositionX[collidedCarId] = anIntArray342[collidedCarId];
							carsPositionZ[collidedCarId] = anIntArray344[collidedCarId];
							method25(collidedCarId);
							if (anIntArray365[collidedCarId] < anInt267)
								anIntArray365[collidedCarId]++;
						}
						if (carsCollisionState[carId][collidedCarId] == 0) {
							int i_127_ = anIntArray339[carId];
							anIntArray339[carId] = anIntArray339[collidedCarId];
							anIntArray339[collidedCarId] = i_127_;
							i_127_ = anIntArray340[carId];
							anIntArray340[carId] = anIntArray340[collidedCarId];
							anIntArray340[collidedCarId] = i_127_;
							carsCollisionState[carId][collidedCarId] = 1;
							carsCollisionState[collidedCarId][carId] = 1;
						} else if (carsCollisionState[carId][collidedCarId] == 1) {
							int i_128_ = ((carsPositionX[carId] - carsPositionX[collidedCarId]) / 6);
							int i_129_ = ((carsPositionZ[carId] - carsPositionZ[collidedCarId]) / 6);
							carsPositionX[carId] += i_128_;
							carsPositionZ[carId] += i_129_;
							carsPositionX[collidedCarId] -= i_128_;
							carsPositionZ[collidedCarId] -= i_129_;
							if (carsPositionX[carId] < 8192) {
								carsPositionX[carId] = 8192;
								anIntArray339[carId] = -anIntArray339[carId];
							}
							if (carsPositionX[carId] > (trackWidth - 1) * 16384) {
								carsPositionX[carId] = (trackWidth - 1) * 16384;
								anIntArray339[carId] = -anIntArray339[carId];
							}
							if (carsPositionZ[carId] < 8192) {
								carsPositionZ[carId] = 8192;
								anIntArray340[carId] = -anIntArray340[carId];
							}
							if (carsPositionZ[carId] > (trackHeight - 1) * 16384) {
								carsPositionZ[carId] = (trackHeight - 1) * 16384;
								anIntArray340[carId] = -anIntArray340[carId];
							}
							if (carsPositionX[collidedCarId] < 8192) {
								carsPositionX[collidedCarId] = 8192;
								anIntArray339[collidedCarId] = -anIntArray339[collidedCarId];
							}
							if (carsPositionX[collidedCarId] > (trackWidth - 1) * 16384) {
								carsPositionX[collidedCarId] = (trackWidth - 1) * 16384;
								anIntArray339[collidedCarId] = -anIntArray339[collidedCarId];
							}
							if (carsPositionZ[collidedCarId] < 8192) {
								carsPositionZ[collidedCarId] = 8192;
								anIntArray340[collidedCarId] = -anIntArray340[carId];
							}
							if (carsPositionZ[collidedCarId] > (trackHeight - 1) * 16384) {
								carsPositionZ[collidedCarId] = (trackHeight - 1) * 16384;
								anIntArray340[collidedCarId] = -anIntArray340[collidedCarId];
							}
							method25(carId);
							method25(collidedCarId);
							carsCollisionState[carId][collidedCarId] = 2;
							carsCollisionState[collidedCarId][carId] = 2;
						}
					}
				}
			}
		}
	}

	public void mainLoop() {
		if (detailSelectScreenOpen != 1) {
			if (titleScreenOpen == 1) {
				anInt323 = (anInt323 + 1) % 64;
				anInt324 = (anInt324 + 1) % 64;
				trackTimesScrollTick++;
				if (trackTimesScrollTick > 3150) {
					trackTimesScrollTick = 0;
					bestScoresState = 1 - bestScoresState;
				}
				if (botsSelected == 1) {
					botsSelected = 0;
					openGameScreen();
				}
			}
			if (raceCompleted == 1 || escPressed == 1) {
				escPressed = 0;
				raceCompleted = 0;
				openTitleScreen();
			}
			if (raceStartTick > -50) {
				if (cameraZoom > 800) {
					cameraZoom -= 4;
					cameraYaw = cameraYaw + 1 & 0xff;
				} else if (cameraPitch < 234) {
					cameraPitch++;
				} else {
					raceStartTick--;
				}
				if (raceStartTick == 100)
					xmPlayer.method40(7, 8, 10000, 63);
				if (raceStartTick == 70)
					xmPlayer.method33(7);
				if (raceStartTick == 50)
					xmPlayer.method40(7, 8, 10000, 63);
				if (raceStartTick == 20)
					xmPlayer.method33(7);
				if (raceStartTick == 0)
					xmPlayer.method40(7, 8, 15000, 63);
				if (raceStartTick == -40)
					xmPlayer.method33(7);
			}
			for (int carId = 0; carId < carAmount; carId++) {
				anIntArray343[carId] = carsRotation[carId];
				anIntArray342[carId] = carsPositionX[carId];
				anIntArray344[carId] = carsPositionZ[carId];
			}
			for (int carId = 0; carId < carAmount; carId++) {
				int carRotationSin = world.sinCosLookupTable[carsRotation[carId]];
				int carRotationCos = world.sinCosLookupTable[carsRotation[carId] + 256];
				int i_132_ = (anIntArray339[carId] * carRotationCos + anIntArray340[carId] * carRotationSin >> 15);
				int i_133_ = (-anIntArray339[carId] * carRotationSin + anIntArray340[carId] * carRotationCos >> 15);
				if (anIntArray346[carId] - anIntArray350[carId] < anInt293 || anIntArray347[carId] - anIntArray351[carId] < anInt293) {
					if (i_132_ > 0)
						i_132_ -= anInt287;
					else
						i_132_ += anInt287;
				}
				i_132_ -= i_132_ * anInt289 >> 8;
				if (i_132_ >= -anInt285 && i_132_ <= anInt285)
					i_132_ = 0;
				if (anIntArray348[carId] - anIntArray352[carId] < anInt293)
					i_133_ += anIntArray341[carId];
				if (anIntArray345[carId] - anIntArray349[carId] < anInt293 || anIntArray348[carId] - anIntArray352[carId] < anInt293) {
					if (i_133_ > 0)
						i_133_ -= anInt288;
					else
						i_133_ += anInt288;
				}
				i_133_ -= i_133_ * anInt290 >> 8;
				if (i_133_ >= -anInt286 && i_133_ <= anInt286)
					i_133_ = 0;
				anIntArray339[carId] = i_132_ * carRotationCos - i_133_ * carRotationSin >> 15;
				anIntArray340[carId] = i_132_ * carRotationSin + i_133_ * carRotationCos >> 15;
				carsPositionX[carId] += anIntArray339[carId];
				carsPositionZ[carId] += anIntArray340[carId];
				if (carsPositionX[carId] < 8192) {//64 * 128
					carsPositionX[carId] = 8192;
					//Adds bump back effect
					anIntArray339[carId] = -anIntArray339[carId];
				}
				if (carsPositionX[carId] > (trackWidth - 1) * 16384) {//128*128
					carsPositionX[carId] = (trackWidth - 1) * 16384;
					//Adds bump back effect
					anIntArray339[carId] = -anIntArray339[carId];
				}
				if (carsPositionZ[carId] < 8192) {
					carsPositionZ[carId] = 8192;
					//Adds bump back effect
					anIntArray340[carId] = -anIntArray340[carId];
				}
				if (carsPositionZ[carId] > (trackHeight - 1) * 16384) {
					carsPositionZ[carId] = (trackHeight - 1) * 16384;
					//Adds bump back effect
					anIntArray340[carId] = -anIntArray340[carId];
				}
				//Player driving logic
				if (carId == selfCarLogicId && raceStartTick <= 0) {
					if (raceCompleted <= 0) {
						int i_134_ = i_133_ / 1024;
						if (i_133_ > 0)
							i_134_++;
						else if (i_133_ < 0)
							i_134_--;
						if (i_134_ > 4)
							i_134_ = 4;
						else if (i_134_ < -4)
							i_134_ = -4;
						anIntArray341[carId] = 0;
						if (leftArrowKeyDown && anIntArray345[carId] - anIntArray349[carId] < anInt293)
							carsRotation[carId] = carsRotation[carId] + i_134_ & 0xff;
						if (rightArrowKeyDown && anIntArray345[carId] - anIntArray349[carId] < anInt293)
							carsRotation[carId] = carsRotation[carId] - i_134_ & 0xff;
						if (upArrowKeyDown) {
							anIntArray341[carId] = carsBaseAcceleration[carId];
						}
						if (downArrowKeyDown) {
							anIntArray341[carId] = carsBaseBrake[carId];
						}
					}
					int i_135_ = i_133_;
					if (i_135_ < 0)
						i_135_ = -i_135_;
					xmPlayer.method34(6, 9000 + i_135_ * 4);
				}
				//Bots driving logic
				if ((carId != selfCarLogicId || raceCompleted > 0) && raceStartTick <= 0) {
					int i_136_ = (carsPositionX[carId] - 8192) / 16384;
					int i_137_ = (carsPositionZ[carId] - 8192) / 16384;
					int i_138_ = i_136_ - anIntArray361[carId];
					int i_139_ = i_137_ - anIntArray362[carId];
					int i_140_ = 4 - anIntArray364[carId];
					if (i_138_ >= -i_140_ && i_138_ <= i_140_ && i_139_ >= -i_140_ && i_139_ <= i_140_) {
						int[] is = { 0, -1, 1, -2, 2, -3, 3, -4 };
						for (int i_141_ = 0; i_141_ < 8; i_141_++) {
							int i_142_ = anIntArray363[carId] + is[i_141_] & 0x7;
							i_132_ = anIntArray361[carId];
							i_133_ = anIntArray362[carId];
							if (i_142_ == 0)
								i_133_ = anIntArray362[carId] - 1;
							else if (i_142_ == 1) {
								i_132_ = anIntArray361[carId] + 1;
								i_133_ = anIntArray362[carId] - 1;
							} else if (i_142_ == 2)
								i_132_ = anIntArray361[carId] + 1;
							else if (i_142_ == 3) {
								i_132_ = anIntArray361[carId] + 1;
								i_133_ = anIntArray362[carId] + 1;
							} else if (i_142_ == 4)
								i_133_ = anIntArray362[carId] + 1;
							else if (i_142_ == 5) {
								i_132_ = anIntArray361[carId] - 1;
								i_133_ = anIntArray362[carId] + 1;
							} else if (i_142_ == 6)
								i_132_ = anIntArray361[carId] - 1;
							else {
								i_132_ = anIntArray361[carId] - 1;
								i_133_ = anIntArray362[carId] - 1;
							}
							if ((selectedBotTrackSprite.getPixel(i_132_, i_133_) & 0xffffff) == 16711935) {
								anIntArray364[carId] = 0;
								anIntArray363[carId] = anIntArray363[carId] + is[i_141_];
								break;
							}
							if ((selectedBotTrackSprite.getPixel(i_132_, i_133_) & 0xffffff) == 65535) {
								anInt328 = 0;
								anIntArray364[carId] = 3;
								anIntArray363[carId] = anIntArray363[carId] + is[i_141_];
								break;
							}
						}
						anIntArray361[carId] = i_132_;
						anIntArray362[carId] = i_133_;
						anIntArray365[carId] = 0;
						anIntArray366[carId] = 0;
					}
					i_136_ = (carsPositionX[carId] + anIntArray339[carId] * 14) / 128;
					i_137_ = (carsPositionZ[carId] + anIntArray340[carId] * 14) / 128;
					i_132_ = anIntArray361[carId] * 128 + 64;
					i_133_ = anIntArray362[carId] * 128 + 64;
					if (anInt328 == 1) {
						i_132_ += carId % 2 * 128 - 32;
						i_133_ += carId % 2 * 128 - 32;
					}
					int i_143_ = (int) ((Math.atan2((double) (i_133_ - i_137_), (double) (i_132_ - i_136_)) * 40.744) - 64.0) & 0xff;
					int i_144_ = i_133_ / 1024;
					if (i_133_ > 0)
						i_144_++;
					else if (i_133_ < 0)
						i_144_--;
					if (i_144_ > 4)
						i_144_ = 4;
					else if (i_144_ < -4)
						i_144_ = -4;
					anIntArray341[carId] = carsBaseAcceleration[carId] + (int) (Math.random() * 6.0) - 3;
					if (anIntArray365[carId] == anInt267)
						anIntArray365[carId] = anInt268 + anIntArray366[carId] * 10;
					if (anIntArray365[carId] > anInt267) {
						anIntArray365[carId]--;
						if (anIntArray365[carId] == anInt267 + 1) {
							anIntArray365[carId] = 0;
							anIntArray366[carId]++;
						}
						if (anIntArray366[carId] == 0)
							i_143_ = i_143_ + 128 & 0xff;
						if (anIntArray366[carId] == 1)
							anIntArray341[carId] = carsBaseBrake[carId];
						else if (anIntArray366[carId] >= 2) {
							i_143_ = (int) (Math.random() * 255.0);
							anIntArray341[carId] = carsBaseBrake[carId];
						}
						if (anIntArray366[carId] >= 6) {
							carsPositionX[carId] = anIntArray361[carId] * 16384 + 8192;
							carsPositionZ[carId] = anIntArray362[carId] * 16384 + 8192;
							anIntArray339[carId] = 0;
							anIntArray340[carId] = 0;
						}
					}
					int i_145_ = i_143_ - carsRotation[carId];
					if (i_145_ >= i_144_ || i_145_ <= -i_144_) {
						if (i_145_ > 0 && i_145_ < 128 || i_145_ < -128 && i_145_ > -256)
							carsRotation[carId] = carsRotation[carId] + i_144_ & 0xff;
						else
							carsRotation[carId] = carsRotation[carId] - i_144_ & 0xff;
					}
				}
				method25(carId);
			}
			doCollision();
			for (int i = 0; i < carAmount; i++) {
				for (int i_146_ = 0; i_146_ < anInt291; i_146_++) {
					anIntArray353[i]++;
					if (anIntArray353[i] > anIntArray357[i]) {
						anIntArray353[i] = 0;
						anIntArray357[i]--;
						if (anIntArray345[i] > anIntArray349[i])
							anIntArray349[i]++;
					}
					anIntArray356[i]++;
					if (anIntArray356[i] > anIntArray360[i]) {
						anIntArray356[i] = 0;
						anIntArray360[i]--;
						if (anIntArray348[i] > anIntArray352[i])
							anIntArray352[i]++;
					}
					anIntArray354[i]++;
					if (anIntArray354[i] > anIntArray358[i]) {
						anIntArray354[i] = 0;
						anIntArray358[i]--;
						if (anIntArray346[i] > anIntArray350[i])
							anIntArray350[i]++;
					}
					anIntArray355[i]++;
					if (anIntArray355[i] > anIntArray359[i]) {
						anIntArray355[i] = 0;
						anIntArray359[i]--;
						if (anIntArray347[i] > anIntArray351[i])
							anIntArray351[i]++;
					}
				}
				if (anIntArray345[i] < anIntArray349[i]) {
					anIntArray349[i] = anIntArray345[i];
					anIntArray357[i] = anInt292;
				}
				if (anIntArray346[i] < anIntArray350[i]) {
					anIntArray350[i] = anIntArray346[i];
					anIntArray358[i] = anInt292;
				}
				if (anIntArray347[i] < anIntArray351[i]) {
					anIntArray351[i] = anIntArray347[i];
					anIntArray359[i] = anInt292;
				}
				if (anIntArray348[i] < anIntArray352[i]) {
					anIntArray352[i] = anIntArray348[i];
					anIntArray360[i] = anInt292;
				}
				int i_147_ = carsPositionX[i] / 128;
				int i_148_ = carsPositionZ[i] / 128;
				int i_149_ = ((anIntArray349[i] + anIntArray350[i] + anIntArray351[i] + anIntArray352[i]) / 4);
				int i_150_ = (int) (Math.atan2((double) (anIntArray352[i] - anIntArray349[i]), 112.0) * 40.0);
				int i_151_ = (int) (Math.atan2((double) (anIntArray350[i] - anIntArray351[i]), 48.0) * 40.0);
				carsObject[i].requestSetTranslate(i_147_, i_149_, i_148_);
				carsObject[i].requestSetRotate(i_150_, -carsRotation[i], i_151_);
				if (i == selfCarLogicId && raceStartTick > 0) {
					if (raceStartTick == 149 || raceStartTick == 130 || raceStartTick == 110) {
						arrowObject.requestSetTranslate(i_147_, i_149_, i_148_);
						arrowObject.requestSetRotate(0, -carsRotation[i], 0);
						world.addObject3d(arrowObject);
					}
					if (raceStartTick == 140 || raceStartTick == 120 || raceStartTick == 100)
						world.removeObject(arrowObject);
				}
			}
			if (raceCompleted <= 0) {
				for (int carId = 0; carId < carAmount; carId++) {
					int carX = carsPositionX[carId] / 128;
					int carY = carsPositionZ[carId] / 128;
					if (carId != selfCarLogicId || checkpointTick <= 0 && wrongCheckpointTick <= 0) {
						for (int checkpointId = 0; checkpointId < checkpointCount; checkpointId++) {
							int postPillar1X = postPositionX[checkpointId][0];
							int postPillar1Y = postPositionZ[checkpointId][0];
							int postPillar2X = postPositionX[checkpointId][1];
							int postPillar2Y = postPositionZ[checkpointId][1];
							if (postPillar2X < postPillar1X) {
								int i_159_ = postPillar1X;
								postPillar1X = postPillar2X;
								postPillar2X = i_159_;
							}
							if (postPillar2Y < postPillar1Y) {
								int i_160_ = postPillar1Y;
								postPillar1Y = postPillar2Y;
								postPillar2Y = i_160_;
							}
							if (postPillar1Y == postPillar2Y) {
								if (carX >= postPillar1X && carX <= postPillar2X && ((carY >= postPillar1Y && anIntArray370[carId] <= postPillar1Y) || (carY <= postPillar1Y && anIntArray370[carId] >= postPillar1Y))) {
									if (carId == selfCarLogicId) {
										if (carsCheckpoint[carId] == checkpointId)
											checkpointTick = 100;
										else
											wrongCheckpointTick = 100;
									}
									if (carsCheckpoint[carId] == checkpointId) {
										carsCheckpoint[carId]++;
										if (carsCheckpoint[carId] >= checkpointCount) {
											carsCheckpoint[carId] = 0;
											carsLap[carId]++;
											if (carId == selfCarLogicId) {
												if (lapTimeElapse < bestLapTime)
													bestLapTime = lapTimeElapse;
												lapTimeElapse = 0;
											}
										}
									}
								}
							} else if (carY >= postPillar1Y && carY <= postPillar2Y && ((carX >= postPillar1X && anIntArray369[carId] <= postPillar1X) || (carX <= postPillar1X && (anIntArray369[carId] >= postPillar1X)))) {
								if (carId == selfCarLogicId) {
									if (carsCheckpoint[carId] == checkpointId)
										checkpointTick = 100;
									else
										wrongCheckpointTick = 100;
								}
								if (carsCheckpoint[carId] == checkpointId) {
									carsCheckpoint[carId]++;
									if (carsCheckpoint[carId] >= checkpointCount) {
										carsCheckpoint[carId] = 0;
										carsLap[carId]++;
										if (carId == selfCarLogicId) {
											if (bestLapTime > lapTimeElapse)
												bestLapTime = lapTimeElapse;
											lapTimeElapse = 0;
										}
									}
								}
							}
						}
					}
					anIntArray369[carId] = carX;
					anIntArray370[carId] = carY;
				}
			}
			positionCheckTick = (positionCheckTick + 1) % 25;
			if (positionCheckTick == 1 && raceCompleted <= 0) {
				for (int carId = 0; carId < carAmount; carId++) {
					int checkpoint = carsCheckpoint[carId];
					int raceCompletionBase = (checkpoint + carsLap[carId] * checkpointCount) * 100000;
					int distanceToPostX = ((postPositionX[checkpoint][0] + postPositionX[checkpoint][1]) / 2 - carsPositionX[carId] / 128);
					int distanceToPostZ = ((postPositionZ[checkpoint][0] + postPositionZ[checkpoint][1]) / 2 - carsPositionZ[carId] / 128);
					carsRaceCompletion[carId] = (raceCompletionBase - (int) Math.sqrt((double) (distanceToPostX * distanceToPostX + distanceToPostZ * distanceToPostZ)));
				}
				selfCarPosition = 1;
				for (int id = 0; id < carAmount; id++) {
					if (carsRaceCompletion[id] > carsRaceCompletion[selfCarLogicId])
						selfCarPosition++;
				}
			}
			if (selfCarDrawId == selfCarLogicId && raceStartTick <= 0 && raceCompleted <= 0) {
				int i = carsRotation[selfCarDrawId] - cameraYaw;
				if (i > 0 && i < 128 || i < -128 && i > -256)
					cameraYaw = cameraYaw + 1 & 0xff;
				else if (cameraYaw != carsRotation[selfCarDrawId])
					cameraYaw = cameraYaw - 1 & 0xff;
			}
			if (checkpointTick % 15 == 9 && checkpointTick > 50)
				xmPlayer.method40(7, 8, 15000, 63);
			else if (checkpointTick % 15 == 1)
				xmPlayer.method33(7);
			if (wrongCheckpointTick % 15 == 9 && wrongCheckpointTick > 50)
				xmPlayer.method40(7, 8, 12000, 63);
			else if (wrongCheckpointTick % 15 == 1)
				xmPlayer.method33(7);
			if (checkpointTick > 0)
				checkpointTick--;
			if (wrongCheckpointTick > 0)
				wrongCheckpointTick--;
			if (carsLap[selfCarLogicId] > lapAmount) {
				raceCompleted = 50;
				carsLap[selfCarLogicId] = 0;
				for (int id = 0; id < 50; id++) {
					if (30000 - trackTimeElapsed > bestScoresTime[id + selectedTrackId * 100]) {
						playersName = "";
						scoreSubmitState = 1;
					}
				}
				for (int id = 0; id < 50; id++) {
					if (30000 - bestLapTime > bestScoresTime[id + selectedTrackId * 100 + 50]) {
						playersName = "";
						scoreSubmitState = 1;
					}
				}
			}
			if (raceStartTick <= 0 && raceCompleted <= 0) {
				lapTimeElapse++;
				trackTimeElapsed++;
			}
		}
	}

	public boolean keyDown(Event event, int key) {
		if (isLoading == 0) {
			if (key == 1006)
				leftArrowKeyDown = true;
			if (key == 1007)
				rightArrowKeyDown = true;
			if (key == 1004)
				upArrowKeyDown = true;
			if (key == 1005)
				downArrowKeyDown = true;
			if ((key == 113 || key == 27) && scoreSubmitState == 0 && titleScreenOpen == 0 && detailSelectScreenOpen == 0 && escPressed == 0) {
				escPressed = 1;
			}
			if (scoreSubmitState == 1) {
				if (key >= 32 && key <= 122 && playersName.length() < 12)
					playersName += (char) key;
				if (key == 8 && playersName.length() > 0)
					playersName = playersName.substring(0, playersName.length() - 1);
				if ((key == 13 || key == 10) && playersName.length() > 0) {
					scoreSubmitState = 2;
					for (int id = 0; id < 50; id++) {
						if (30000 - trackTimeElapsed > bestScoresTime[id + selectedTrackId * 100]) {
							this.submitScore(playersName, 30000 - trackTimeElapsed, selectedTrackId * 2, 0);
							break;
						}
					}
					for (int id = 0; id < 50; id++) {
						if (30000 - bestLapTime > bestScoresTime[id + selectedTrackId * 100 + 50]) {
							this.submitScore(playersName, 30000 - bestLapTime, selectedTrackId * 2 + 1, 0);
							break;
						}
					}
					scoreSubmitState = 0;
				}
			}
		}
		return true;
	}

	public boolean keyUp(Event event, int key) {
		if (key == 1006)
			leftArrowKeyDown = false;
		if (key == 1007)
			rightArrowKeyDown = false;
		if (key == 1004)
			upArrowKeyDown = false;
		if (key == 1005)
			downArrowKeyDown = false;
		return true;
	}

	public boolean mouseDown(Event event, int x, int y) {
		if (isLoading == 0) {
			if (titleScreenOpen == 1 && detailSelectScreenOpen == 0) {
				if (x > 156 && x < 256 && y > 351 && y < 379) {
					botsSelected = 1;
					easyBots = 1;
				}
				if (x > 256 && y > 351 && x < 356 && y < 379) {
					botsSelected = 1;
					easyBots = 0;
				}
				if (x > 45 && y > 124 && x < 256 && y < 156) {
					trackTimesScrollTick = 0;
					selectedTrackId = (selectedTrackId - 1 + tracksLen) % tracksLen;
					selectedTrackAltered = 1;
				}
				if (x > 46 && y > 319 && x < 233 && y < 345) {
					trackTimesScrollTick = 0;
					selectedTrackId = (selectedTrackId + 1) % tracksLen;
					selectedTrackAltered = 1;
				}
				if (x > 273 && y > 132 && x < 469 && y < 341) {
					bestScoresState = 1 - bestScoresState;
					trackTimesScrollTick = 0;
				}
			}
			if (raceCompleted > 0 && titleScreenOpen == 0 && scoreSubmitState == 0)
				raceCompleted = 1;
			if (detailSelectScreenOpen == 1) {
				if (y < 170) {
					highMem = 0;
					world.loadTextures("textures2.txt", this);
				} else {
					highMem = 1;
					world.loadTextures("textures.txt", this);
				}
				openTitleScreen();
				xmPlayer.start();
				detailSelectScreenOpen = 0;
			}
		}
		return true;
	}
}
