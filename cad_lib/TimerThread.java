/* Thread_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package cad_lib;

import java.applet.Applet;

class TimerThread extends Thread {
	public int timer;
	public int delay = 40;
	public Applet applet;

	public void run() {
		for (;;) {
			try {
				Thread.sleep((long) delay);
			} catch (InterruptedException interruptedexception) {
				/* empty */
			}
			timer++;
			if (applet != null)
				applet.destroy();
		}
	}
}
