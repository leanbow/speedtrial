/* Class1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package cad_lib;
import java.applet.Applet;
import java.awt.Color;
import java.io.DataInputStream;
import java.io.InputStream;
import java.net.URL;

public class object3d
{
    public Color originalColor;
    public Color modifiedColor;
    public int originalColorRgb = -1;
    public int modifiedColorRgb = -1;
    public int vertexAmount;
    public int[] cameraModifiedVerticesX;
    public int[] cameraModifiedVerticesY;
    public int[] cameraModifiedVerticesZ;
    public int[] screenX;
    public int[] screenY;
    public int polygonCount;
    public int[] polygonPointsCount;
    public int[][] polygonPoints;
    public int[] polygonColors1;
    public int[] polygonColors2;
    public int[] verticesNormalX;
    public int[] verticesNormalY;
    public int[] verticesNormalZ;
    public int[] anIntArray131;
    public int[] anIntArray132;
    public int[] anIntArray133;
    public int anInt134;
    public int zOffset;
    private static int[] sinCosLookupTable = new int[512];
    private static byte[] aByteArray137 = new byte[64];
    private static int[] anIntArray138 = new int[256];
    private int anInt139 = 4;
    private int anInt140 = 12345678;
    private int updateNeeded = 1;
    private int vertexLimit;
    private int[] verticesX;
    private int[] verticesY;
    private int[] verticexZ;
    private int[] transformedVerticesX;
    private int[] transformedVerticesY;
    private int[] transformedVerticesZ;
    private int polygonLimit;
    private int[][] anIntArrayArray150;
    private int[] polygonsMinX;
    private int[] polygonsMaxX;
    private int[] polygonsMinY;
    private int[] polygonsMaxY;
    private int[] polygonsMinZ;
    private int[] polygonsMaxZ;
    private int translateX;
    private int translateY;
    private int translateZ;
    private int rotatePitch;
    private int rotateYaw;
    private int rotateRoll;
    private int scaleX;
    private int scaleY;
    private int scaleZ;
    private int scaleXFromY;
    private int scaleZFromY;
    private int scaleXFromZ;
    private int scaleYFromZ;
    private int scaleZFromX;
    private int scaleYFromX;
    private int transformType;
    private int minX;
    private int maxX;
    private int minY;
    private int maxY;
    private int minZ;
    private int maxZ;
    private byte[] outputBuffer;
    private int bufferPos;
    private int anInt181;
    
	public object3d(int i, int i_0_) {
		init(i, i_0_);
		anIntArrayArray150 = new int[i_0_][1];
		for (int i_1_ = 0; i_1_ < i_0_; i_1_++)
			anIntArrayArray150[i_1_][0] = i_1_;
	}
    
	private object3d(int i, int i_2_, boolean bool) {
		init(i, i_2_);
		anIntArrayArray150 = new int[i_2_][];
	}
    
	private void init(int i, int i_3_) {
		verticesX = new int[i];
		verticesY = new int[i];
		verticexZ = new int[i];
		transformedVerticesX = new int[i];
		transformedVerticesY = new int[i];
		transformedVerticesZ = new int[i];
		cameraModifiedVerticesX = new int[i];
		cameraModifiedVerticesY = new int[i];
		cameraModifiedVerticesZ = new int[i];
		screenX = new int[i];
		screenY = new int[i];
		polygonPointsCount = new int[i_3_];
		polygonPoints = new int[i_3_][];
		polygonColors1 = new int[i_3_];
		polygonColors2 = new int[i_3_];
		verticesNormalX = new int[i_3_];
		verticesNormalY = new int[i_3_];
		verticesNormalZ = new int[i_3_];
		anIntArray132 = new int[i_3_];
		anIntArray131 = new int[i_3_];
		anIntArray133 = new int[i_3_];
		polygonsMinX = new int[i_3_];
		polygonsMaxX = new int[i_3_];
		polygonsMinY = new int[i_3_];
		polygonsMaxY = new int[i_3_];
		polygonsMinZ = new int[i_3_];
		polygonsMaxZ = new int[i_3_];
		polygonCount = 0;
		vertexAmount = 0;
		vertexLimit = i;
		polygonLimit = i_3_;
		translateX = translateY = translateZ = 0;
		rotatePitch = rotateYaw = rotateRoll = 0;
		scaleX = scaleY = scaleZ = 256;
		scaleXFromY = scaleZFromY = scaleXFromZ = scaleYFromZ = scaleZFromX = scaleYFromX = 256;
		transformType = 0;
	}
    
	public object3d(Applet applet, String string) {
		byte[] buffer = null;
		try {
			URL url = new URL(applet.getCodeBase(), string);
			InputStream inputstream = url.openStream();
			DataInputStream datainputstream = new DataInputStream(inputstream);
			buffer = new byte[3];
			bufferPos = 0;
			anInt181 = 0;
			for (int off = 0; off < 3; off += datainputstream.read(buffer, off, 3 - off)) {
				/* empty */
			}
			int fileLength = read(buffer);
			buffer = new byte[fileLength];
			bufferPos = 0;
			anInt181 = 0;
			for (int off = 0; off < fileLength; off += datainputstream.read(buffer, off, fileLength - off)) {
				/* empty */
			}
			datainputstream.close();
		} catch (java.io.IOException ioexception) {
			/* empty */
		}
		int vertexCount = read(buffer);
		int polygonCount = read(buffer);
		init(vertexCount, polygonCount);
		anIntArrayArray150 = new int[polygonCount][];
		for (int id = 0; id < vertexCount; id++) {
			int x = read(buffer);
			int y = read(buffer);
			int z = read(buffer);
			addVertex(x, y, z);
		}
		for (int polygonId = 0; polygonId < polygonCount; polygonId++) {
			int polygonPointsCount = read(buffer);
			int polygonColor1 = read(buffer);//figure out
			int polygonColor2 = read(buffer);//figure out
			int length2 = read(buffer);
			
			int[] polygonPoints = new int[polygonPointsCount];
			for (int id = 0; id < polygonPointsCount; id++)
				polygonPoints[id] = read(buffer);
			
			int[] is_18_ = new int[length2];
			for (int i_19_ = 0; i_19_ < length2; i_19_++)
				is_18_[i_19_] = read(buffer);
			
			addPolygon(polygonPointsCount, polygonPoints, polygonColor1, polygonColor2);
			
			anIntArrayArray150[polygonId] = is_18_;
		}
		updateNeeded = 1;
	}
    
    public object3d(object3d[] class1s, int i) {
	int i_20_ = 0;
	int i_21_ = 0;
	for (int i_22_ = 0; i_22_ < i; i_22_++) {
	    i_20_ += class1s[i_22_].polygonCount;
	    i_21_ += class1s[i_22_].vertexAmount;
	}
	init(i_21_, i_20_);
	anIntArrayArray150 = new int[i_20_][];
	for (int i_23_ = 0; i_23_ < i; i_23_++) {
	    object3d class1_24_ = class1s[i_23_];
	    class1_24_.method70();
	    for (int i_25_ = 0; i_25_ < class1_24_.polygonCount; i_25_++) {
		int[] is = new int[class1_24_.polygonPointsCount[i_25_]];
		int[] is_26_ = class1_24_.polygonPoints[i_25_];
		for (int i_27_ = 0; i_27_ < class1_24_.polygonPointsCount[i_25_];
		     i_27_++)
		    is[i_27_]
			= addVertex(class1_24_.verticesX[is_26_[i_27_]],
				   class1_24_.verticesY[is_26_[i_27_]],
				   class1_24_.verticexZ[is_26_[i_27_]]);
		int i_28_ = addPolygon(class1_24_.polygonPointsCount[i_25_], is,
				     class1_24_.polygonColors1[i_25_],
				     class1_24_.polygonColors2[i_25_]);
		if (i > 1) {
		    anIntArrayArray150[i_28_]
			= (new int
			   [class1_24_.anIntArrayArray150[i_25_].length + 1]);
		    anIntArrayArray150[i_28_][0] = i_23_;
		    for (int i_29_ = 0;
			 i_29_ < class1_24_.anIntArrayArray150[i_25_].length;
			 i_29_++)
			anIntArrayArray150[i_28_][i_29_ + 1]
			    = class1_24_.anIntArrayArray150[i_25_][i_29_];
		} else {
		    anIntArrayArray150[i_28_]
			= new int[class1_24_.anIntArrayArray150[i_25_].length];
		    for (int i_30_ = 0;
			 i_30_ < class1_24_.anIntArrayArray150[i_25_].length;
			 i_30_++)
			anIntArrayArray150[i_28_][i_30_]
			    = class1_24_.anIntArrayArray150[i_25_][i_30_];
		}
	    }
	}
	updateNeeded = 1;
    }
    
    public object3d(int i, int[] is, int[] is_31_, int[] is_32_, int[] is_33_,
		  int[] is_34_, int[] is_35_, boolean bool, int i_36_,
		  int i_37_) {
	this(i * 2, bool ? i + 2 : i);
	vertexAmount = i * 2;
	for (int i_38_ = 0; i_38_ < i; i_38_++) {
	    verticesX[i_38_] = is[i_38_];
	    verticesY[i_38_] = is_31_[i_38_];
	    verticexZ[i_38_] = is_32_[i_38_];
	    verticesX[i_38_ + i] = is_33_[i_38_];
	    verticesY[i_38_ + i] = is_34_[i_38_];
	    verticexZ[i_38_ + i] = is_35_[i_38_];
	}
	polygonCount = bool ? i + 2 : i;
	for (int i_39_ = 0; i_39_ < i; i_39_++) {
	    int[] is_40_ = new int[4];
	    polygonPointsCount[i_39_] = 4;
	    is_40_[0] = i_39_;
	    is_40_[1] = (i_39_ + 1) % i;
	    is_40_[2] = (i_39_ + 1) % i + i;
	    is_40_[3] = i_39_ + i;
	    polygonPoints[i_39_] = is_40_;
	    polygonColors1[i_39_] = i_36_;
	    polygonColors2[i_39_] = i_37_;
	}
	if (bool) {
	    int[] is_41_ = new int[i];
	    polygonPointsCount[i] = i;
	    for (int i_42_ = 0; i_42_ < i; i_42_++)
		is_41_[i - i_42_ - 1] = i_42_;
	    polygonPoints[i] = is_41_;
	    polygonColors1[i] = i_36_;
	    polygonColors2[i] = i_37_;
	    int[] is_43_ = new int[i];
	    polygonPointsCount[i + 1] = i;
	    for (int i_44_ = 0; i_44_ < i; i_44_++)
		is_43_[i_44_] = i_44_ + i;
	    polygonPoints[i + 1] = is_43_;
	    polygonColors1[i + 1] = i_36_;
	    polygonColors2[i + 1] = i_37_;
	}
	updateNeeded = 1;
    }
    
    public object3d(int i, int[] is, int[] is_45_, int[] is_46_, int i_47_,
		  int i_48_, int i_49_, boolean bool, int i_50_, int i_51_) {
	this(i + 1, bool ? i + 1 : i);
	for (int i_52_ = 0; i_52_ < i; i_52_++) {
	    verticesX[i_52_] = is[i_52_];
	    verticesY[i_52_] = is_45_[i_52_];
	    verticexZ[i_52_] = is_46_[i_52_];
	}
	verticesX[i] = i_47_;
	verticesY[i] = i_48_;
	verticexZ[i] = i_49_;
	vertexAmount = i + 1;
	polygonCount = bool ? i + 1 : i;
	for (int i_53_ = 0; i_53_ < i; i_53_++) {
	    int[] is_54_ = new int[3];
	    is_54_[0] = i_53_;
	    is_54_[1] = (i_53_ + 1) % i;
	    is_54_[2] = i;
	    polygonPointsCount[i_53_] = 3;
	    polygonPoints[i_53_] = is_54_;
	    polygonColors1[i_53_] = i_50_;
	    polygonColors2[i_53_] = i_51_;
	}
	if (bool) {
	    int[] is_55_ = new int[i];
	    polygonPointsCount[i] = i;
	    for (int i_56_ = 0; i_56_ < i; i_56_++)
		is_55_[i - i_56_ - 1] = i_56_;
	    polygonPoints[i] = is_55_;
	    polygonColors1[i] = i_50_;
	    polygonColors2[i] = i_51_;
	}
	updateNeeded = 1;
    }
    
    public object3d(int i, int[] is, int[] is_57_, int[] is_58_, int i_59_,
		  int i_60_) {
	this(i, 1);
	vertexAmount = i;
	for (int i_61_ = 0; i_61_ < i; i_61_++) {
	    verticesX[i_61_] = is[i_61_];
	    verticesY[i_61_] = is_57_[i_61_];
	    verticexZ[i_61_] = is_58_[i_61_];
	}
	polygonCount = 1;
	polygonPointsCount[0] = i;
	int[] is_62_ = new int[i];
	for (int i_63_ = 0; i_63_ < i; i_63_++)
	    is_62_[i_63_] = i_63_;
	polygonPoints[0] = is_62_;
	polygonColors1[0] = i_59_;
	polygonColors2[0] = i_60_;
	updateNeeded = 1;
    }
    
	public int addVertex(int x, int y, int z) {
		for (int id = 0; id < vertexAmount; id++) {
			if (verticesX[id] == x && verticesY[id] == y && verticexZ[id] == z)
				return id;
		}
		if (vertexAmount >= vertexLimit)
			return -1;
		verticesX[vertexAmount] = x;
		verticesY[vertexAmount] = y;
		verticexZ[vertexAmount] = z;
		return vertexAmount++;
	}
    
	public int addPolygon(int pointsCount, int[] points, int color1, int color2) {
		if (polygonCount >= polygonLimit)
			return -1;
		polygonPointsCount[polygonCount] = pointsCount;
		polygonPoints[polygonCount] = points;
		polygonColors1[polygonCount] = color1;
		polygonColors2[polygonCount] = color2;
		updateNeeded = 1;
		return polygonCount++;
	}
    
	public void setVertex(int id, int x, int y, int z) {
		if (x != anInt140)
			verticesX[id] = x;
		if (y != anInt140)
			verticesY[id] = y;
		if (z != anInt140)
			verticexZ[id] = z;
		updateNeeded = 1;
	}
    
	public int method46(int i, int i_72_, int i_73_) {
		int i_74_ = 9999999;
		int i_75_ = 0;
		for (int i_76_ = 0; i_76_ < vertexAmount; i_76_++) {
			int i_77_ = 0;
			if (i != anInt140)
				i_77_ += Math.abs(i - verticesX[i_76_]);
			if (i_72_ != anInt140)
				i_77_ += Math.abs(i_72_ - verticesY[i_76_]);
			if (i_73_ != anInt140)
				i_77_ += Math.abs(i_73_ - verticexZ[i_76_]);
			if (i_77_ < i_74_) {
				i_74_ = i_77_;
				i_75_ = i_76_;
			}
		}
		return i_75_;
	}
    
	public void method47(int i, int i_78_) {
		polygonColors1[i] = i_78_;
	}

	public void method48(int i, int i_79_) {
		polygonColors2[i] = i_79_;
	}
    
    public void method49(int i) {
		int[] is = polygonPoints[i];
		int i_80_ = polygonPointsCount[i];
		int[] is_81_ = new int[i_80_];
		for (int i_82_ = 0; i_82_ < i_80_; i_82_++)
			is_81_[i_82_] = is[i_80_ - i_82_ - 1];
		for (int i_83_ = 0; i_83_ < i_80_; i_83_++)
			is[i_83_] = is_81_[i_83_];
		updateNeeded = 1;
	}
    
	public void method50(int i) {
		int[] is = polygonPoints[i];
		int i_84_ = polygonPointsCount[i];
		int i_85_ = is[0];
		for (int i_86_ = 0; i_86_ < i_84_ - 1; i_86_++)
			is[i_86_] = is[i_86_ + 1];
		is[i_84_ - 1] = i_85_;
		updateNeeded = 1;
	}
    
	public void method51(int i) {
		int[] is = polygonPoints[i];
		int i_87_ = polygonPointsCount[i];
		int i_88_ = is[i_87_ - 1];
		for (int i_89_ = i_87_ - 1; i_89_ > 0; i_89_--)
			is[i_89_] = is[i_89_ - 1];
		is[0] = i_88_;
		updateNeeded = 1;
	}

	public void method52(int i) {
		int polyCount = polygonPointsCount[i];
		if (polyCount > 3) {
			int[] points = polygonPoints[i];
			int i_91_ = verticesX[points[0]];
			int i_92_ = verticesY[points[0]];
			int i_93_ = verticexZ[points[0]];
			int i_94_ = verticesX[points[1]] - i_91_;
			int i_95_ = verticesY[points[1]] - i_92_;
			int i_96_ = verticexZ[points[1]] - i_93_;
			int i_97_ = verticesX[points[2]] - i_91_;
			int i_98_ = verticesY[points[2]] - i_92_;
			int i_99_ = verticexZ[points[2]] - i_93_;
			long l = (long) (i_95_ * i_99_ - i_98_ * i_96_);
			long l_100_ = (long) (i_96_ * i_97_ - i_99_ * i_94_);
			long l_101_ = (long) (i_94_ * i_98_ - i_97_ * i_95_);
			long l_102_ = (long) Math.sqrt((double) (l * l + l_100_ * l_100_ + l_101_ * l_101_));
			if (l_102_ != 0L) {
				for (int i_103_ = 3; i_103_ < polyCount; i_103_++) {
					int i_104_ = points[i_103_];
					long l_105_ = (((long) (verticesX[i_104_] - i_91_) * l + (long) (verticesY[i_104_] - i_92_) * l_100_ + (long) (verticexZ[i_104_] - i_93_) * l_101_) / l_102_);
					if (l_105_ != 0L) {
						verticesX[i_104_] -= l_105_ * l / l_102_;
						verticesY[i_104_] -= l_105_ * l_100_ / l_102_;
						verticexZ[i_104_] -= l_105_ * l_101_ / l_102_;
					}
				}
				updateNeeded = 1;
			}
		}
	}
    
	public void requestAdjustRotate(int pitch, int yaw, int roll) {
		rotatePitch = rotatePitch + pitch & 0xff;
		rotateYaw = rotateYaw + yaw & 0xff;
		rotateRoll = rotateRoll + roll & 0xff;
		method60();
		updateNeeded = 1;
	}

	public void requestSetRotate(int pitch, int yaw, int roll) {
		rotatePitch = pitch & 0xff;
		rotateYaw = yaw & 0xff;
		rotateRoll = roll & 0xff;
		method60();
		updateNeeded = 1;
	}
    
	public void requestAdjustTranslate(int x, int y, int z) {
		translateX += x;
		translateY += y;
		translateZ += z;
		method60();
		updateNeeded = 1;
	}
    
	public void requestSetTranslate(int x, int y, int z) {
		translateX = x;
		translateY = y;
		translateZ = z;
		method60();
		updateNeeded = 1;
	}

	public void requestScale(int x, int y, int z) {
		scaleX = x;
		scaleY = y;
		scaleZ = z;
		method60();
		updateNeeded = 1;
	}
    
	public void method58(int xy, int zy, int xz, int yz, int zx, int yx) {
		scaleXFromY = xy;
		scaleZFromY = zy;
		scaleXFromZ = xz;
		scaleYFromZ = yz;
		scaleZFromX = zx;
		scaleYFromX = yx;
		method60();
		updateNeeded = 1;
	}
    
	public void method59(int i, int i_121_, int i_122_) {
		int i_123_ = translateX - i;
		int i_124_ = translateY - i_121_;
		int i_125_ = translateZ - i_122_;
		for (int i_126_ = 0; i_126_ < vertexAmount; i_126_++) {
			verticesX[i_126_] += i_123_;
			verticesY[i_126_] += i_124_;
			verticexZ[i_126_] += i_125_;
		}
		translateX = i;
		translateY = i_121_;
		translateZ = i_122_;
		updateNeeded = 1;
	}
    
	private void method60() {
		if (scaleXFromY != 256 || scaleZFromY != 256 || scaleXFromZ != 256 || scaleYFromZ != 256 || scaleZFromX != 256 || scaleYFromX != 256)
			transformType = 4;
		else if (scaleX != 256 || scaleY != 256 || scaleZ != 256)
			transformType = 3;
		else if (rotatePitch != 0 || rotateYaw != 0 || rotateRoll != 0)
			transformType = 2;
		else if (translateX != 0 || translateY != 0 || translateZ != 0)
			transformType = 1;
		else
			transformType = 0;
	}
    
	private void translate(int x, int y, int z) {
		for (int id = 0; id < vertexAmount; id++) {
			transformedVerticesX[id] += x;
			transformedVerticesY[id] += y;
			transformedVerticesZ[id] += z;
		}
	}
    
	private void rotateCameraModifiedVertices(int cameraPitch, int cameraYaw, int cameraRoll) {
		for (int vertexId = 0; vertexId < vertexAmount; vertexId++) {
			if (cameraRoll != 0) {
				int rollSin = sinCosLookupTable[cameraRoll];
				int rollCos = sinCosLookupTable[cameraRoll + 256];
				int z = ((cameraModifiedVerticesY[vertexId] * rollSin + cameraModifiedVerticesX[vertexId] * rollCos) >> 15);
				cameraModifiedVerticesY[vertexId] = (cameraModifiedVerticesY[vertexId] * rollCos - cameraModifiedVerticesX[vertexId] * rollSin) >> 15;
				cameraModifiedVerticesX[vertexId] = z;
			}
			if (cameraYaw != 0) {
				int yawSin = sinCosLookupTable[cameraYaw];
				int yawCos = sinCosLookupTable[cameraYaw + 256];
				int z = ((cameraModifiedVerticesZ[vertexId] * yawSin + cameraModifiedVerticesX[vertexId] * yawCos) >> 15);
				cameraModifiedVerticesZ[vertexId] = (cameraModifiedVerticesZ[vertexId] * yawCos - cameraModifiedVerticesX[vertexId] * yawSin) >> 15;
				cameraModifiedVerticesX[vertexId] = z;
			}
			if (cameraPitch != 0) {
				int pitchSin = sinCosLookupTable[cameraPitch];
				int pitchCos = sinCosLookupTable[cameraPitch + 256];
				int y = ((cameraModifiedVerticesY[vertexId] * pitchCos - cameraModifiedVerticesZ[vertexId] * pitchSin) >> 15);
				cameraModifiedVerticesZ[vertexId] = (cameraModifiedVerticesY[vertexId] * pitchSin + cameraModifiedVerticesZ[vertexId] * pitchCos) >> 15;
				cameraModifiedVerticesY[vertexId] = y;
			}
		}
	}
    
	private void rotate(int pitch, int yaw, int roll) {
		for (int vertexId = 0; vertexId < vertexAmount; vertexId++) {
			if (pitch != 0) {
				int pitchSin = sinCosLookupTable[pitch];
				int pitchCos = sinCosLookupTable[pitch + 256];
				int y = ((transformedVerticesY[vertexId] * pitchCos - transformedVerticesZ[vertexId] * pitchSin) >> 15);
				transformedVerticesZ[vertexId] = (transformedVerticesY[vertexId] * pitchSin + transformedVerticesZ[vertexId] * pitchCos) >> 15;
				transformedVerticesY[vertexId] = y;
			}
			if (yaw != 0) {
				int yawSin = sinCosLookupTable[yaw];
				int yawCos = sinCosLookupTable[yaw + 256];
				int x = ((transformedVerticesZ[vertexId] * yawSin + transformedVerticesX[vertexId] * yawCos) >> 15);
				transformedVerticesZ[vertexId] = (transformedVerticesZ[vertexId] * yawCos - transformedVerticesX[vertexId] * yawSin) >> 15;
				transformedVerticesX[vertexId] = x;
			}
			if (roll != 0) {
				int rollSin = sinCosLookupTable[roll];
				int rollCos = sinCosLookupTable[roll + 256];
				int x = ((transformedVerticesY[vertexId] * rollSin + transformedVerticesX[vertexId] * rollCos) >> 15);
				transformedVerticesY[vertexId] = (transformedVerticesY[vertexId] * rollCos - transformedVerticesX[vertexId] * rollSin) >> 15;
				transformedVerticesX[vertexId] = x;
			}
		}
	}
    
	private void scale(int scaleXFromY, int scaleZFromY, int scaleXFromZ, int scaleYFromZ, int scaleZFromX, int scaleYFromX) {
		for (int id = 0; id < vertexAmount; id++) {
			if (scaleXFromY != 0)
				transformedVerticesX[id] += transformedVerticesY[id] * scaleXFromY >> 8;
			if (scaleZFromY != 0)
				transformedVerticesZ[id] += transformedVerticesY[id] * scaleZFromY >> 8;
			if (scaleXFromZ != 0)
				transformedVerticesX[id] += transformedVerticesZ[id] * scaleXFromZ >> 8;
			if (scaleYFromZ != 0)
				transformedVerticesY[id] += transformedVerticesZ[id] * scaleYFromZ >> 8;
			if (scaleZFromX != 0)
				transformedVerticesZ[id] += transformedVerticesX[id] * scaleZFromX >> 8;
			if (scaleYFromX != 0)
				transformedVerticesY[id] += transformedVerticesX[id] * scaleYFromX >> 8;
		}
	}
    
	private void scale(int x, int y, int z) {
		for (int vertexId = 0; vertexId < vertexAmount; vertexId++) {
			transformedVerticesX[vertexId] = transformedVerticesX[vertexId] * x >> 8;
			transformedVerticesY[vertexId] = transformedVerticesY[vertexId] * y >> 8;
			transformedVerticesZ[vertexId] = transformedVerticesZ[vertexId] * z >> 8;
		}
	}
    
	private void calculateBounds() {
		minX = minY = minZ = 999999;
		maxX = maxY = maxZ = -999999;
		for (int polyId = 0; polyId < polygonCount; polyId++) {
			int[] polyPoints = polygonPoints[polyId];
			int polyPoint = polyPoints[0];
			int polyPointCount = polygonPointsCount[polyId];
			int lowestX;
			int highestX = lowestX = transformedVerticesX[polyPoint];
			int lowestY;
			int highestY = lowestY = transformedVerticesY[polyPoint];
			int lowestZ;
			int highestZ = lowestZ = transformedVerticesZ[polyPoint];
			for (int i_171_ = 0; i_171_ < polyPointCount; i_171_++) {
				polyPoint = polyPoints[i_171_];
				if (lowestX > transformedVerticesX[polyPoint])
					lowestX = transformedVerticesX[polyPoint];
				else if (highestX < transformedVerticesX[polyPoint])
					highestX = transformedVerticesX[polyPoint];
				if (lowestY > transformedVerticesY[polyPoint])
					lowestY = transformedVerticesY[polyPoint];
				else if (highestY < transformedVerticesY[polyPoint])
					highestY = transformedVerticesY[polyPoint];
				if (lowestZ > transformedVerticesZ[polyPoint])
					lowestZ = transformedVerticesZ[polyPoint];
				else if (highestZ < transformedVerticesZ[polyPoint])
					highestZ = transformedVerticesZ[polyPoint];
			}
			polygonsMinX[polyId] = lowestX;
			polygonsMaxX[polyId] = highestX;
			polygonsMinY[polyId] = lowestY;
			polygonsMaxY[polyId] = highestY;
			polygonsMinZ[polyId] = lowestZ;
			polygonsMaxZ[polyId] = highestZ;
			if (minX > lowestX)
				minX = lowestX;
			if (maxX < highestX)
				maxX = highestX;
			if (minY > lowestY)
				minY = lowestY;
			if (maxY < highestY)
				maxY = highestY;
			if (minZ > lowestZ)
				minZ = lowestZ;
			if (maxZ < highestZ)
				maxZ = highestZ;
		}
	}
    
	private void calculateNormals() {
		for (int poliId = 0; poliId < polygonCount; poliId++) {
			int[] points = polygonPoints[poliId];
			int point1X = transformedVerticesX[points[0]];
			int point1Y = transformedVerticesY[points[0]];
			int point1Z = transformedVerticesZ[points[0]];
			int x1 = transformedVerticesX[points[1]] - point1X;
			int y1 = transformedVerticesY[points[1]] - point1Y;
			int z1 = transformedVerticesZ[points[1]] - point1Z;
			int x2 = transformedVerticesX[points[2]] - point1X;
			int y2 = transformedVerticesY[points[2]] - point1Y;
			int z2 = transformedVerticesZ[points[2]] - point1Z;
			int normalX = y1 * z2 - y2 * z1;
			int normalY = z1 * x2 - z2 * x1;
			int normalZ = x1 * y2 - x2 * y1;
			for (/**/; (normalX > 8192 || normalY > 8192 || normalZ > 8192 || normalX < -8192 || normalY < -8192 || normalZ < -8192);) {
				normalX >>= 1;
				normalY >>= 1;
				normalZ >>= 1;
			}
			int normalLen = (int) (256.0 * Math.sqrt((double) (normalX * normalX + normalY * normalY + normalZ * normalZ)));
			if (normalLen <= 0)
				normalLen = 1;
			verticesNormalX[poliId] = normalX * 65536 / normalLen;
			verticesNormalY[poliId] = normalY * 65536 / normalLen;
			verticesNormalZ[poliId] = normalZ * 65536 / normalLen;
			anIntArray131[poliId] = anInt140;
			anIntArray132[poliId] = -1;
		}
	}
    
	private void update() {
		if (updateNeeded == 1) {
			updateNeeded = 0;
			for (int id = 0; id < vertexAmount; id++) {
				transformedVerticesX[id] = verticesX[id];
				transformedVerticesY[id] = verticesY[id];
				transformedVerticesZ[id] = verticexZ[id];
			}
			if (transformType >= 2)
				rotate(rotatePitch, rotateYaw, rotateRoll);
			if (transformType >= 3)
				scale(scaleX, scaleY, scaleZ);
			if (transformType >= 4)
				scale(scaleXFromY, scaleZFromY, scaleXFromZ, scaleYFromZ, scaleZFromX, scaleYFromX);
			if (transformType >= 1)
				translate(translateX, translateY, translateZ);
			calculateBounds();
			calculateNormals();
		}
	}
    
	public void update(int cameraX, int cameraY, int cameraZ, int cameraPitch, int cameraYaw, int cameraRoll, int screenWidth, int minZ) {
		update();
		for (int id = 0; id < vertexAmount; id++) {
			cameraModifiedVerticesX[id] = transformedVerticesX[id] - cameraX;
			cameraModifiedVerticesY[id] = transformedVerticesY[id] - cameraY;
			cameraModifiedVerticesZ[id] = transformedVerticesZ[id] - cameraZ;
		}
		rotateCameraModifiedVertices(cameraPitch, cameraYaw, cameraRoll);
		for (int id = 0; id < vertexAmount; id++) {
			if (cameraModifiedVerticesZ[id] >= minZ)
				screenX[id] = ((cameraModifiedVerticesX[id] << screenWidth) / cameraModifiedVerticesZ[id]);
			else
				screenX[id] = cameraModifiedVerticesX[id] << screenWidth;
			if (cameraModifiedVerticesZ[id] >= minZ)
				screenY[id] = ((cameraModifiedVerticesY[id] << screenWidth) / cameraModifiedVerticesZ[id]);
			else
				screenY[id] = cameraModifiedVerticesY[id] << screenWidth;
		}
	}
    
	public void method70() {
		update();
		for (int i = 0; i < vertexAmount; i++) {
			verticesX[i] = transformedVerticesX[i];
			verticesY[i] = transformedVerticesY[i];
			verticexZ[i] = transformedVerticesZ[i];
		}
		translateX = translateY = translateZ = 0;
		rotatePitch = rotateYaw = rotateRoll = 0;
		scaleX = scaleY = scaleZ = 256;
		scaleXFromY = scaleZFromY = scaleXFromZ = scaleYFromZ = scaleZFromX = scaleYFromX = 256;
		transformType = 0;
	}
    
	public boolean method71(int i, int i_194_, int i_195_, int i_196_, int i_197_, int i_198_, int i_199_) {
		update();
		if ((i >= minX || i_196_ >= minX) && (i <= maxX || i_196_ <= maxX) && (i_194_ >= minY || i_197_ >= minY) && (i_194_ <= maxY || i_197_ <= maxY) && (i_195_ >= minZ || i_198_ >= minZ) && (i_195_ <= maxZ || i_198_ <= maxZ)) {
			for (int i_200_ = 0; i_200_ < polygonCount; i_200_++) {
				if ((i > polygonsMinX[i_200_] || i_196_ > polygonsMinX[i_200_]) && (i < polygonsMaxX[i_200_] || i_196_ < polygonsMaxX[i_200_])) {
					int i_201_ = i;
					int i_202_ = i_194_;
					int i_203_ = i_195_;
					int i_204_ = i_196_;
					int i_205_ = i_197_;
					int i_206_ = i_198_;
					if (i_204_ != i_201_) {
						double d = ((double) (i_205_ - i_202_) / (double) (i_204_ - i_201_));
						double d_207_ = ((double) (i_206_ - i_203_) / (double) (i_204_ - i_201_));
						if (i_201_ < polygonsMinX[i_200_]) {
							i_202_ += (double) (polygonsMinX[i_200_] - i_201_) * d;
							i_203_ += (double) (polygonsMinX[i_200_] - i_201_) * d_207_;
							i_201_ = polygonsMinX[i_200_];
						} else if (i_201_ > polygonsMaxX[i_200_]) {
							i_202_ += (double) (polygonsMaxX[i_200_] - i_201_) * d;
							i_203_ += (double) (polygonsMaxX[i_200_] - i_201_) * d_207_;
							i_201_ = polygonsMaxX[i_200_];
						}
						if (i_204_ < polygonsMinX[i_200_]) {
							i_205_ += (double) (polygonsMinX[i_200_] - i_204_) * d;
							i_206_ += (double) (polygonsMinX[i_200_] - i_204_) * d_207_;
							i_204_ = polygonsMinX[i_200_];
						} else if (i_204_ > polygonsMaxX[i_200_]) {
							i_205_ += (double) (polygonsMaxX[i_200_] - i_204_) * d;
							i_206_ += (double) (polygonsMaxX[i_200_] - i_204_) * d_207_;
							i_204_ = polygonsMaxX[i_200_];
						}
					}
					if ((i_202_ >= polygonsMinY[i_200_] || i_205_ >= polygonsMinY[i_200_]) && (i_202_ <= polygonsMaxY[i_200_] || i_205_ <= polygonsMaxY[i_200_])) {
						if (i_205_ != i_202_) {
							double d = ((double) (i_206_ - i_203_) / (double) (i_205_ - i_202_));
							if (i_202_ < polygonsMinY[i_200_]) {
								i_203_ += (double) (polygonsMinY[i_200_] - i_202_) * d;
								i_202_ = polygonsMinY[i_200_];
							} else if (i_202_ > polygonsMaxY[i_200_]) {
								i_203_ += (double) (polygonsMaxY[i_200_] - i_202_) * d;
								i_202_ = polygonsMaxY[i_200_];
							}
							if (i_205_ < polygonsMinY[i_200_]) {
								i_206_ += (double) (polygonsMinY[i_200_] - i_205_) * d;
								i_205_ = polygonsMinY[i_200_];
							} else if (i_205_ > polygonsMaxY[i_200_]) {
								i_206_ += (double) (polygonsMaxY[i_200_] - i_205_) * d;
								i_205_ = polygonsMaxY[i_200_];
							}
						}
						if ((i_203_ >= polygonsMinZ[i_200_] || i_206_ >= polygonsMinZ[i_200_]) && (i_203_ <= polygonsMaxZ[i_200_] || i_206_ <= polygonsMaxZ[i_200_])) {
							int i_208_ = polygonPoints[i_200_][0];
							int i_209_ = transformedVerticesX[i_208_];
							int i_210_ = transformedVerticesY[i_208_];
							int i_211_ = transformedVerticesZ[i_208_];
							int i_212_ = ((i - i_209_) * verticesNormalX[i_200_] + (i_194_ - i_210_) * verticesNormalY[i_200_] + ((i_195_ - i_211_) * verticesNormalZ[i_200_]));
							int i_213_ = ((i_196_ - i_209_) * verticesNormalX[i_200_] + (i_197_ - i_210_) * verticesNormalY[i_200_] + ((i_198_ - i_211_) * verticesNormalZ[i_200_]));
							i_199_ *= 256;
							if (i_212_ <= i_199_ && i_213_ >= -i_199_ || i_212_ >= -i_199_ && i_213_ <= i_199_)
								return true;
						}
					}
				}
			}
		}
		return false;
	}
    
	public boolean method72(int i, int i_214_, int i_215_, int i_216_) {
		update();
		if (i >= minX && i <= maxX && i_214_ >= minY && i_214_ <= maxY && i_215_ >= minZ && i_215_ <= maxZ) {
			for (int i_217_ = 0; i_217_ < polygonCount; i_217_++) {
				if (i >= polygonsMinX[i_217_] && i <= polygonsMaxX[i_217_] && i_214_ >= polygonsMinY[i_217_] && i_214_ <= polygonsMaxY[i_217_] && i_215_ >= polygonsMinZ[i_217_] && i_215_ <= polygonsMinZ[i_217_]) {
					int i_218_ = polygonPoints[i_217_][0];
					int i_219_ = transformedVerticesX[i_218_];
					int i_220_ = transformedVerticesY[i_218_];
					int i_221_ = transformedVerticesZ[i_218_];
					int i_222_ = ((i - i_219_) * verticesNormalX[i_217_] + (i_214_ - i_220_) * verticesNormalY[i_217_] + (i_215_ - i_221_) * verticesNormalZ[i_217_]);
					i_216_ *= 256;
					if (i_222_ >= -i_216_ && i_222_ <= i_216_)
						return true;
				}
			}
		}
		return false;
	}
    
	public object3d copy() {
		object3d[] class1s = new object3d[1];
		class1s[0] = this;
		return new object3d(class1s, 1);
	}
    
	private void write(int value) {
		if (value == anInt140)
			value = 123456;
		value += 0x20000;
		outputBuffer[bufferPos++] = aByteArray137[value >> 12 & 0x3f];
		outputBuffer[bufferPos++] = aByteArray137[value >> 6 & 0x3f];
		outputBuffer[bufferPos++] = aByteArray137[value & 0x3f];
		anInt181++;
		if (anInt181 == 40) {
			anInt181 = 0;
			outputBuffer[bufferPos++] = (byte) 10;
		}
	}

	public int read(byte[] buffer) {
		for (/**/; buffer[bufferPos] == 10 || buffer[bufferPos] == 13; bufferPos++) {
			/* empty */
		}
		int firstValue = anIntArray138[buffer[bufferPos++] & 0xff];
		int secondValue = anIntArray138[buffer[bufferPos++] & 0xff];
		int thirdValue = anIntArray138[buffer[bufferPos++] & 0xff];
		int finalValue = (firstValue << 12) | (secondValue << 6) | thirdValue;
		finalValue -= 0x20000;
		if (finalValue == 123456)
			finalValue = anInt140;
		return finalValue;
	}
    
	public byte[] encode() {
		method70();
		int i = 3 + vertexAmount * 3 + polygonCount * anInt139;
		for (int i_226_ = 0; i_226_ < polygonCount; i_226_++)
			i += polygonPointsCount[i_226_] + anIntArrayArray150[i_226_].length;
		i *= 3;
		outputBuffer = new byte[i + i / 120];
		bufferPos = 0;
		anInt181 = 0;
		write(i + i / 120 * 2 - 3);
		write(vertexAmount);
		write(polygonCount);
		for (int i_227_ = 0; i_227_ < vertexAmount; i_227_++) {
			write(verticesX[i_227_]);
			write(verticesY[i_227_]);
			write(verticexZ[i_227_]);
		}
		for (int i_228_ = 0; i_228_ < polygonCount; i_228_++) {
			write(polygonPointsCount[i_228_]);
			write(polygonColors1[i_228_]);
			write(polygonColors2[i_228_]);
			write(anIntArrayArray150[i_228_].length);
			for (int i_229_ = 0; i_229_ < polygonPointsCount[i_228_]; i_229_++)
				write(polygonPoints[i_228_][i_229_]);
			for (int i_230_ = 0; i_230_ < anIntArrayArray150[i_228_].length; i_230_++)
				write(anIntArrayArray150[i_228_][i_230_]);
		}
		return outputBuffer;
	}
    
    public object3d[] method77() {
	method70();
	int i = 0;
	for (int i_231_ = 0; i_231_ < polygonCount; i_231_++) {
	    if (anIntArrayArray150[i_231_][0] > i)
		i = anIntArrayArray150[i_231_][0];
	}
	int[] is = new int[++i];
	int[] is_232_ = new int[i];
	for (int i_233_ = 0; i_233_ < i; i_233_++) {
	    is[i_233_] = 0;
	    is_232_[i_233_] = 0;
	}
	for (int i_234_ = 0; i_234_ < polygonCount; i_234_++) {
	    is_232_[anIntArrayArray150[i_234_][0]]++;
	    is[anIntArrayArray150[i_234_][0]] += polygonPointsCount[i_234_];
	}
	object3d[] class1s = new object3d[i];
	for (int i_235_ = 0; i_235_ < i; i_235_++)
	    class1s[i_235_] = new object3d(is[i_235_], is_232_[i_235_], true);
	for (int i_236_ = 0; i_236_ < polygonCount; i_236_++) {
	    object3d class1_237_ = class1s[anIntArrayArray150[i_236_][0]];
	    int[] is_238_ = new int[polygonPointsCount[i_236_]];
	    int[] is_239_ = polygonPoints[i_236_];
	    for (int i_240_ = 0; i_240_ < polygonPointsCount[i_236_]; i_240_++)
		is_238_[i_240_]
		    = class1_237_.addVertex(verticesX[is_239_[i_240_]],
					   verticesY[is_239_[i_240_]],
					   verticexZ[is_239_[i_240_]]);
	    int i_241_ = class1_237_.addPolygon(polygonPointsCount[i_236_], is_238_,
					      polygonColors1[i_236_],
					      polygonColors2[i_236_]);
	    int i_242_ = anIntArrayArray150[i_236_].length - 1;
	    if (i_242_ < 1)
		i_242_ = 1;
	    class1_237_.anIntArrayArray150[i_241_] = new int[i_242_];
	    if (i_242_ > 1) {
		for (int i_243_ = 0; i_243_ < i_242_; i_243_++)
		    class1_237_.anIntArrayArray150[i_241_][i_243_]
			= anIntArrayArray150[i_236_][i_243_ + 1];
	    } else
		class1_237_.anIntArrayArray150[i_241_][0] = i_241_;
	}
	return class1s;
    }
    
    static {
		for (int id = 0; id < 256; id++) {
			sinCosLookupTable[id] = (int) (Math.sin((double) 2 * Math.PI * id / 256) * 32768.0);
			sinCosLookupTable[id + 256] = (int) (Math.cos((double) 2 * Math.PI * id / 256) * 32768.0);
		}
		
		for (int i = 0; i < 10; i++)
			aByteArray137[i] = (byte) (48 + i);
		for (int i = 0; i < 26; i++)
			aByteArray137[i + 10] = (byte) (65 + i);
		for (int i = 0; i < 26; i++)
			aByteArray137[i + 36] = (byte) (97 + i);
		aByteArray137[62] = (byte) 163;
		aByteArray137[63] = (byte) 36;
		
		for (int i = 0; i < 10; i++)
			anIntArray138[48 + i] = i;
		for (int i = 0; i < 26; i++)
			anIntArray138[65 + i] = i + 10;
		for (int i = 0; i < 26; i++)
			anIntArray138[97 + i] = i + 36;
		anIntArray138[163] = 62;
		anIntArray138[36] = 63;
    }
}
