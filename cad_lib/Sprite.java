/* Image_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package cad_lib;

import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.awt.image.PixelGrabber;

public class Sprite extends Image implements ImageObserver {
	private int width;
	private int height;
	private int[] pixels;
	private Image image;

	public Sprite(Image img) {
		image = img;
		width = image.getWidth(this);
		height = image.getHeight(this);
		pixels = new int[width * height];
		PixelGrabber pixelgrabber = new PixelGrabber(image, 0, 0, width, height, pixels, 0, width);
		try {
			pixelgrabber.grabPixels();
		} catch (InterruptedException interruptedexception) {
			throw new RuntimeException("Unable to allocated Image " + img + " To Sprite");
		}
	}

	public Sprite(String string, Applet applet) {
		image = applet.getImage(applet.getCodeBase(), string);
		MediaTracker mediatracker = new MediaTracker(applet);
		mediatracker.addImage(image, 0);
		try {
			mediatracker.waitForID(0);
		} catch (InterruptedException interruptedexception) {
			throw new RuntimeException("Unable to allocated Image " + string + " To Sprite");
		}
		width = image.getWidth(this);
		height = image.getHeight(this);
		pixels = new int[width * height];
		PixelGrabber pixelgrabber = new PixelGrabber(image, 0, 0, width, height, pixels, 0, width);
		try {
			pixelgrabber.grabPixels();
		} catch (InterruptedException interruptedexception) {
			throw new RuntimeException("Unable to allocated Image " + string + " To Sprite");
		}
	}

	public int getWidth(ImageObserver imageobserver) {
		return width;
	}

	public int getHeight(ImageObserver imageobserver) {
		return height;
	}

	public int getPixel(int x, int y) {
		if (x < 0 || y < 0 || x >= width || y >= height)
			return 0;
		return pixels[x + y * width];
	}

	public boolean imageUpdate(Image image, int infoFlags, int x, int y, int width, int height) {
		return true;
	}

	public ImageProducer getSource() {
		return image.getSource();
	}

	public Graphics getGraphics() {
		return image.getGraphics();
	}

	public Object getProperty(String string, ImageObserver imageobserver) {
		return image.getProperty(string, imageobserver);
	}

	public void flush() {
		image.flush();
	}
}
