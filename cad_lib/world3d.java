/* Class2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package cad_lib;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.io.DataInputStream;
import java.io.InputStream;
import java.net.URL;

public class world3d {
	public int screenWidth = 9;//2^9=512
	public int minZ = 5;
	public int maxZ = 1000;
	public int minMaxX = 256;
	public int minMaxY = 192;
	public int anInt187 = 290;
	public int anInt188 = 4;
	public int anInt189 = 180;
	public int anInt190 = 155;
	public int anInt191 = 95;
	public int anInt192 = 1024;
	public int anInt193 = 2;
	public int[] sinCosLookupTable = new int[512];
	private int anInt195 = 4;
	private int cameraX;
	private int cameraY;
	private int cameraZ;
	private int cameraPitch;
	private int cameraYaw;
	private int cameraRoll;
	private int objectsPos;
	private int maxObjectCount;
	private object3d[] objects;
	private object3d[] onScreenObjects;
	private int onScreenObjectCount;
	private int[] onScreenObjectPolygonId;
	private int[] anIntArray208;
	private int[] objectsNormalX;
	private int[] objectsNormalY;
	private int[] objectsNormalZ;
	private int[] anIntArray212;
	private int[] onScreenObjectsPolygonColor;
	private int[] anIntArray214;
	private int[] objectsLowestZ;
	private int[] objectsHighestZ;
	private int anInt217;
	private int[] anIntArray218;
	private int[][] anIntArrayArray219;
	private int[][] anIntArrayArray220;
	private int[][][][] anIntArrayArrayArrayArray221;
	private Color[][] aColorArrayArray222;
	private Color[][] aColorArrayArray223;
	private Color[] aColorArray224;
	private int anInt225 = 12345678;
	private int anInt226;

	public world3d(int i, int i_0_, int i_1_) {
		objectsPos = 0;
		maxObjectCount = i;
		objects = new object3d[maxObjectCount];
		onScreenObjectCount = 0;
		onScreenObjects = new object3d[i_0_];
		onScreenObjectPolygonId = new int[i_0_];
		anIntArray208 = new int[i_0_];
		objectsNormalX = new int[i_0_];
		objectsNormalY = new int[i_0_];
		objectsNormalZ = new int[i_0_];
		anIntArray212 = new int[i_0_];
		onScreenObjectsPolygonColor = new int[i_0_];
		anIntArray214 = new int[i_0_];
		objectsLowestZ = new int[i_0_];
		objectsHighestZ = new int[i_0_];
		cameraX = 0;
		cameraY = 0;
		cameraZ = 0;
		cameraPitch = 0;
		cameraYaw = 0;
		cameraRoll = 0;
		screenWidth = 9;
		anInt217 = 0;
		anIntArray218 = new int[i_1_];
		anIntArrayArray219 = new int[i_1_][];
		anIntArrayArray220 = new int[i_1_][];
		anIntArrayArrayArrayArray221 = new int[i_1_][][][];
		aColorArrayArray222 = new Color[i_1_][];
		aColorArrayArray223 = new Color[i_1_][];
		aColorArray224 = new Color[i_1_];
		
		for (int id = 0; id < 256; id++) {
			sinCosLookupTable[id] = (int) (Math.sin((double) 2 * Math.PI * id / 256) * 32768.0);
			sinCosLookupTable[id + 256] = (int) (Math.cos((double) 2 * Math.PI * id / 256) * 32768.0);
		}
		
	}

	public void addObject3d(object3d object3d) {
		if (objectsPos < maxObjectCount)
			objects[objectsPos++] = object3d;
	}

	public void removeObject(object3d class1) {
		for (int id = 0; id < objectsPos; id++) {
			if (objects[id] == class1) {
				objectsPos--;
				for (int id2 = id; id2 < objectsPos; id2++)
					objects[id2] = objects[id2 + 1];
			}
		}
	}

	public void clearObjects() {
		for (int id = 0; id < objectsPos; id++)
			objects[id] = null;
		objectsPos = 0;
	}

	public void method81() {
		if (objectsPos != 0) {
			for (int i = 0; i < objectsPos; i++)
				objects[i].update(0, 0, 0, 0, 0, 0, screenWidth, minZ);
		}
	}

	public void method82(Graphics graphics, int i, int i_4_) {
		if (objectsPos != 0) {
			graphics.setColor(Color.yellow);
			for (int i_5_ = 0; i_5_ < objectsPos; i_5_++) {
				object3d class1 = objects[i_5_];
				if (class1.anInt134 == 0) {
					for (int i_6_ = 0; i_6_ < class1.polygonCount; i_6_++) {
						int i_7_ = class1.polygonPointsCount[i_6_];
						int[] is = class1.polygonPoints[i_6_];
						for (int i_8_ = 0; i_8_ < i_7_; i_8_++) {
							int i_9_;
							int i_10_;
							if (i_8_ == i_7_ - 1) {
								i_9_ = is[i_8_];
								i_10_ = is[0];
							} else {
								i_9_ = is[i_8_];
								i_10_ = is[i_8_ + 1];
							}
							if (i == 0)
								graphics.drawLine((class1.cameraModifiedVerticesZ[i_9_] - cameraZ << 8) / i_4_, (class1.cameraModifiedVerticesY[i_9_] - cameraY << 8) / i_4_, (class1.cameraModifiedVerticesZ[i_10_] - cameraZ << 8) / i_4_, (class1.cameraModifiedVerticesY[i_10_] - cameraY << 8) / i_4_);
							if (i == 1)
								graphics.drawLine((class1.cameraModifiedVerticesX[i_9_] - cameraX << 8) / i_4_, (class1.cameraModifiedVerticesY[i_9_] - cameraY << 8) / i_4_, (class1.cameraModifiedVerticesX[i_10_] - cameraX << 8) / i_4_, (class1.cameraModifiedVerticesY[i_10_] - cameraY << 8) / i_4_);
							if (i == 2)
								graphics.drawLine((class1.cameraModifiedVerticesX[i_9_] - cameraX << 8) / i_4_, (cameraZ - class1.cameraModifiedVerticesZ[i_9_] << 8) / i_4_, (class1.cameraModifiedVerticesX[i_10_] - cameraX << 8) / i_4_, (cameraZ - class1.cameraModifiedVerticesZ[i_10_] << 8) / i_4_);
						}
					}
				}
			}
			for (int i_11_ = 0; i_11_ < objectsPos; i_11_++) {
				object3d class1 = objects[i_11_];
				if (class1.anInt134 == 1) {
					for (int i_12_ = 0; i_12_ < class1.polygonCount; i_12_++) {
						if (class1.polygonCount > 1)
							graphics.setColor(Color.green);
						else
							graphics.setColor(Color.magenta);
						int i_13_ = class1.polygonPointsCount[i_12_];
						int[] is = class1.polygonPoints[i_12_];
						for (int i_14_ = 0; i_14_ < i_13_; i_14_++) {
							int i_15_;
							int i_16_;
							if (i_14_ == i_13_ - 1) {
								i_15_ = is[i_14_];
								i_16_ = is[0];
							} else {
								i_15_ = is[i_14_];
								i_16_ = is[i_14_ + 1];
							}
							if (i == 0)
								graphics.drawLine((class1.cameraModifiedVerticesZ[i_15_] - cameraZ << 8) / i_4_, (class1.cameraModifiedVerticesY[i_15_] - cameraY << 8) / i_4_, (class1.cameraModifiedVerticesZ[i_16_] - cameraZ << 8) / i_4_, (class1.cameraModifiedVerticesY[i_16_] - cameraY << 8) / i_4_);
							if (i == 1)
								graphics.drawLine((class1.cameraModifiedVerticesX[i_15_] - cameraX << 8) / i_4_, (class1.cameraModifiedVerticesY[i_15_] - cameraY << 8) / i_4_, (class1.cameraModifiedVerticesX[i_16_] - cameraX << 8) / i_4_, (class1.cameraModifiedVerticesY[i_16_] - cameraY << 8) / i_4_);
							if (i == 2)
								graphics.drawLine((class1.cameraModifiedVerticesX[i_15_] - cameraX << 8) / i_4_, (cameraZ - class1.cameraModifiedVerticesZ[i_15_] << 8) / i_4_, (class1.cameraModifiedVerticesX[i_16_] - cameraX << 8) / i_4_, (cameraZ - class1.cameraModifiedVerticesZ[i_16_] << 8) / i_4_);
						}
					}
				}
			}
		}
	}

	private void method83(Graphics graphics, Color color, int i, int i_17_) {
		graphics.setColor(color);
		graphics.drawLine(i - 3, i_17_ - 3, i + 3, i_17_ + 3);
		graphics.drawLine(i + 3, i_17_ - 3, i - 3, i_17_ + 3);
	}

	public void method84(Graphics graphics, Graphics graphics_18_, Graphics graphics_19_, object3d class1, int i) {
		Color color = Color.white;
		for (int i_20_ = 0; i_20_ < class1.polygonCount; i_20_++) {
			int i_21_ = class1.polygonPointsCount[i_20_];
			int[] is = class1.polygonPoints[i_20_];
			for (int i_22_ = 0; i_22_ < i_21_; i_22_++) {
				int i_23_ = is[i_22_];
				method83(graphics, color, (class1.cameraModifiedVerticesZ[i_23_] - cameraZ << 8) / i, (class1.cameraModifiedVerticesY[i_23_] - cameraY << 8) / i);
				method83(graphics_18_, color, (class1.cameraModifiedVerticesX[i_23_] - cameraX << 8) / i, (class1.cameraModifiedVerticesY[i_23_] - cameraY << 8) / i);
				method83(graphics_19_, color, (class1.cameraModifiedVerticesX[i_23_] - cameraX << 8) / i, (cameraZ - class1.cameraModifiedVerticesZ[i_23_] << 8) / i);
			}
		}
	}

	public void render(Graphics graphics) {
		if (objectsPos != 0) {
			for (int objectId = 0; objectId < objectsPos; objectId++) {
				objects[objectId].update(cameraX, cameraY, cameraZ, cameraPitch, cameraYaw, cameraRoll, screenWidth, minZ);
			}
			onScreenObjectCount = 0;
			for (int objectId = 0; objectId < objectsPos; objectId++) {
				object3d object = objects[objectId];
				for (int polyId = 0; polyId < object.polygonCount; polyId++) {
					int polyPointsCount = object.polygonPointsCount[polyId];
					int[] polyPoints = object.polygonPoints[polyId];
					boolean minBoundary = false;
					boolean maxBoundary = false;
					for (int polyPointId = 0; polyPointId < polyPointsCount; polyPointId++) {
						int z = object.cameraModifiedVerticesZ[polyPoints[polyPointId]];
						if (z > minZ)
							minBoundary = true;
						if (z < maxZ)
							maxBoundary = true;
					}
					if (minBoundary && maxBoundary) {
						minBoundary = false;
						maxBoundary = false;
						for (int polyPointId = 0; polyPointId < polyPointsCount; polyPointId++) {
							int x = object.screenX[polyPoints[polyPointId]];
							if (x > -minMaxX)
								minBoundary = true;
							if (x < minMaxX)
								maxBoundary = true;
						}
						if (minBoundary && maxBoundary) {
							minBoundary = false;
							maxBoundary = false;
							for (int polyPointId = 0; polyPointId < polyPointsCount; polyPointId++) {
								int y = object.screenY[polyPoints[polyPointId]];
								if (y > -minMaxY)
									minBoundary = true;
								if (y < minMaxY)
									maxBoundary = true;
							}
							if (minBoundary && maxBoundary) {
								onScreenObjects[onScreenObjectCount] = object;
								onScreenObjectPolygonId[onScreenObjectCount] = polyId;
								method90(onScreenObjectCount);
								if (anIntArray212[onScreenObjectCount] < 0)
									onScreenObjectsPolygonColor[onScreenObjectCount] = object.polygonColors1[polyId];
								else
									onScreenObjectsPolygonColor[onScreenObjectCount] = object.polygonColors2[polyId];
								if (onScreenObjectsPolygonColor[onScreenObjectCount] != anInt225) {
									int totalDepth = 0;
									for (int id = 0; id < polyPointsCount; id++)
										totalDepth += object.cameraModifiedVerticesZ[polyPoints[id]];
									int averageDepth;
									anIntArray208[onScreenObjectCount] = averageDepth = totalDepth / polyPointsCount;
									anIntArray208[onScreenObjectCount] -= object.zOffset;
									averageDepth = anInt187 - averageDepth / anInt188;
									if (averageDepth > 200)
										averageDepth = 200;
									else if (averageDepth < 0)
										averageDepth = 0;
									if (object.anIntArray131[polyId] == anInt225)
										object.anIntArray131[polyId] = ((object.verticesNormalX[polyId] * anInt189) + (object.verticesNormalY[polyId] * anInt190) + (object.verticesNormalZ[polyId] * anInt191)) / anInt192;
									averageDepth += object.anIntArray131[polyId];
									if (averageDepth > 256)
										averageDepth = 256;
									else if (averageDepth < 0)
										averageDepth = 0;
									anIntArray214[onScreenObjectCount++] = averageDepth;
								}
							}
						}
					}
				}
			}
			if (onScreenObjectCount != 0) {
				int[] is = new int[onScreenObjectCount];
				for (int i = 0; i < onScreenObjectCount; i++) {
					int i_36_ = -5000;
					for (int onScreenObjectId = 0; onScreenObjectId < onScreenObjectCount; onScreenObjectId++) {
						if (i_36_ < anIntArray208[onScreenObjectId]) {
							i_36_ = anIntArray208[onScreenObjectId];
							is[i] = onScreenObjectId;
						}
					}
					anIntArray208[is[i]] = -5000;
				}
				for (int loopCount = 0; loopCount < anInt193; loopCount++) {
					for (int i_38_ = 1; i_38_ <= 4; i_38_++) {
						for (int i_39_ = 0; i_39_ < onScreenObjectCount - i_38_; i_39_++) {
							if (!method91(is[i_39_], is[i_39_ + i_38_])) {
								int i_40_ = i_38_;
								int i_41_ = is[i_39_ + i_38_];
								for (int i_42_ = i_38_ - 1; i_42_ >= 0; i_42_--) {
									if (!method91(i_41_, is[i_39_ + i_42_]))
										break;
									is[i_39_ + i_42_ + 1] = is[i_39_ + i_42_];
									is[i_39_ + i_42_] = i_41_;
									i_40_ = i_42_;
								}
								i_41_ = is[i_39_];
								for (int i_43_ = 1; i_43_ <= i_40_; i_43_++) {
									if (!method91(is[i_39_ + i_43_], i_41_))
										break;
									is[i_39_ + i_43_ - 1] = is[i_39_ + i_43_];
									is[i_39_ + i_43_] = i_41_;
								}
							}
						}
					}
				}
				for (int i = 0; i < onScreenObjectCount; i++) {
					int i_44_ = is[i];
					object3d class1 = onScreenObjects[i_44_];
					int i_45_ = onScreenObjectPolygonId[i_44_];
					int i_46_ = 0;
					int i_47_ = class1.polygonPointsCount[i_45_];
					int[] is_48_ = class1.polygonPoints[i_45_];
					int[] is_49_ = new int[i_47_ + 10];
					int[] is_50_ = new int[i_47_ + 10];
					for (int i_51_ = 0; i_51_ < i_47_; i_51_++) {
						int i_52_ = is_48_[i_51_];
						if (class1.cameraModifiedVerticesZ[i_52_] >= minZ) {
							is_49_[i_46_] = class1.screenX[i_52_];
							is_50_[i_46_] = class1.screenY[i_52_];
							i_46_++;
						} else {
							int i_53_;
							if (i_51_ == 0)
								i_53_ = is_48_[i_47_ - 1];
							else
								i_53_ = is_48_[i_51_ - 1];
							if (class1.cameraModifiedVerticesZ[i_53_] >= minZ) {
								int i_54_ = (class1.cameraModifiedVerticesZ[i_52_] - class1.cameraModifiedVerticesZ[i_53_]);
								int i_55_ = (class1.cameraModifiedVerticesX[i_52_] - ((class1.cameraModifiedVerticesX[i_52_] - class1.cameraModifiedVerticesX[i_53_]) * (class1.cameraModifiedVerticesZ[i_52_] - minZ) / i_54_));
								int i_56_ = (class1.cameraModifiedVerticesY[i_52_] - ((class1.cameraModifiedVerticesY[i_52_] - class1.cameraModifiedVerticesY[i_53_]) * (class1.cameraModifiedVerticesZ[i_52_] - minZ) / i_54_));
								is_49_[i_46_] = (i_55_ << screenWidth) / minZ;
								is_50_[i_46_] = (i_56_ << screenWidth) / minZ;
								i_46_++;
							}
							if (i_51_ == i_47_ - 1)
								i_53_ = is_48_[0];
							else
								i_53_ = is_48_[i_51_ + 1];
							if (class1.cameraModifiedVerticesZ[i_53_] >= minZ) {
								int i_57_ = (class1.cameraModifiedVerticesZ[i_52_] - class1.cameraModifiedVerticesZ[i_53_]);
								int i_58_ = (class1.cameraModifiedVerticesX[i_52_] - ((class1.cameraModifiedVerticesX[i_52_] - class1.cameraModifiedVerticesX[i_53_]) * (class1.cameraModifiedVerticesZ[i_52_] - minZ) / i_57_));
								int i_59_ = (class1.cameraModifiedVerticesY[i_52_] - ((class1.cameraModifiedVerticesY[i_52_] - class1.cameraModifiedVerticesY[i_53_]) * (class1.cameraModifiedVerticesZ[i_52_] - minZ) / i_57_));
								is_49_[i_46_] = (i_58_ << screenWidth) / minZ;
								is_50_[i_46_] = (i_59_ << screenWidth) / minZ;
								i_46_++;
							}
						}
					}
					if (onScreenObjectsPolygonColor[i_44_] < 0) {
						for (int i_60_ = 0; i_60_ < i_46_; i_60_++) {
							for (/**/; (is_50_[i_60_] > 32767 || is_49_[i_60_] > 32767 || is_49_[i_60_] < -32767 || is_50_[i_60_] < -32767); is_49_[i_60_] >>= 1)
								is_50_[i_60_] >>= 1;
						}
						graphics.setColor(transformColor(-onScreenObjectsPolygonColor[i_44_] - 1, anIntArray214[i_44_], onScreenObjects[i_44_]));
						graphics.fillPolygon(is_49_, is_50_, i_46_);
					}
					if (onScreenObjectsPolygonColor[i_44_] >= 0 && onScreenObjectsPolygonColor[i_44_] != anInt225) {
						int i_61_ = onScreenObjectsPolygonColor[i_44_];
						if (aColorArray224[i_61_] != null) {
							for (int i_62_ = 0; i_62_ < i_46_; i_62_++) {
								for (/**/; (is_50_[i_62_] > 32767 || is_49_[i_62_] > 32767 || is_49_[i_62_] < -32767 || is_50_[i_62_] < -32767); is_49_[i_62_] >>= 1)
									is_50_[i_62_] >>= 1;
							}
							graphics.setColor(transformColor(aColorArray224[i_61_], anIntArray214[i_44_], onScreenObjects[i_44_]));
							graphics.fillPolygon(is_49_, is_50_, i_46_);
						}
						if (i_47_ == 4 || i_47_ == 3) {
							for (int i_63_ = 0; i_63_ < anIntArray218[i_61_]; i_63_++) {
								int i_64_ = anIntArrayArray219[i_61_][i_63_];
								int textureType = anIntArrayArray220[i_61_][i_63_];
								int[][] is_66_ = (anIntArrayArrayArrayArray221[i_61_][i_63_]);
								Color color = aColorArrayArray222[i_61_][i_63_];
								Color color_67_ = aColorArrayArray223[i_61_][i_63_];
								int[] is_68_ = new int[i_64_];
								int[] is_69_ = new int[i_64_];
								int[] is_70_ = new int[i_64_];
								for (int i_71_ = 0; i_71_ < i_64_; i_71_++) {
									int i_72_ = 0;
									int i_73_ = 0;
									int i_74_ = 0;
									int i_75_ = 0;
									for (int i_76_ = 0; i_76_ < 4; i_76_++) {
										int i_77_ = is_66_[i_71_][i_76_];
										if (i_76_ < i_47_)
											i_75_ = is_48_[i_76_];
										i_72_ += i_77_ * (class1.cameraModifiedVerticesX[i_75_]);
										i_73_ += i_77_ * (class1.cameraModifiedVerticesY[i_75_]);
										i_74_ += i_77_ * (class1.cameraModifiedVerticesZ[i_75_]);
									}
									is_68_[i_71_] = i_72_ >> 8;
									is_69_[i_71_] = i_73_ >> 8;
									is_70_[i_71_] = i_74_ >> 8;
								}
								if (textureType == 0) {
									for (int i_78_ = 0; i_78_ < i_64_ - 1; i_78_++) {
										int i_79_ = is_68_[i_78_];
										int i_80_ = is_69_[i_78_];
										int i_81_ = is_70_[i_78_];
										int i_82_ = is_68_[i_78_ + 1];
										int i_83_ = is_69_[i_78_ + 1];
										int i_84_ = is_70_[i_78_ + 1];
										if (i_81_ >= minZ || i_84_ >= minZ) {
											if (i_81_ >= minZ && i_84_ < minZ) {
												i_82_ -= ((i_82_ - i_79_) * (i_84_ - minZ) / (i_84_ - i_81_));
												i_83_ -= ((i_83_ - i_80_) * (i_84_ - minZ) / (i_84_ - i_81_));
												i_84_ = minZ;
											}
											if (i_81_ < minZ && i_84_ >= minZ) {
												i_79_ -= ((i_79_ - i_82_) * (i_81_ - minZ) / (i_81_ - i_84_));
												i_80_ -= ((i_80_ - i_83_) * (i_81_ - minZ) / (i_81_ - i_84_));
												i_81_ = minZ;
											}
											int i_85_ = (i_79_ << screenWidth) / i_81_;
											int i_86_ = (i_80_ << screenWidth) / i_81_;
											int i_87_ = (i_82_ << screenWidth) / i_84_;
											int i_88_ = (i_83_ << screenWidth) / i_84_;
											for (; (i_86_ > 32768 || i_85_ > 32768 || i_85_ < -32768 || i_86_ < -32768); i_86_ >>= 1)
												i_85_ >>= 1;
											for (; (i_88_ > 32768 || i_87_ > 32768 || i_87_ < -32768 || i_88_ < -32768); i_88_ >>= 1)
												i_87_ >>= 1;
											if (color_67_ != null) {
												graphics.setColor(transformColor(color_67_, anIntArray214[i_44_], onScreenObjects[i_44_]));
												graphics.drawLine(i_85_, i_86_, i_87_, i_88_);
												graphics.setColor(transformColor(color, anIntArray214[i_44_], onScreenObjects[i_44_]));
												graphics.drawLine(i_85_ + 1, i_86_ + 1, i_87_ + 1, i_88_ + 1);
											} else {
												graphics.setColor(transformColor(color, anIntArray214[i_44_], onScreenObjects[i_44_]));
												graphics.drawLine(i_85_, i_86_, i_87_, i_88_);
											}
										}
									}
								} else if (textureType == 1) {
									int[] is_89_ = new int[i_64_ + 10];
									int[] is_90_ = new int[i_64_ + 10];
									i_46_ = 0;
									for (int i_91_ = 0; i_91_ < i_64_; i_91_++) {
										if (is_70_[i_91_] >= minZ) {
											is_89_[i_46_] = ((is_68_[i_91_] << screenWidth) / is_70_[i_91_]);
											is_90_[i_46_] = ((is_69_[i_91_] << screenWidth) / is_70_[i_91_]);
											i_46_++;
										} else {
											int i_92_ = i_91_;
											int i_93_;
											if (i_91_ == 0)
												i_93_ = i_64_ - 1;
											else
												i_93_ = i_91_ - 1;
											if (is_70_[i_93_] >= minZ) {
												int i_94_ = (is_70_[i_92_] - is_70_[i_93_]);
												int i_95_ = (is_68_[i_92_] - ((is_68_[i_92_] - is_68_[i_93_]) * (is_70_[i_92_] - minZ) / i_94_));
												int i_96_ = (is_69_[i_92_] - ((is_69_[i_92_] - is_69_[i_93_]) * (is_70_[i_92_] - minZ) / i_94_));
												is_89_[i_46_] = ((i_95_ << screenWidth) / minZ);
												is_90_[i_46_] = ((i_96_ << screenWidth) / minZ);
												i_46_++;
											}
											if (i_91_ == i_64_ - 1)
												i_93_ = 0;
											else
												i_93_ = i_91_ + 1;
											if (is_70_[i_93_] >= minZ) {
												int i_97_ = (is_70_[i_92_] - is_70_[i_93_]);
												int i_98_ = (is_68_[i_92_] - ((is_68_[i_92_] - is_68_[i_93_]) * (is_70_[i_92_] - minZ) / i_97_));
												int i_99_ = (is_69_[i_92_] - ((is_69_[i_92_] - is_69_[i_93_]) * (is_70_[i_92_] - minZ) / i_97_));
												is_89_[i_46_] = ((i_98_ << screenWidth) / minZ);
												is_90_[i_46_] = ((i_99_ << screenWidth) / minZ);
												i_46_++;
											}
										}
									}
									if (i_46_ > 2) {
										graphics.setColor(transformColor(color, anIntArray214[i_44_], onScreenObjects[i_44_]));
										for (int i_100_ = 0; i_100_ < i_46_; i_100_++) {
											for (; (is_90_[i_100_] > 32767 || is_89_[i_100_] > 32767 || is_89_[i_100_] < -32767 || is_90_[i_100_] < -32767); is_89_[i_100_] >>= 1)
												is_90_[i_100_] >>= 1;
										}
										if (textureType == 1)
											graphics.fillPolygon(is_89_, is_90_, i_46_);
										if (textureType == 2)
											graphics.drawPolygon(is_89_, is_90_, i_46_);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private Color transformColor(Color color, int colorModifier, object3d object3d) {
		if (color == null)
			return null;
		if (color.equals(object3d.originalColor))
			color = object3d.modifiedColor;
		int red = color.getRed();
		int green = color.getGreen();
		int blue = color.getBlue();
		red = red * colorModifier >> 8;
		green = green * colorModifier >> 8;
		blue = blue * colorModifier >> 8;
		return new Color(red, green, blue);
	}

	private Color transformColor(int colorRgb, int colorModifier, object3d object3d) {
		if (colorRgb == object3d.originalColorRgb)
			colorRgb = object3d.modifiedColorRgb;
		int red = colorRgb >> 10 & 0x1f;
		int green = colorRgb >> 5 & 0x1f;
		int blue = colorRgb & 0x1f;
		red = red * colorModifier >> 5;
		green = green * colorModifier >> 5;
		blue = blue * colorModifier >> 5;
		return new Color(red, green, blue);
	}

	public void setCamera(int camX, int camY, int camZ, int camPitch, int camYaw, int camRoll) {
		cameraX = camX;
		cameraY = camY;
		cameraZ = camZ;
		cameraPitch = camPitch;
		cameraYaw = camYaw;
		cameraRoll = camRoll;
	}

	public void setCamera(int camX, int camY, int camZ, int camPitch, int camYaw, int camRoll, int zoom) {
		cameraPitch = (256 - camPitch) % 256;
		cameraYaw = (256 - camYaw) % 256;
		cameraRoll = (256 - camRoll) % 256;
		int xOff = 0;
		int yOff = 0;
		int zOff = zoom;
		if (camPitch != 0) {
			int pitchSin = sinCosLookupTable[camPitch];
			int pitchCos = sinCosLookupTable[camPitch + 256];
			int y = yOff * pitchCos - zOff * pitchSin >> 15;
			zOff = yOff * pitchSin + zOff * pitchCos >> 15;
			yOff = y;
		}
		if (camYaw != 0) {
			int yawSin = sinCosLookupTable[camYaw];
			int yawCos = sinCosLookupTable[camYaw + 256];
			int x = zOff * yawSin + xOff * yawCos >> 15;
			zOff = zOff * yawCos - xOff * yawSin >> 15;
			xOff = x;
		}
		if (camRoll != 0) {
			int rollSin = sinCosLookupTable[camRoll];
			int rollCos = sinCosLookupTable[camRoll + 256];
			int x = yOff * rollSin + xOff * rollCos >> 15;
			yOff = yOff * rollCos - xOff * rollSin >> 15;
			xOff = x;
		}
		cameraX = camX - xOff;
		cameraY = camY - yOff;
		cameraZ = camZ - zOff;
	}

	private void method90(int object3dId) {
		object3d object3d = onScreenObjects[object3dId];
		int polygonId = onScreenObjectPolygonId[object3dId];
		int[] points = object3d.polygonPoints[polygonId];
		int polygonCount = object3d.polygonPointsCount[polygonId];
		int i_134_ = object3d.anIntArray132[polygonId];
		int point1X = object3d.cameraModifiedVerticesX[points[0]];
		int point1Y = object3d.cameraModifiedVerticesY[points[0]];
		int point1Z = object3d.cameraModifiedVerticesZ[points[0]];
		int x1 = object3d.cameraModifiedVerticesX[points[1]] - point1X;
		int y1 = object3d.cameraModifiedVerticesY[points[1]] - point1Y;
		int z1 = object3d.cameraModifiedVerticesZ[points[1]] - point1Z;
		int x2 = object3d.cameraModifiedVerticesX[points[2]] - point1X;
		int y2 = object3d.cameraModifiedVerticesY[points[2]] - point1Y;
		int z2 = object3d.cameraModifiedVerticesZ[points[2]] - point1Z;
		int normalX = y1 * z2 - y2 * z1;
		int normalY = z1 * x2 - z2 * x1;
		int normalZ = x1 * y2 - x2 * y1;
		if (i_134_ == -1) {
			i_134_ = 0;
			for (/**/; (normalX > 25000 || normalY > 25000 || normalZ > 25000 || normalX < -25000 || normalY < -25000 || normalZ < -25000);) {
				i_134_++;
				normalX >>= 1;
				normalY >>= 1;
				normalZ >>= 1;
			}
			object3d.anIntArray132[polygonId] = i_134_;
			object3d.anIntArray133[polygonId] = (int) ((double) anInt195 * Math.sqrt((double) (normalX * normalX + normalY * normalY + normalZ * normalZ)));
		} else {
			normalX >>= i_134_;
			normalY >>= i_134_;
			normalZ >>= i_134_;
		}
		anIntArray212[object3dId] = point1X * normalX + point1Y * normalY + point1Z * normalZ;
		objectsNormalX[object3dId] = normalX;
		objectsNormalY[object3dId] = normalY;
		objectsNormalZ[object3dId] = normalZ;
		int lowestZ = object3d.cameraModifiedVerticesZ[points[0]];
		int highestZ = lowestZ;
		for (int polyId = 1; polyId < polygonCount; polyId++) {
			int z = object3d.cameraModifiedVerticesZ[points[polyId]];
			if (z > highestZ)
				highestZ = z;
			else if (z < lowestZ)
				lowestZ = z;
		}
		objectsLowestZ[object3dId] = lowestZ;
		objectsHighestZ[object3dId] = highestZ;
	}

	private boolean method91(int i, int i_151_) {
		if (objectsLowestZ[i] > objectsHighestZ[i_151_])
			return true;
		if (objectsLowestZ[i_151_] > objectsHighestZ[i])
			return false;
		object3d class1 = onScreenObjects[i];
		object3d class1_152_ = onScreenObjects[i_151_];
		int i_153_ = onScreenObjectPolygonId[i];
		int i_154_ = onScreenObjectPolygonId[i_151_];
		int[] is = class1.polygonPoints[i_153_];
		int[] is_155_ = class1_152_.polygonPoints[i_154_];
		int i_156_ = class1.polygonPointsCount[i_153_];
		int i_157_ = class1_152_.polygonPointsCount[i_154_];
		int i_158_ = class1_152_.cameraModifiedVerticesX[is_155_[0]];
		int i_159_ = class1_152_.cameraModifiedVerticesY[is_155_[0]];
		int i_160_ = class1_152_.cameraModifiedVerticesZ[is_155_[0]];
		int i_161_ = objectsNormalX[i_151_];
		int i_162_ = objectsNormalY[i_151_];
		int i_163_ = objectsNormalZ[i_151_];
		int i_164_ = class1_152_.anIntArray133[i_154_];
		int i_165_ = anIntArray212[i_151_];
		boolean bool = false;
		for (int i_166_ = 0; i_166_ < i_156_; i_166_++) {
			int i_167_ = is[i_166_];
			int i_168_ = ((i_158_ - class1.cameraModifiedVerticesX[i_167_]) * i_161_ + (i_159_ - class1.cameraModifiedVerticesY[i_167_]) * i_162_ + (i_160_ - class1.cameraModifiedVerticesZ[i_167_]) * i_163_);
			if (i_168_ < -i_164_ && i_165_ < 0 || i_168_ > i_164_ && i_165_ > 0) {
				bool = true;
				break;
			}
		}
		if (!bool)
			return true;
		i_158_ = class1.cameraModifiedVerticesX[is[0]];
		i_159_ = class1.cameraModifiedVerticesY[is[0]];
		i_160_ = class1.cameraModifiedVerticesZ[is[0]];
		i_161_ = objectsNormalX[i];
		i_162_ = objectsNormalY[i];
		i_163_ = objectsNormalZ[i];
		i_164_ = class1.anIntArray133[i_153_];
		i_165_ = anIntArray212[i];
		bool = false;
		for (int i_169_ = 0; i_169_ < i_157_; i_169_++) {
			int i_170_ = is_155_[i_169_];
			int i_171_ = ((i_158_ - class1_152_.cameraModifiedVerticesX[i_170_]) * i_161_ + (i_159_ - class1_152_.cameraModifiedVerticesY[i_170_]) * i_162_ + (i_160_ - class1_152_.cameraModifiedVerticesZ[i_170_]) * i_163_);
			if (i_171_ < -i_164_ && i_165_ > 0 || i_171_ > i_164_ && i_165_ < 0) {
				bool = true;
				break;
			}
		}
		if (!bool)
			return true;
		return false;
	}

	public int addColors(int vectors, Color color) {
		int i_172_ = anInt217;
		anIntArray218[i_172_] = vectors;
		anIntArrayArray219[i_172_] = new int[vectors];
		anIntArrayArray220[i_172_] = new int[vectors];
		anIntArrayArrayArrayArray221[i_172_] = new int[vectors][][];
		aColorArrayArray222[i_172_] = new Color[vectors];
		aColorArrayArray223[i_172_] = new Color[vectors];
		aColorArray224[i_172_] = color;
		anInt217++;
		return i_172_;
	}

	public void addTextureInfo(int colorId, int vectorId, int type, int size, int[] x, int[] y, Color color1, Color color2) {
		//colorId, vectorId, type, size, x, y, color1, color2
		int[][] is_178_ = new int[size][4];
		for (int id = 0; id < size; id++) {
			int coordX = x[id];
			int coordY = y[id];
			is_178_[id][0] = (256 - coordX) * (256 - coordY) >> 8;
			is_178_[id][1] = coordX * (256 - coordY) >> 8;
			is_178_[id][2] = coordX * coordY >> 8;
			is_178_[id][3] = (256 - coordX) * coordY >> 8;
		}
		anIntArrayArray220[colorId][vectorId] = type;
		anIntArrayArray219[colorId][vectorId] = size;
		aColorArrayArray222[colorId][vectorId] = color1;
		aColorArrayArray223[colorId][vectorId] = color2;
		anIntArrayArrayArrayArray221[colorId][vectorId] = is_178_;
	}

	public int loadTextures(String string, Applet applet) {
		String line = " ";
		int textureAmount;
		try {
			URL url = new URL(applet.getCodeBase(), string);
			InputStream inputstream = url.openStream();
			DataInputStream datainputstream = new DataInputStream(inputstream);
			for (/**/; line.length() == 0 || line.charAt(0) != 'T'; line = datainputstream.readLine()) {
				/* empty */
			}
			int charPos = 0;
			int data = 0;
			for (/**/; line.charAt(charPos) != '='; charPos++) {
				/* empty */
			}
			for (charPos++; line.charAt(charPos) != ';'; charPos++) {
				data *= 10;
				data += line.charAt(charPos) - 48;
			}
			textureAmount = data;
			for (int textureId = 0; textureId < textureAmount; textureId++) {
				for (/**/; line.length() == 0 || line.charAt(0) != 'C'; line = datainputstream.readLine()) {
					/* empty */
				}
				charPos = 0;
				data = 0;
				for (/**/; line.charAt(charPos) != '='; charPos++) {
					/* empty */
				}
				charPos++;
				Color color;
				if (line.charAt(charPos) != 'N') {
					for (/**/; line.charAt(charPos) != ','; charPos++) {
						data *= 10;
						data += line.charAt(charPos) - 48;
					}
					int red = data;
					charPos++;
					data = 0;
					for (/**/; line.charAt(charPos) != ','; charPos++) {
						data *= 10;
						data += line.charAt(charPos) - 48;
					}
					int green = data;
					charPos++;
					data = 0;
					for (/**/; line.charAt(charPos) != ';'; charPos++) {
						data *= 10;
						data += line.charAt(charPos) - 48;
					}
					int blue = data;
					color = new Color(red, green, blue);
				} else
					color = null;
				data = 0;
				for (/**/; line.charAt(charPos) != '='; charPos++) {
					/* empty */
				}
				for (charPos++; line.charAt(charPos) != ';'; charPos++) {
					data *= 10;
					data += line.charAt(charPos) - 48;
				}
				int vectors = data;
				line = "";
				int colorId = addColors(vectors, color);
				for (int vectorId = 0; vectorId < vectors; vectorId++) {
					for (/**/; (line.length() == 0 || line.charAt(0) != 'T'); line = datainputstream.readLine()) {
						/* empty */
					}
					charPos = 0;
					data = 0;
					for (/**/; line.charAt(charPos) != '='; charPos++) {
						/* empty */
					}
					for (charPos++; line.charAt(charPos) != ';'; charPos++) {
						data *= 10;
						data += line.charAt(charPos) - 48;
					}
					int type = data;
					data = 0;
					for (/**/; line.charAt(charPos) != '='; charPos++) {
						/* empty */
					}
					for (charPos++; line.charAt(charPos) != ';'; charPos++) {
						data *= 10;
						data += line.charAt(charPos) - 48;
					}
					int size = data;
					data = 0;
					for (/**/; line.charAt(charPos) != '='; charPos++) {
						/* empty */
					}
					for (charPos++; line.charAt(charPos) != ','; charPos++) {
						data *= 10;
						data += line.charAt(charPos) - 48;
					}
					int col1Red = data;
					charPos++;
					data = 0;
					for (/**/; line.charAt(charPos) != ','; charPos++) {
						data *= 10;
						data += line.charAt(charPos) - 48;
					}
					int col1Green = data;
					charPos++;
					data = 0;
					for (/**/; line.charAt(charPos) != ';'; charPos++) {
						data *= 10;
						data += line.charAt(charPos) - 48;
					}
					int col1Blue = data;
					Color color1 = new Color(col1Red, col1Green, col1Blue);
					for (/**/; line.charAt(charPos) != '='; charPos++) {
						/* empty */
					}
					charPos++;
					Color color2;
					if (line.charAt(charPos) != 'N') {
						data = 0;
						for (/**/; line.charAt(charPos) != ','; charPos++) {
							data *= 10;
							data += line.charAt(charPos) - 48;
						}
						col1Red = data;
						charPos++;
						data = 0;
						for (/**/; line.charAt(charPos) != ','; charPos++) {
							data *= 10;
							data += line.charAt(charPos) - 48;
						}
						col1Green = data;
						charPos++;
						data = 0;
						for (/**/; line.charAt(charPos) != ';'; charPos++) {
							data *= 10;
							data += line.charAt(charPos) - 48;
						}
						col1Blue = data;
						color2 = new Color(col1Red, col1Green, col1Blue);
					} else
						color2 = null;
					int[] x = new int[size];
					for (/**/; (line.length() == 0 || line.charAt(0) != 'X'); line = datainputstream.readLine()) {
						/* empty */
					}
					for (charPos = 0; line.charAt(charPos) != '='; charPos++) {
						/* empty */
					}
					charPos++;
					for (int id = 0; id < size - 1; id++) {
						data = 0;
						for (/**/; line.charAt(charPos) != ','; charPos++) {
							data *= 10;
							data += line.charAt(charPos) - 48;
						}
						x[id] = data;
						charPos++;
					}
					data = 0;
					for (/**/; line.charAt(charPos) != ';'; charPos++) {
						data *= 10;
						data += line.charAt(charPos) - 48;
					}
					x[size - 1] = data;
					int[] y = new int[size];
					for (/**/; (line.length() == 0 || line.charAt(0) != 'Y'); line = datainputstream.readLine()) {
						/* empty */
					}
					for (charPos = 0; line.charAt(charPos) != '='; charPos++) {
						/* empty */
					}
					charPos++;
					for (int id = 0; id < size - 1; id++) {
						data = 0;
						for (/**/; line.charAt(charPos) != ','; charPos++) {
							data *= 10;
							data += line.charAt(charPos) - 48;
						}
						y[id] = data;
						charPos++;
					}
					data = 0;
					for (/**/; line.charAt(charPos) != ';'; charPos++) {
						data *= 10;
						data += line.charAt(charPos) - 48;
					}
					y[size - 1] = data;
					addTextureInfo(colorId, vectorId, type, size, x, y, color1, color2);
				}
			}
			datainputstream.close();
		} catch (java.net.MalformedURLException malformedurlexception) {
			throw new RuntimeException("Texture file: " + string + " is invalid.");
		} catch (java.io.IOException ioexception) {
			throw new RuntimeException("Texture file: " + string + " is invalid.");
		}
		return textureAmount;
	}

	public object3d[] method95(int i, int i_202_, int i_203_, int i_204_, int i_205_, int i_206_, int i_207_) {
		int[] is = new int[objectsPos];
		int i_208_ = 0;
		for (int i_209_ = 0; i_209_ < objectsPos; i_209_++) {
			if (objects[i_209_].method71(i, i_202_, i_203_, i_204_, i_205_, i_206_, i_207_))
				is[i_208_++] = i_209_;
		}
		if (i_208_ == 0)
			return null;
		object3d[] class1s = new object3d[i_208_];
		for (int i_210_ = 0; i_210_ < i_208_; i_210_++)
			class1s[i_210_] = objects[is[i_210_]];
		return class1s;
	}

	public object3d[] method96(int i, int i_211_, int i_212_, int i_213_, int i_214_, int i_215_, int i_216_, int i_217_) {
		int[] is = new int[objectsPos];
		int i_218_ = 0;
		for (int i_219_ = i; i_219_ < objectsPos; i_219_++) {
			if (objects[i_219_].method71(i_211_, i_212_, i_213_, i_214_, i_215_, i_216_, i_217_))
				is[i_218_++] = i_219_;
		}
		if (i_218_ == 0)
			return null;
		object3d[] class1s = new object3d[i_218_];
		for (int i_220_ = 0; i_220_ < i_218_; i_220_++)
			class1s[i_220_] = objects[is[i_220_]];
		return class1s;
	}

	public object3d method97(object3d class1, int i, int i_221_, int i_222_, int i_223_, int i_224_, int i_225_, int i_226_, int i_227_) {
		for (int i_228_ = i; i_228_ < objectsPos; i_228_++) {
			if (objects[i_228_] != class1 && objects[i_228_].method71(i_221_, i_222_, i_223_, i_224_, i_225_, i_226_, i_227_))
				return objects[i_228_];
		}
		return null;
	}

	public object3d method98(int i, int i_229_, int i_230_, int i_231_, int i_232_, int i_233_, int i_234_) {
		anInt226 = (anInt226 + 1) % objectsPos;
		for (int i_235_ = 0; i_235_ < objectsPos; i_235_++) {
			int i_236_ = (i_235_ + anInt226) % objectsPos;
			if (objects[i_236_].method71(i, i_229_, i_230_, i_231_, i_232_, i_233_, i_234_) && objects[i_236_].anInt134 == 0)
				return objects[i_236_];
		}
		return null;
	}

	public object3d[] method99(int i, int i_237_, int i_238_, int i_239_) {
		int[] is = new int[objectsPos];
		int i_240_ = 0;
		for (int i_241_ = 0; i_241_ < objectsPos; i_241_++) {
			if (objects[i_241_].method72(i, i_237_, i_238_, i_239_))
				is[i_240_++] = i_241_;
		}
		if (i_240_ == 0)
			return null;
		object3d[] class1s = new object3d[i_240_];
		for (int i_242_ = 0; i_242_ < i_240_; i_242_++)
			class1s[i_242_] = objects[is[i_242_]];
		return class1s;
	}

	public object3d method100(int i, int i_243_, int i_244_, int i_245_) {
		anInt226 = (anInt226 + 1) % objectsPos;
		for (int i_246_ = 0; i_246_ < objectsPos; i_246_++) {
			int i_247_ = (i_246_ + anInt226) % objectsPos;
			if (objects[i_247_].method72(i, i_243_, i_244_, i_245_) && objects[i_247_].anInt134 == 0)
				return objects[i_247_];
		}
		return null;
	}

	public byte[] export() {
		object3d class1 = new object3d(objects, objectsPos);
		return class1.encode();
	}
}
