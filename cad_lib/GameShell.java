/* Applet_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package cad_lib;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.image.MemoryImageSource;
import java.awt.image.PixelGrabber;
import java.io.DataInputStream;
import java.io.InputStream;
import java.net.URL;

public class GameShell extends Applet implements Runnable {
	private int paintState = 1;
	private int loadingPercent;
	private int width = 512;
	private int height = 384;
	private Thread shellThread;
	private TimerThread stopThread;
	private Font loadingFont;
	private int delayTime = 20;
	private String location = "/cgi-bin/uniscore.cgi?";
	public String[] bestScoresOwner;
	public int[] bestScoresTime;

	public void mainLoad() {
		/* empty */
	}

	public void mainLoop() {
		/* empty */
	}

	public void mainDestroy() {
		/* empty */
	}

	public void mainRedraw() {
		/* empty */
	}

	public void drawText(Graphics graphics, String text, Font font, int x, int y) {
		FontMetrics fontmetrics = this.getFontMetrics(font);
		graphics.setFont(font);
		graphics.drawString(text, x - fontmetrics.stringWidth(text) / 2, y + fontmetrics.getHeight() / 4);
	}

	public void submitScore(String name, int playersTime, int scoreIndex, int scoreIndexOffset) {
		String playersName = "";
		int hash = 12345678;
		for (int id = 0; id < name.length(); id++) {
			char c = name.charAt(id);
			if (c >= '0' && c <= '9' || c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z')
				playersName += c;
			else
				playersName += '_';
			if (id == 14)
				break;
		}
		if (playersName == "")
			playersName = "Anonymous";
		for (int id = 0; id < 50; id++) {
			if (playersTime > bestScoresTime[id + scoreIndex * 50]) {
				for (int id_ = 49; id_ > id; id_--) {
					bestScoresTime[id_ + scoreIndex * 50] = bestScoresTime[id_ + scoreIndex * 50 - 1];
					bestScoresOwner[id_ + scoreIndex * 50] = bestScoresOwner[id_ + scoreIndex * 50 - 1];
				}
				bestScoresOwner[id + scoreIndex * 50] = playersName;
				bestScoresTime[id + scoreIndex * 50] = playersTime;
				break;
			}
		}
		scoreIndex += scoreIndexOffset;
		hash = (hash + playersTime * 13 + scoreIndex * 5) % 20000;
		for (int id = 0; id < playersName.length(); id++)
			hash = (hash * (id + 1) + id) % 30000;
		hash = hash * hash % 100000000;
		try {
			URL url = new URL("http://" + this.getCodeBase().getHost() + location + playersName + "+" + playersTime + "+" + scoreIndex + "+" + hash + "+" + (int) (Math.random() * 99999.0));
			InputStream inputstream = url.openStream();
			DataInputStream datainputstream = new DataInputStream(inputstream);
			datainputstream.readLine();
			datainputstream.close();
		} catch (Throwable throwable) {
			/* empty */
		}
	}

	public void fetchBestScores(int version, int count) {
		bestScoresOwner = new String[count * 50];
		bestScoresTime = new int[count * 50];
		for (int id = 0; id < count * 50; id++) {
			bestScoresOwner[id] = "";
			bestScoresTime[id] = 0;
		}
		try {
			URL url = new URL("http://" + this.getCodeBase().getHost() + location + version + "+" + count + "+" + (int) (Math.random() * 99999.0));
			InputStream inputstream = url.openStream();
			DataInputStream datainputstream = new DataInputStream(inputstream);
			for (int id = 0; id < count * 50; id++) {
				bestScoresOwner[id] = datainputstream.readLine();
				String string = datainputstream.readLine();
				bestScoresTime[id] = Integer.valueOf(string, 10).intValue();
			}
			datainputstream.close();
		} catch (Throwable throwable) {
			/* empty */
		}
		if (bestScoresOwner[49] == "") {
			for (int id = 0; id < count * 50; id++) {
				bestScoresOwner[id] = "GamesArena";
				bestScoresTime[id] = 0;
			}
		}
	}

	public final void setDelayTime(int i) {
		delayTime = i;
	}

	public final Image loadImage(String string) {
		Image image = this.getImage(this.getCodeBase(), string);
		MediaTracker mediatracker = new MediaTracker(this);
		mediatracker.addImage(image, 0);
		try {
			mediatracker.waitForID(0);
		} catch (InterruptedException interruptedexception) {
			System.out.println("Error!");
		}
		return image;
	}

	public final void loadImagesFromSheet(String string, Image[] images, int imageCount, int width, int height) {
		Image image = this.getImage(this.getCodeBase(), string);
		MediaTracker mediaTracker = new MediaTracker(this);
		mediaTracker.addImage(image, 0);
		try {
			mediaTracker.waitForID(0);
		} catch (InterruptedException interruptedexception) {
			System.out.println("Error!");
		}
		int imageWidth = image.getWidth(this);
		int imageHeight = image.getHeight(this);
		int[] imagePixels = new int[imageWidth * imageHeight];
		PixelGrabber pixelgrabber = new PixelGrabber(image, 0, 0, imageWidth, imageHeight, imagePixels, 0, imageWidth);
		try {
			pixelgrabber.grabPixels();
		} catch (InterruptedException interruptedexception) {
			System.out.println("Error!");
		}
		int widthOff = 0;
		int heightOff = 0;
		for (int imageId = 0; imageId < imageCount; imageId++) {
			int pixelsPos = 0;
			int[] pixels = new int[width * height];
			for (int h = heightOff; h < heightOff + height; h++) {
				for (int w = widthOff; w < widthOff + width; w++)
					pixels[pixelsPos++] = imagePixels[w + h * imageWidth];
			}
			images[imageId] = this.createImage(new MemoryImageSource(width, height, pixels, 0, width));
			while (!this.prepareImage(images[imageId], this)) {
				/* empty */
			}
			widthOff += width;
			if (widthOff >= imageWidth) {
				widthOff = 0;
				heightOff += height;
			}
		}
	}

	public final void clearScreen() {
		Graphics graphics = this.getGraphics();
		graphics.setColor(Color.black);
		graphics.fillRect(0, 0, width, height);
	}

	public final void setLoadingPercent(int percent) {
		loadingPercent = percent;
		int x = (width - 304) / 2;
		int y = (height - 32) / 2;
		Graphics graphics = this.getGraphics();
		graphics.setColor(Color.red);
		graphics.fillRect(x, y, 304, 32);
		graphics.setColor(Color.black);
		graphics.fillRect(x + 1, y + 1, 302, 30);
		graphics.setColor(Color.red);
		graphics.fillRect(x + 2, y + 2, percent * 3, 28);
		graphics.setColor(Color.white);
		graphics.setFont(loadingFont);
		graphics.drawString("Now Loading - " + percent + "%", x + 96, y + 24);
	}

	public final void init() {
		loadingFont = new Font("TimesRoman", 0, 15);
		width = this.getSize().width;
		height = this.getSize().height;
		paintState = 1;
		shellThread = new Thread(this);
		shellThread.start();
	}

	public final void start() {
		if (stopThread != null) {
			stopThread.stop();
			stopThread = null;
		}
	}

	public final void stop() {
		if (stopThread == null) {
			stopThread = new TimerThread();
			stopThread.delay = 4000;
			stopThread.applet = this;
			stopThread.start();
		}
	}

	public final void destroy() {
		mainDestroy();
		if (shellThread != null) {
			shellThread.stop();
			shellThread = null;
		}
		if (stopThread != null) {
			stopThread.stop();
			stopThread = null;
		}
	}

	public final void run() {
		if (paintState == 1) {
			paintState = 2;
			clearScreen();
			setLoadingPercent(0);
			mainLoad();
			setLoadingPercent(100);
			paintState = 0;
		}
		long oldTime = System.currentTimeMillis() / (long) delayTime;
		long newTime = 0L;
		int[] oldTimes = new int[20];
		int oldTimesPos = 0;
		int oldTimesCount = 0;
		int averageCycle = 0;
		for (;;) {
			int timeDelta = 0;
			while (timeDelta == 0) {
				newTime = System.currentTimeMillis() / (long) delayTime;
				timeDelta = (int) (newTime - oldTime);
				oldTime = newTime;
				if (timeDelta > 20)
					timeDelta = 20;
				if (timeDelta == 0) {
					try {
						Thread.sleep(1L);
					} catch (InterruptedException interruptedexception) {
						/* empty */
					}
				}
			}
			oldTimes[oldTimesPos] = timeDelta;
			oldTimesPos = (oldTimesPos + 1) % 10;
			if (oldTimesCount < 10)
				oldTimesCount++;
			timeDelta = 0;
			for (int id = 0; id < oldTimesCount; id++)
				timeDelta += oldTimes[id];
			averageCycle += timeDelta * 16 / oldTimesCount;
			this.showStatus("Num:" + 500 / timeDelta);
			timeDelta = averageCycle / 16;
			for (int id = 0; id < timeDelta; id++) {
				mainLoop();
			}
			averageCycle -= timeDelta * 16;
			mainRedraw();
		}
	}

	public final void update(Graphics graphics) {
		paint(graphics);
	}

	public final void paint(Graphics graphics) {
		if (paintState == 2) {
			clearScreen();
			setLoadingPercent(loadingPercent);
		}
	}
}
