/* InputStream_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package cad_lib;
import java.io.InputStream;
import java.util.Arrays;

import sun.audio.AudioPlayer;

public class xmplayer extends InputStream
{
    byte[] aByteArray21 = new byte[65536];
    byte[] aByteArray22;
    int[] anIntArray23 = new int[16000];
    int[] anIntArray24
	= { 0, 0, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4,
	    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
	    5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6,
	    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
	    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
	    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7,
	    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
	    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
	    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
	    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
	    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
	    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7 };
    int songLengthInPatterns;
    int channelCount;
    int patternCount;
    int instrumentCount;
    int flags;
    int defaultTempo;
    int defaultBeatsPerMinute;
    int anInt32;
    int anInt33;
    int anInt34;
    int anInt35;
    int anInt36;
    int anInt37;
    int anInt38;
    int anInt39;
    int anInt40;
    int anInt41;
    int anInt42;
    int anInt43;
    int anInt44;
    int anInt45;
    int anInt46;
    int anInt47;
    int[] patternRows = new int[256];
    int[] sampleLengths = new int[256];
    int[] sampleVolumes = new int[256];
    int[] sampleLoopLength = new int[256];
    int[] patternOrderTable = new int[256];
    int[] sampleData16bitEnabled = new int[256];
    byte[] sampleRelativeNoteNumbers = new byte[256];
    byte[] sampleFinetunes = new byte[256];
    int[] anIntArray56 = new int[8000];
    int[] anIntArray57 = new int[68];
    int[] anIntArray58 = new int[64];
    int[] anIntArray59 = new int[64];
    int[] anIntArray60 = new int[64];
    int[] anIntArray61 = new int[64];
    public int anInt62 = -1;
    public int anInt63;
    public int anInt64;
    public int anInt65;
    public int anInt66;
    public int anInt67;
    int[] patternOffsets = new int[256];
    int[] sampleOffsets = new int[256];
    int anInt70;
    public int anInt71 = 64;
    public int anInt72;
    public int anInt73;
    public int anInt74;
    int[] anIntArray75 = new int[64];
    int[] anIntArray76 = new int[64];
    int[] anIntArray77 = new int[64];
    int[] anIntArray78 = new int[64];
    int[] anIntArray79 = new int[64];
    int[] anIntArray80 = new int[64];
    int[] anIntArray81 = new int[64];
    int[] anIntArray82 = new int[64];
    int[] anIntArray83 = new int[64];
    int[] anIntArray84 = new int[64];
    int[] anIntArray85 = new int[64];
    int[] anIntArray86 = new int[64];
    int[] anIntArray87 = new int[64];
    int[] anIntArray88 = new int[64];
    int[] anIntArray89 = new int[64];
    int[] anIntArray90 = new int[64];
    int[] anIntArray91 = new int[64];
    int[] anIntArray92 = new int[64];
    int[] anIntArray93 = new int[64];
    int[] anIntArray94 = new int[64];
    int[] anIntArray95 = new int[64];
    int[] numberOfVolumePoints = new int[256];
    int[] volumeSustainPoints = new int[256];
    int[] volumeLoopStartPoints = new int[256];
    int[] volumeLoopEndPoints = new int[256];
    int[][] pointsForVolumeEnvelope = new int[256][12];
    int[][] poinstForPanningEnvelope = new int[256][12];
    int[] volumeTypes = new int[256];
    int[] volumeFadeouts = new int[256];
    int[][] anIntArrayArray104 = new int[64][256];
    byte[][] sampleNumbers = new byte[256][96];
    byte[][] aByteArrayArray106 = new byte[256][256];
    byte[][] aByteArrayArray107 = new byte[256][256];
    int[] anIntArray108 = new int[32];
    int[] anIntArray109 = new int[32];
    int[] anIntArray110 = new int[32];
    int[] anIntArray111 = new int[32];
    int[] anIntArray112 = new int[32];
    
    public xmplayer(byte[] is) {
	is[0]++;
	aByteArray22 = is;
	method38(is);
		for (int i = -32768; i < 32767; i++)
			aByteArray21[i + 32768] = method30(i);
		for (int i = 0; i < 256; i++) {
			for (int i_0_ = -128; i_0_ < 127; i_0_++) {
				aByteArrayArray106[i][i_0_ + 128] = (byte) ((256 - i) * i_0_ / 256);
				aByteArrayArray107[i][i_0_ + 128] = (byte) (i * i_0_ / 256);
			}
		}
	for (int i = 0; i < 32; i++) {
	    anIntArray108[i] = 0;
	    anIntArray109[i] = 0;
	    anIntArray110[i] = 0;
	    anIntArray111[i] = 0;
	    anIntArray112[i] = 10;
	}
	for (int i = 0; i < 64; i++) {
	    for (int i_1_ = -128; i_1_ < 127; i_1_++)
		anIntArrayArray104[i][i_1_ + 128] = i_1_ * i;
	}
	anInt32 = 1;
	anInt46 = 0;
    }
    
    public void start() {
	//AudioPlayer.player.start(this);
    }
    
    public void stop() {
	AudioPlayer.player.stop(this);
    }
    
    public byte method30(int i) {
	int i_2_ = i >> 8 & 0x80;
	if (i_2_ != 0)
	    i = -i;
	if (i > 32635)
	    i = 32635;
	i += 132;
	int i_3_ = anIntArray24[i >> 7 & 0xff];
	int i_4_ = i >> i_3_ + 3 & 0xf;
	byte i_5_ = (byte) ((i_2_ | i_3_ << 4 | i_4_) ^ 0xffffffff);
	return i_5_;
    }
    
    public void method31(int i, int i_6_, int i_7_, int i_8_, int i_9_,
			 int i_10_) {
	if (i_7_ <= 0 || i_7_ > 340000)
	    method33(i);
	else {
	    if (i_8_ < 0)
		i_8_ = 0;
	    else if (i_8_ > 63)
		i_8_ = 63;
	    anIntArray108[i] = i_6_ << 8;
	    anIntArray112[i] = i_6_ + i_9_ << 8;
	    anIntArray111[i] = i_10_ << 8;
	    anIntArray109[i] = i_8_;
	    anIntArray110[i] = i_7_ * 256 / 8000;
	}
    }
    
    public void method32(int i, int i_11_) {
	if (i_11_ < 0)
	    i_11_ = 0;
	else if (i_11_ > 63)
	    i_11_ = 63;
	anIntArray109[i] = i_11_;
    }
    
    public void method33(int i) {
	anIntArray110[i] = 0;
    }
    
    public void method34(int i, int i_12_) {
	anIntArray110[i] = i_12_ * 256 / 8000;
    }
    
	int getUnsigned(byte[] is, int i) {
		return is[i] & 0xff;
	}
    
	int getUnsignedShort(byte[] is, int i) {
		return (is[i] & 0xff) + (is[i + 1] & 0xff) * 256;
	}
    
	int getInt(byte[] is, int i) {
		return ((is[i] & 0xff) + (is[i + 1] & 0xff) * 256 + (is[i + 2] & 0xff) * 65536 + (is[i + 3] & 0xff) * 65536 * 256);
	}
    
	public void method38(byte[] is) {
		for (int i = 0; i < 8000; i++)
			anIntArray56[i] = (int) (8363.0 * Math.pow(2.0, (double) (4608 - i) / 768.0));
		for (int i = 0; i < 68; i++)
			anIntArray57[i] = (int) (-2048.0 * Math.sin((double) i * Math.PI / 34));
		for (int i = 0; i < 64; i++) {
			anIntArray80[i] = 0;
			anIntArray81[i] = 0;
			anIntArray82[i] = 0;
			anIntArray83[i] = 0;
			anIntArray84[i] = 0;
			anIntArray85[i] = 0;
			anIntArray75[i] = 0;
			anIntArray76[i] = 0;
			anIntArray77[i] = 0;
			anIntArray78[i] = 0;
			anIntArray79[i] = 0;
			anIntArray86[i] = 0;
			anIntArray87[i] = 0;
			anIntArray88[i] = 0;
			anIntArray89[i] = 0;
			anIntArray60[i] = 0;
			anIntArray58[i] = 0;
			anIntArray59[i] = 0;
			anIntArray90[i] = 0;
			anIntArray61[i] = -1;
			anIntArray91[i] = 0;
			anIntArray92[i] = 0;
			anIntArray93[i] = 0;
			anIntArray94[i] = 0;
			anIntArray95[i] = 0;
		}
		songLengthInPatterns = getUnsignedShort(is, 64);
		channelCount = getUnsignedShort(is, 68);
		patternCount = getUnsignedShort(is, 70);
		instrumentCount = getUnsignedShort(is, 72);
		flags = getUnsignedShort(is, 74);//0 = Amiga frequency table (see below); 1 = Linear frequency table
		defaultTempo = getUnsignedShort(is, 76);
		defaultBeatsPerMinute = getUnsignedShort(is, 78);
		for (int id = 0; id < songLengthInPatterns; id++)
			patternOrderTable[id] = getUnsigned(is, id + 80);
		
		int headerSize = 60 + getInt(is, 60);
		int headerOffset = 0;
		for (int patternId = 0; patternId < patternCount; patternId++) {
			patternRows[patternId] = getUnsignedShort(is, headerSize + headerOffset + 5);
			patternOffsets[patternId] = headerSize + headerOffset + getInt(is, headerSize + headerOffset);
			headerOffset += getInt(is, headerSize + headerOffset) + getUnsignedShort(is, headerSize + headerOffset + 7);//short - Packed patterndata size, int - Pattern header length
		}
		int instrumentHeaderOffset = headerSize + headerOffset;
		headerOffset = 0;
		int sampleId = 0;
		for (int instrumentId = 0; instrumentId < instrumentCount; instrumentId++) {
			int samplesCount = getUnsignedShort(is, instrumentHeaderOffset + headerOffset + 27);
			for (int id = 0; id < 96; id++) {
				if (samplesCount > 0)
					sampleNumbers[instrumentId][id] = (byte) (sampleId + getUnsigned(is, instrumentHeaderOffset + headerOffset + 33 + id));
				else
					sampleNumbers[instrumentId][id] = (byte) -1;
			}
			numberOfVolumePoints[instrumentId] = getUnsigned(is, instrumentHeaderOffset + headerOffset + 225);
			volumeSustainPoints[instrumentId] = getUnsigned(is, instrumentHeaderOffset + headerOffset + 227);
			volumeLoopStartPoints[instrumentId] = getUnsigned(is, instrumentHeaderOffset + headerOffset + 228);
			volumeLoopEndPoints[instrumentId] = getUnsigned(is, instrumentHeaderOffset + headerOffset + 229);
			volumeTypes[instrumentId] = getUnsigned(is, instrumentHeaderOffset + headerOffset + 233);//bit 0: On; 1: Sustain; 2: Loop
			volumeFadeouts[instrumentId] = getUnsignedShort(is, instrumentHeaderOffset + headerOffset + 239);
			for (int id = 0; id < 12; id++) {
				pointsForVolumeEnvelope[instrumentId][id] = getUnsignedShort(is, instrumentHeaderOffset + headerOffset + 129 + id * 4);
				poinstForPanningEnvelope[instrumentId][id] = getUnsignedShort(is, instrumentHeaderOffset + headerOffset + 131 + id * 4);
			}
			int sampleHeaderSize = getInt(is, instrumentHeaderOffset + headerOffset + 29);
			headerOffset += getInt(is, instrumentHeaderOffset + headerOffset);
			int sampleHeaderOffset = headerOffset + sampleHeaderSize * samplesCount;
			for (int id = 0; id < samplesCount; id++) {
				int type = getUnsigned(is, instrumentHeaderOffset + headerOffset + 14);//Bit 0-1: 0 = No loop, 1 = Forward loop, 2 = Ping-pong loop; 4: 16-bit sampledata
				if ((type & 0x10) == 16)
					sampleData16bitEnabled[sampleId] = 1;
				else
					sampleData16bitEnabled[sampleId] = 0;
				if ((type & 0x3) == 0) {
					sampleLoopLength[sampleId] = 0;
					sampleLengths[sampleId] = getInt(is, instrumentHeaderOffset + headerOffset);
				} else {
					sampleLengths[sampleId] = (getInt(is, instrumentHeaderOffset + headerOffset + 4) + getInt(is, instrumentHeaderOffset + headerOffset + 8));
					sampleLoopLength[sampleId] = getInt(is, instrumentHeaderOffset + headerOffset + 8);
				}
				sampleFinetunes[sampleId] = is[instrumentHeaderOffset + headerOffset + 13];
				sampleRelativeNoteNumbers[sampleId] = (byte) (is[instrumentHeaderOffset + headerOffset + 16] - 1);
				sampleVolumes[sampleId] = getUnsigned(is, instrumentHeaderOffset + headerOffset + 12);
				sampleOffsets[sampleId] = instrumentHeaderOffset + sampleHeaderOffset;
				if (sampleData16bitEnabled[sampleId] == 1) {
					sampleLengths[sampleId] /= 2;
					sampleLoopLength[sampleId] /= 2;
				}
				sampleHeaderOffset += getInt(is, instrumentHeaderOffset + headerOffset);
				headerOffset += sampleHeaderSize;
				sampleId++;
			}
			sampleId -= samplesCount;
			for (int id = 0; id < samplesCount; id++) {
				int i_26_ = 0;
				for (int sample = 0; sample < sampleLengths[sampleId]; sample++) {
					int i_28_;
					if (sampleData16bitEnabled[sampleId] == 0) {
						i_28_ = is[sampleOffsets[sampleId] + sample] + i_26_;
						is[sampleOffsets[sampleId] + sample] = (byte) ((byte) i_28_ / 2);
					} else {
						i_28_ = (getUnsignedShort(is, sampleOffsets[sampleId] + sample * 2) + i_26_);
						is[sampleOffsets[sampleId] + sample] = (byte) ((byte) (i_28_ / 256) / 2);
					}
					i_26_ = i_28_;
				}
				sampleId++;
				headerOffset = sampleHeaderOffset;
			}
		}
	}

	public byte toByte(int i) {
		if (i < -128)
			i = -128;
		else if (i > 127)
			i = 127;
		return (byte) i;
	}
    
	public void method40(int i, int i_29_, int i_30_, int i_31_) {
		method31(i, sampleOffsets[i_29_], i_30_, i_31_, sampleLengths[i_29_], sampleLoopLength[i_29_]);
    }
    
    public void method41(int i) {
	int i_32_ = 0;
	if (anInt32 != 1 && anInt32 != 2) {
	    anInt72 = 0;
	    anInt74 = 0;
	    anInt73 = 0;
	} else {
	    anInt72 += i * defaultBeatsPerMinute;
	    if (anInt73 == 0 && anInt72 >= 20000) {
		if (anInt32 == 1) {
		    anInt32 = 2;
		    anInt35 = 0;
		    anInt34 = 0;
		    anInt36 = patternOrderTable[anInt35];
		    anInt70 = patternOffsets[anInt36];
		    anInt33 = 0;
		}
		if (anInt34 >= patternRows[anInt36] || anInt63 == 1
		    || anInt64 == 1) {
		    if (anInt64 == 1) {
			anInt35 = anInt65;
			anInt46 = 1;
		    } else
			anInt35++;
		    if (anInt62 != -1) {
			anInt35 = anInt62;
			anInt62 = -1;
		    }
		    if (anInt35 >= songLengthInPatterns) {
			anInt35 = 0;
			anInt46 = 1;
		    }
		    anInt34 = 0;
		    anInt36 = patternOrderTable[anInt35];
		    anInt70 = patternOffsets[anInt36];
		    anInt33 = 0;
		    if (anInt66 > 0) {
			anInt63 = 1;
			anInt65 = anInt66;
			anInt66 = 0;
		    }
		    if (anInt63 == 1) {
			anInt65 = anInt65 / 16 * 10 + anInt65 % 16;
			if (anInt65 >= patternRows[anInt36])
			    anInt65 = 0;
			anInt34 = anInt65;
			for (int i_33_ = 0; i_33_ < anInt65; i_33_++) {
			    for (anInt43 = 0; anInt43 < channelCount; anInt43++) {
				anInt37
				    = aByteArray22[anInt70 + anInt33] & 0xff;
				anInt33++;
				if (anInt37 < 128) {
				    anInt33--;
				    anInt37 = 31;
				} else
				    anInt37 -= 128;
				if ((anInt37 & 0x1) == 1) {
				    anInt38 = (aByteArray22[anInt70 + anInt33]
					       & 0xff);
				    anInt33++;
				}
				if ((anInt37 & 0x2) == 2) {
				    anInt39 = (aByteArray22[anInt70 + anInt33]
					       & 0xff) - 1;
				    anInt33++;
				}
				if ((anInt37 & 0x4) == 4) {
				    anInt40 = (aByteArray22[anInt70 + anInt33]
					       & 0xff) - 16;
				    anInt33++;
				}
				if ((anInt37 & 0x8) == 8) {
				    anInt41 = (aByteArray22[anInt70 + anInt33]
					       & 0xff);
				    anInt33++;
				}
				if ((anInt37 & 0x10) == 16) {
				    anInt42 = (aByteArray22[anInt70 + anInt33]
					       & 0xff);
				    anInt33++;
				}
			    }
			}
		    }
		    anInt62 = -1;
		    anInt63 = 0;
		    anInt64 = 0;
		}
		anInt34++;
		for (anInt43 = 0; anInt43 < channelCount; anInt43++) {
		    anInt38 = -1;
		    anInt39 = -1;
		    anInt40 = -1;
		    anInt41 = -1;
		    anInt42 = 0;
		    anInt37 = aByteArray22[anInt70 + anInt33] & 0xff;
		    anInt33++;
		    if (anInt37 < 128) {
			anInt33--;
			anInt37 = 31;
		    } else
			anInt37 -= 128;
		    if ((anInt37 & 0x1) == 1) {
			anInt38 = aByteArray22[anInt70 + anInt33] & 0xff;
			anInt33++;
		    }
		    if ((anInt37 & 0x2) == 2) {
			anInt39 = (aByteArray22[anInt70 + anInt33] & 0xff) - 1;
			anInt33++;
		    }
		    if ((anInt37 & 0x4) == 4) {
			anInt40
			    = (aByteArray22[anInt70 + anInt33] & 0xff) - 16;
			anInt33++;
		    }
		    if ((anInt37 & 0x8) == 8) {
			anInt41 = aByteArray22[anInt70 + anInt33] & 0xff;
			anInt33++;
		    }
		    if ((anInt37 & 0x10) == 16) {
			anInt42 = aByteArray22[anInt70 + anInt33] & 0xff;
			anInt33++;
		    }
		    if (anInt40 >= 80 && anInt40 < 96) {
			anIntArray95[anInt43] = 2;
			anIntArray75[anInt43] = 80 - anInt40;
			anInt40 = -1;
		    }
		    if (anInt40 >= 96 && anInt40 < 112) {
			anIntArray95[anInt43] = 2;
			anIntArray75[anInt43] = anInt40 - 96;
			anInt40 = -1;
		    }
		    if (anInt40 >= 112 && anInt40 < 128) {
			anInt40 = anIntArray58[anInt43] - (anInt40 - 112);
			if (anInt40 < 0)
			    anInt40 = 0;
			else if (anInt40 > 63)
			    anInt40 = 63;
		    }
		    if (anInt40 >= 128 && anInt40 < 136) {
			anInt40 = anIntArray58[anInt43] + (anInt40 - 128);
			if (anInt40 < 0)
			    anInt40 = 0;
			else if (anInt40 > 63)
			    anInt40 = 63;
		    }
		    if (anInt40 > 64)
			anInt40 = -1;
		    if (anInt41 == 13) {
			anInt63 = 1;
			anInt65 = anInt42;
		    }
		    if (anInt41 == 15) {
			if (anInt42 < 32)
			    defaultTempo = anInt42;
			else
			    defaultBeatsPerMinute = anInt42;
		    }
		    if (anInt41 == 12)
			anInt40 = anInt42;
		    if (anInt41 == 14) {
			anInt41 = anInt41 * 16 + anInt42 / 16;
			anInt42 = anInt42 & 0xf;
		    }
		    if (anInt41 == 236 && anInt42 < defaultTempo)
			anInt38 = 97;
		    if (anInt41 == 11) {
			anInt64 = 1;
			anInt65 = anInt42;
		    }
		    if (anInt41 == 20)
			anInt38 = 97;
		    if (anInt41 == 21) {
			anIntArray90[anInt43] = anInt42;
			if (anIntArray90[anInt43]
			    >= (pointsForVolumeEnvelope[i_32_]
				[numberOfVolumePoints[i_32_] - 1]))
			    anIntArray90[anInt43]
				= (pointsForVolumeEnvelope[i_32_]
				   [numberOfVolumePoints[i_32_] - 1]) - 1;
		    }
		    if (anInt39 >= 0 && anInt38 <= 96) {
			anIntArray90[anInt43] = 0;
			anIntArray92[anInt43] = 1;
			anIntArray93[anInt43] = 0;
		    }
		    if (anInt41 == 3 && anInt40 < 0 && anInt39 != -1)
			anInt40 = sampleVolumes[anIntArray60[anInt43]];
		    if (anInt38 >= 0 && anInt38 <= 96 && anInt41 != 3) {
			if (anInt39 == -1 && anInt40 < 0)
			    anInt40 = anIntArray58[anInt43];
			if (anInt39 == -1)
			    anInt39 = anIntArray60[anInt43];
			else {
			    anIntArray61[anInt43] = anInt39;
			    if (anInt38 < 96)
				anInt39 = sampleNumbers[anInt39][anInt38];
			    else
				anInt39 = sampleNumbers[anInt39][95];
			}
			if (anInt40 < 0)
			    anInt40 = sampleVolumes[anInt39];
			anInt45 = (7680
				   - (anInt38 + sampleRelativeNoteNumbers[anInt39]) * 16 * 4
				   - sampleFinetunes[anInt39] / 2);
			if (anInt45 < 500)
			    anInt45 = 500;
			else if (anInt45 > 7999)
			    anInt45 = 7999;
			anIntArray88[anInt43] = anInt45;
			anInt44 = anIntArray56[anInt45];
			anIntArray60[anInt43] = anInt39;
			anIntArray58[anInt43] = anInt40;
			anIntArray59[anInt43] = anInt45;
			int i_34_ = sampleOffsets[anInt39];
			int i_35_ = sampleLengths[anInt39];
			if (anInt41 == 9) {
			    if (anInt42 * 256 > sampleLengths[anInt39]) {
				i_34_ += sampleLengths[anInt39];
				i_35_ = 0;
			    } else {
				i_34_ += anInt42 * 256;
				i_35_ -= anInt42 * 256;
			    }
			}
			method31(anInt43, i_34_, anInt44,
				 anInt40 * anInt71 / 64, i_35_,
				 sampleLoopLength[anInt39]);
			anIntArray78[anInt43] = 0;
		    } else if (anInt38 > 96) {
			if (anIntArray91[anInt43] == 1)
			    anIntArray92[anInt43] = 0;
			else
			    method33(anInt43);
		    } else if (anInt40 >= 0) {
			method32(anInt43, anInt40 * anInt71 / 64);
			anIntArray58[anInt43] = anInt40;
		    }
		    if (anInt41 == 3) {
			anIntArray94[anInt43] = 1;
			if (anInt38 >= 0 && anInt38 <= 96) {
			    anInt39 = anIntArray60[anInt43];
			    anInt45
				= (7680
				   - (anInt38 + sampleRelativeNoteNumbers[anInt39]) * 16 * 4
				   - sampleFinetunes[anInt39] / 2);
			    if (anInt45 < 500)
				anInt45 = 500;
			    else if (anInt45 > 7999)
				anInt45 = 7999;
			    anIntArray88[anInt43] = anInt45;
			}
			if (anInt42 != 0) {
			    anIntArray89[anInt43] = anInt42;
			    if (flags == 0)
				anIntArray89[anInt43]
				    = anIntArray89[anInt43] * 2;
			}
		    } else if (anInt41 != 5)
			anIntArray94[anInt43] = 0;
		    if (anInt41 == 4) {
			anIntArray79[anInt43] = 1;
			if (anInt42 / 16 > 0)
			    anIntArray76[anInt43] = anInt42 / 16;
			if ((anInt42 & 0xf) > 0)
			    anIntArray77[anInt43] = anInt42 & 0xf;
		    } else if (anInt41 != 6) {
			if (anIntArray79[anInt43] != 0) {
			    anInt44 = anIntArray56[anIntArray59[anInt43]];
			    method34(anInt43, anInt44);
			}
			anIntArray78[anInt43] = 0;
			anIntArray79[anInt43] = 0;
		    }
		    if (anInt41 == 10 || anInt41 == 6 || anInt41 == 5) {
			anIntArray95[anInt43] = 1;
			if (anInt42 != 0)
			    anIntArray75[anInt43]
				= (anInt42 & 0xf0) / 16 - (anInt42 & 0xf);
		    } else if (anIntArray95[anInt43] == 2)
			anIntArray95[anInt43] = 1;
		    else
			anIntArray95[anInt43] = 0;
		    if (anInt41 == 234) {
			if (anInt42 == 0)
			    anInt42 = anIntArray80[anInt43];
			else
			    anIntArray80[anInt43] = anInt42;
			anIntArray58[anInt43] += anInt42;
			if (anIntArray58[anInt43] < 0)
			    anIntArray58[anInt43] = 0;
			else if (anIntArray58[anInt43] > 63)
			    anIntArray58[anInt43] = 63;
			method32(anInt43,
				 anIntArray58[anInt43] * anInt71 / 64);
		    }
		    if (anInt41 == 235) {
			if (anInt42 == 0)
			    anInt42 = anIntArray83[anInt43];
			else
			    anIntArray83[anInt43] = anInt42;
			anIntArray58[anInt43] -= anInt42;
			if (anIntArray58[anInt43] < 0)
			    anIntArray58[anInt43] = 0;
			else if (anIntArray58[anInt43] > 63)
			    anIntArray58[anInt43] = 63;
			method32(anInt43,
				 anIntArray58[anInt43] * anInt71 / 64);
		    }
		    if (anInt41 == 1) {
			if (anInt42 != 0)
			    anIntArray86[anInt43] = anInt42;
		    } else
			anIntArray86[anInt43] = 0;
		    if (anInt41 == 2) {
			if (anInt42 != 0)
			    anIntArray87[anInt43] = anInt42;
		    } else
			anIntArray87[anInt43] = 0;
		    if (anInt41 == 225) {
			if (anInt42 == 0)
			    anInt42 = anIntArray81[anInt43];
			else
			    anIntArray81[anInt43] = anInt42;
			anIntArray59[anInt43] -= anInt42 * 4;
			if (anIntArray59[anInt43] < 500)
			    anIntArray59[anInt43] = 500;
			else if (anIntArray59[anInt43] > 7999)
			    anIntArray59[anInt43] = 7999;
			anInt44 = anIntArray56[anIntArray59[anInt43]];
			method34(anInt43, anInt44);
		    }
		    if (anInt41 == 226) {
			if (anInt42 == 0)
			    anInt42 = anIntArray84[anInt43];
			else
			    anIntArray84[anInt43] = anInt42;
			anIntArray59[anInt43] += anInt42 * 4;
			if (anIntArray59[anInt43] < 500)
			    anIntArray59[anInt43] = 500;
			else if (anIntArray59[anInt43] > 7999)
			    anIntArray59[anInt43] = 7999;
			anInt44 = anIntArray56[anIntArray59[anInt43]];
			method34(anInt43, anInt44);
		    }
		    if (anInt41 == 33) {
			if (anInt42 / 16 == 1) {
			    anInt42 = anInt42 & 0xf;
			    if (anInt42 == 0)
				anInt42 = anIntArray82[anInt43];
			    else
				anIntArray82[anInt43] = anInt42;
			    anIntArray59[anInt43] -= anInt42;
			    if (anIntArray59[anInt43] < 500)
				anIntArray59[anInt43] = 500;
			    else if (anIntArray59[anInt43] > 7999)
				anIntArray59[anInt43] = 7999;
			    anInt44 = anIntArray56[anIntArray59[anInt43]];
			    method34(anInt43, anInt44);
			} else {
			    anInt42 = anInt42 & 0xf;
			    if (anInt42 == 0)
				anInt42 = anIntArray85[anInt43];
			    else
				anIntArray85[anInt43] = anInt42;
			    anIntArray59[anInt43] += anInt42;
			    if (anIntArray59[anInt43] < 500)
				anIntArray59[anInt43] = 500;
			    else if (anIntArray59[anInt43] > 7999)
				anIntArray59[anInt43] = 7999;
			    anInt44 = anIntArray56[anIntArray59[anInt43]];
			    method34(anInt43, anInt44);
			}
		    }
		}
	    }
	    if (anInt73 > 0 && anInt72 >= 20000) {
		for (anInt43 = 0; anInt43 < channelCount; anInt43++) {
		    if (anIntArray95[anInt43] != 0) {
			anIntArray58[anInt43] += anIntArray75[anInt43];
			if (anIntArray58[anInt43] < 0)
			    anIntArray58[anInt43] = 0;
			else if (anIntArray58[anInt43] > 63)
			    anIntArray58[anInt43] = 63;
			method32(anInt43,
				 anIntArray58[anInt43] * anInt71 / 64);
		    }
		    if (anIntArray79[anInt43] == 1) {
			anIntArray78[anInt43] = (anIntArray78[anInt43]
						 + anIntArray76[anInt43]) % 68;
			int i_36_ = ((anIntArray57[anIntArray78[anInt43]]
				      * anIntArray77[anInt43])
				     >> 8);
			anInt45 = anIntArray59[anInt43] + i_36_;
			method34(anInt43, anIntArray56[anInt45]);
		    }
		    if (anIntArray86[anInt43] > 0) {
			anIntArray59[anInt43] -= anIntArray86[anInt43] * 4;
			if (anIntArray59[anInt43] < 500)
			    anIntArray59[anInt43] = 500;
			else if (anIntArray59[anInt43] > 7999)
			    anIntArray59[anInt43] = 7999;
			anInt44 = anIntArray56[anIntArray59[anInt43]];
			method34(anInt43, anInt44);
		    }
		    if (anIntArray87[anInt43] > 0) {
			anIntArray59[anInt43] += anIntArray87[anInt43] * 4;
			if (anIntArray59[anInt43] < 500)
			    anIntArray59[anInt43] = 500;
			else if (anIntArray59[anInt43] > 7999)
			    anIntArray59[anInt43] = 7999;
			anInt44 = anIntArray56[anIntArray59[anInt43]];
			method34(anInt43, anInt44);
		    }
		    if (anIntArray94[anInt43] > 0) {
			if (anIntArray59[anInt43] < anIntArray88[anInt43]) {
			    anIntArray59[anInt43] += anIntArray89[anInt43] * 4;
			    if (anIntArray59[anInt43] > anIntArray88[anInt43])
				anIntArray59[anInt43] = anIntArray88[anInt43];
			}
			if (anIntArray59[anInt43] > anIntArray88[anInt43]) {
			    anIntArray59[anInt43] -= anIntArray89[anInt43] * 4;
			    if (anIntArray59[anInt43] < anIntArray88[anInt43])
				anIntArray59[anInt43] = anIntArray88[anInt43];
			}
			if (anIntArray59[anInt43] < 500)
			    anIntArray59[anInt43] = 500;
			else if (anIntArray59[anInt43] > 7999)
			    anIntArray59[anInt43] = 7999;
			anInt44 = anIntArray56[anIntArray59[anInt43]];
			method34(anInt43, anInt44);
		    }
		}
	    }
	    anInt74 += 50;
	    if (anInt74 >= 64 || anInt72 >= 20000) {
		anInt74 = anInt74 % 64;
		for (anInt43 = 0; anInt43 < channelCount; anInt43++) {
		    if (anIntArray61[anInt43] >= 0
			&& (volumeTypes[anIntArray61[anInt43]] & 0x1) == 1) {
			anIntArray91[anInt43] = 1;
			i_32_ = anIntArray61[anInt43];
			int i_37_ = anIntArray90[anInt43];
			int i_38_;
			for (i_38_ = 0;
			     pointsForVolumeEnvelope[i_32_][i_38_ + 1] < i_37_;
			     i_38_++) {
			    /* empty */
			}
			int i_39_ = pointsForVolumeEnvelope[i_32_][i_38_];
			int i_40_ = pointsForVolumeEnvelope[i_32_][i_38_ + 1];
			int i_41_ = poinstForPanningEnvelope[i_32_][i_38_];
			int i_42_ = poinstForPanningEnvelope[i_32_][i_38_ + 1];
			if (i_40_ == i_39_)
			    i_40_++;
			int i_43_ = (((i_40_ - i_37_) * i_41_
				      + (i_37_ - i_39_) * i_42_)
				     / (i_40_ - i_39_));
			int i_44_ = 64 - anIntArray93[anInt43] / 500;
			if (i_44_ < 0)
			    i_44_ = 0;
			anInt40 = anIntArray58[anInt43] * i_43_ * i_44_ / 4096;
			method32(anInt43, anInt40 * anInt71 / 64);
			if ((volumeTypes[i_32_] & 0x2) == 2
			    && anIntArray92[anInt43] == 1
			    && (anIntArray90[anInt43]
				== (pointsForVolumeEnvelope[i_32_]
				    [volumeSustainPoints[i_32_]])))
			    anIntArray90[anInt43]--;
			if (anIntArray90[anInt43]
			    == (pointsForVolumeEnvelope[i_32_]
				[numberOfVolumePoints[i_32_] - 1]))
			    anIntArray90[anInt43]--;
			if (anIntArray92[anInt43] == 0)
			    anIntArray93[anInt43] += volumeFadeouts[i_32_];
			anIntArray90[anInt43]++;
			if ((volumeTypes[i_32_] & 0x4) == 4
			    && (anIntArray90[anInt43]
				== (pointsForVolumeEnvelope[i_32_]
				    [volumeLoopEndPoints[i_32_]])))
			    anIntArray90[anInt43] = (pointsForVolumeEnvelope[i_32_]
						     [volumeLoopStartPoints[i_32_]]);
		    } else
			anIntArray91[anInt43] = 0;
		}
	    }
	    if (anInt72 >= 20000) {
		anInt72 -= 20000;
		anInt73++;
		if (anInt73 >= defaultTempo)
		    anInt73 = 0;
	    }
	    if (anInt72 >= 20000)
		method41(0);
	}
    }
    
    public int read() {
	byte[] is = new byte[1];
	read(is, 0, 1);
	return is[0];
    }
    
    public int read(byte[] is, int i, int i_45_) {
	if (i_45_ > 100) {
	    read(is, i, 100);
	    read(is, i + 100, i_45_ - 100);
	    return i_45_;
	}
	int[] is_46_ = anIntArrayArray104[anIntArray109[0]];
	int i_47_ = anIntArray108[0];
	int i_48_ = anIntArray110[0];
	int i_49_ = anIntArray112[0];
	int i_50_ = anIntArray111[0];
	for (int i_52_ = 0; i_52_ < i_45_; i_52_++) {
	    int i_53_ = i_47_ & 0xff;
	    int i_54_ = i_47_ >> 8;
	    anIntArray23[i_52_]
		= (is_46_
		   [(aByteArrayArray106[i_53_][aByteArray22[i_54_] + 128]
		     + aByteArrayArray107[i_53_][aByteArray22[1 + i_54_] + 128]
		     + 128)]);
	    if ((i_47_ += i_48_) >= i_49_) {
		if (i_50_ == 0) {
		    anIntArray110[0] = i_48_ = 0;
		    i_47_ = i_49_ - 1;
		} else
		    i_47_ = (i_47_ - i_49_) % i_50_ + i_49_ - i_50_;
	    }
	}
	anIntArray108[0] = i_47_;
	for (int i_55_ = 1; i_55_ < channelCount; i_55_++) {
	    is_46_ = anIntArrayArray104[anIntArray109[i_55_]];
	    i_47_ = anIntArray108[i_55_];
	    i_48_ = anIntArray110[i_55_];
	    i_49_ = anIntArray112[i_55_];
	    i_50_ = anIntArray111[i_55_];
	    for (int i_56_ = 0; i_56_ < i_45_; i_56_++) {
		int i_57_ = i_47_ & 0xff;
		int i_58_ = i_47_ >> 8;
		anIntArray23[i_56_]
		    += (is_46_
			[(aByteArrayArray106[i_57_][aByteArray22[i_58_] + 128]
			  + (aByteArrayArray107[i_57_]
			     [aByteArray22[1 + i_58_] + 128])
			  + 128)]);
		if ((i_47_ += i_48_) >= i_49_) {
		    if (i_50_ == 0) {
			anIntArray110[i_55_] = i_48_ = 0;
			i_47_ = i_49_ - 1;
		    } else
			i_47_ = (i_47_ - i_49_) % i_50_ + i_49_ - i_50_;
		}
	    }
	    anIntArray108[i_55_] = i_47_;
	}
	for (int i_59_ = i; i_59_ < i + i_45_; i_59_++)
	    is[i_59_] = aByteArray21[anIntArray23[i_59_ - i] + 32768];
	method41(i_45_);
	return i_45_;
    }
}
